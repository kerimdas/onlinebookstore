﻿namespace onlinestore {
    partial class FormAddMusic {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddMusic));
            this.LabelAddMusicCDPagePrice = new System.Windows.Forms.Label();
            this.ComboBoxAddMusicCDPageType = new System.Windows.Forms.ComboBox();
            this.LabelAddMusicCDPageType = new System.Windows.Forms.Label();
            this.PictureBoxAddMusicCDPage = new System.Windows.Forms.PictureBox();
            this.TextBoxAddMusicCDPagePrice = new System.Windows.Forms.TextBox();
            this.TextBoxAddMusicCDPageSinger = new System.Windows.Forms.TextBox();
            this.PanelAddMusicCDPageSidebarRight = new System.Windows.Forms.Panel();
            this.PanelAddMusicCDPageSidebarLeft = new System.Windows.Forms.Panel();
            this.LabelAddMusicCDPageTop = new System.Windows.Forms.Label();
            this.LabelAddMusicCDPageSinger = new System.Windows.Forms.Label();
            this.TextBoxAddMusicCDPageName = new System.Windows.Forms.TextBox();
            this.LabelAddMusicCDPageName = new System.Windows.Forms.Label();
            this.PanelAddMusicCDPagePictureBox = new System.Windows.Forms.Panel();
            this.PanelAddMusicCDPageTop = new System.Windows.Forms.Panel();
            this.ButtonAddMusicCDPageExit = new System.Windows.Forms.Button();
            this.ButtonAddMusicCDPageAdd = new System.Windows.Forms.Button();
            this.PanelAddMusicCDPageSidebarBottom = new System.Windows.Forms.Panel();
            this.PanelAddMusicCDPageMiddle = new System.Windows.Forms.Panel();
            this.ButtonAddMusicCDPageAddPicture = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAddMusicCDPage)).BeginInit();
            this.PanelAddMusicCDPagePictureBox.SuspendLayout();
            this.PanelAddMusicCDPageTop.SuspendLayout();
            this.PanelAddMusicCDPageMiddle.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelAddMusicCDPagePrice
            // 
            this.LabelAddMusicCDPagePrice.AutoSize = true;
            this.LabelAddMusicCDPagePrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMusicCDPagePrice.ForeColor = System.Drawing.Color.White;
            this.LabelAddMusicCDPagePrice.Location = new System.Drawing.Point(94, 336);
            this.LabelAddMusicCDPagePrice.Name = "LabelAddMusicCDPagePrice";
            this.LabelAddMusicCDPagePrice.Size = new System.Drawing.Size(57, 23);
            this.LabelAddMusicCDPagePrice.TabIndex = 24;
            this.LabelAddMusicCDPagePrice.Text = "Price";
            // 
            // ComboBoxAddMusicCDPageType
            // 
            this.ComboBoxAddMusicCDPageType.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ComboBoxAddMusicCDPageType.FormattingEnabled = true;
            this.ComboBoxAddMusicCDPageType.Location = new System.Drawing.Point(98, 288);
            this.ComboBoxAddMusicCDPageType.Name = "ComboBoxAddMusicCDPageType";
            this.ComboBoxAddMusicCDPageType.Size = new System.Drawing.Size(250, 31);
            this.ComboBoxAddMusicCDPageType.TabIndex = 23;
            // 
            // LabelAddMusicCDPageType
            // 
            this.LabelAddMusicCDPageType.AutoSize = true;
            this.LabelAddMusicCDPageType.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMusicCDPageType.ForeColor = System.Drawing.Color.White;
            this.LabelAddMusicCDPageType.Location = new System.Drawing.Point(94, 262);
            this.LabelAddMusicCDPageType.Name = "LabelAddMusicCDPageType";
            this.LabelAddMusicCDPageType.Size = new System.Drawing.Size(56, 23);
            this.LabelAddMusicCDPageType.TabIndex = 22;
            this.LabelAddMusicCDPageType.Text = "Type";
            // 
            // PictureBoxAddMusicCDPage
            // 
            this.PictureBoxAddMusicCDPage.Location = new System.Drawing.Point(10, 10);
            this.PictureBoxAddMusicCDPage.Name = "PictureBoxAddMusicCDPage";
            this.PictureBoxAddMusicCDPage.Size = new System.Drawing.Size(180, 180);
            this.PictureBoxAddMusicCDPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxAddMusicCDPage.TabIndex = 0;
            this.PictureBoxAddMusicCDPage.TabStop = false;
            // 
            // TextBoxAddMusicCDPagePrice
            // 
            this.TextBoxAddMusicCDPagePrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddMusicCDPagePrice.Location = new System.Drawing.Point(98, 363);
            this.TextBoxAddMusicCDPagePrice.Name = "TextBoxAddMusicCDPagePrice";
            this.TextBoxAddMusicCDPagePrice.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddMusicCDPagePrice.TabIndex = 25;
            this.TextBoxAddMusicCDPagePrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxAddMusicCDPagePrice_KeyPress);
            // 
            // TextBoxAddMusicCDPageSinger
            // 
            this.TextBoxAddMusicCDPageSinger.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddMusicCDPageSinger.Location = new System.Drawing.Point(98, 204);
            this.TextBoxAddMusicCDPageSinger.Name = "TextBoxAddMusicCDPageSinger";
            this.TextBoxAddMusicCDPageSinger.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddMusicCDPageSinger.TabIndex = 21;
            // 
            // PanelAddMusicCDPageSidebarRight
            // 
            this.PanelAddMusicCDPageSidebarRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMusicCDPageSidebarRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelAddMusicCDPageSidebarRight.Location = new System.Drawing.Point(781, 0);
            this.PanelAddMusicCDPageSidebarRight.Name = "PanelAddMusicCDPageSidebarRight";
            this.PanelAddMusicCDPageSidebarRight.Size = new System.Drawing.Size(1, 503);
            this.PanelAddMusicCDPageSidebarRight.TabIndex = 1;
            // 
            // PanelAddMusicCDPageSidebarLeft
            // 
            this.PanelAddMusicCDPageSidebarLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMusicCDPageSidebarLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelAddMusicCDPageSidebarLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelAddMusicCDPageSidebarLeft.Name = "PanelAddMusicCDPageSidebarLeft";
            this.PanelAddMusicCDPageSidebarLeft.Size = new System.Drawing.Size(1, 503);
            this.PanelAddMusicCDPageSidebarLeft.TabIndex = 0;
            // 
            // LabelAddMusicCDPageTop
            // 
            this.LabelAddMusicCDPageTop.AutoSize = true;
            this.LabelAddMusicCDPageTop.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMusicCDPageTop.ForeColor = System.Drawing.Color.White;
            this.LabelAddMusicCDPageTop.Location = new System.Drawing.Point(23, 10);
            this.LabelAddMusicCDPageTop.Name = "LabelAddMusicCDPageTop";
            this.LabelAddMusicCDPageTop.Size = new System.Drawing.Size(180, 28);
            this.LabelAddMusicCDPageTop.TabIndex = 0;
            this.LabelAddMusicCDPageTop.Text = "Add Music CD";
            // 
            // LabelAddMusicCDPageSinger
            // 
            this.LabelAddMusicCDPageSinger.AutoSize = true;
            this.LabelAddMusicCDPageSinger.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMusicCDPageSinger.ForeColor = System.Drawing.Color.White;
            this.LabelAddMusicCDPageSinger.Location = new System.Drawing.Point(94, 177);
            this.LabelAddMusicCDPageSinger.Name = "LabelAddMusicCDPageSinger";
            this.LabelAddMusicCDPageSinger.Size = new System.Drawing.Size(67, 23);
            this.LabelAddMusicCDPageSinger.TabIndex = 20;
            this.LabelAddMusicCDPageSinger.Text = "Singer";
            // 
            // TextBoxAddMusicCDPageName
            // 
            this.TextBoxAddMusicCDPageName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddMusicCDPageName.Location = new System.Drawing.Point(98, 122);
            this.TextBoxAddMusicCDPageName.Name = "TextBoxAddMusicCDPageName";
            this.TextBoxAddMusicCDPageName.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddMusicCDPageName.TabIndex = 19;
            // 
            // LabelAddMusicCDPageName
            // 
            this.LabelAddMusicCDPageName.AutoSize = true;
            this.LabelAddMusicCDPageName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMusicCDPageName.ForeColor = System.Drawing.Color.White;
            this.LabelAddMusicCDPageName.Location = new System.Drawing.Point(94, 95);
            this.LabelAddMusicCDPageName.Name = "LabelAddMusicCDPageName";
            this.LabelAddMusicCDPageName.Size = new System.Drawing.Size(71, 23);
            this.LabelAddMusicCDPageName.TabIndex = 18;
            this.LabelAddMusicCDPageName.Text = "Name";
            // 
            // PanelAddMusicCDPagePictureBox
            // 
            this.PanelAddMusicCDPagePictureBox.Controls.Add(this.PictureBoxAddMusicCDPage);
            this.PanelAddMusicCDPagePictureBox.Location = new System.Drawing.Point(451, 95);
            this.PanelAddMusicCDPagePictureBox.Name = "PanelAddMusicCDPagePictureBox";
            this.PanelAddMusicCDPagePictureBox.Size = new System.Drawing.Size(200, 200);
            this.PanelAddMusicCDPagePictureBox.TabIndex = 17;
            // 
            // PanelAddMusicCDPageTop
            // 
            this.PanelAddMusicCDPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMusicCDPageTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAddMusicCDPageTop.Controls.Add(this.ButtonAddMusicCDPageExit);
            this.PanelAddMusicCDPageTop.Controls.Add(this.LabelAddMusicCDPageTop);
            this.PanelAddMusicCDPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelAddMusicCDPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelAddMusicCDPageTop.Name = "PanelAddMusicCDPageTop";
            this.PanelAddMusicCDPageTop.Size = new System.Drawing.Size(782, 50);
            this.PanelAddMusicCDPageTop.TabIndex = 7;
            // 
            // ButtonAddMusicCDPageExit
            // 
            this.ButtonAddMusicCDPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonAddMusicCDPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddMusicCDPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonAddMusicCDPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonAddMusicCDPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonAddMusicCDPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddMusicCDPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonAddMusicCDPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAddMusicCDPageExit.Image")));
            this.ButtonAddMusicCDPageExit.Location = new System.Drawing.Point(721, 3);
            this.ButtonAddMusicCDPageExit.Name = "ButtonAddMusicCDPageExit";
            this.ButtonAddMusicCDPageExit.Size = new System.Drawing.Size(53, 46);
            this.ButtonAddMusicCDPageExit.TabIndex = 4;
            this.ButtonAddMusicCDPageExit.UseVisualStyleBackColor = true;
            this.ButtonAddMusicCDPageExit.Click += new System.EventHandler(this.ButtonAddMusicCDPageExit_Click);
            // 
            // ButtonAddMusicCDPageAdd
            // 
            this.ButtonAddMusicCDPageAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonAddMusicCDPageAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddMusicCDPageAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddMusicCDPageAdd.FlatAppearance.BorderSize = 0;
            this.ButtonAddMusicCDPageAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddMusicCDPageAdd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAddMusicCDPageAdd.ForeColor = System.Drawing.Color.White;
            this.ButtonAddMusicCDPageAdd.Location = new System.Drawing.Point(276, 424);
            this.ButtonAddMusicCDPageAdd.Name = "ButtonAddMusicCDPageAdd";
            this.ButtonAddMusicCDPageAdd.Size = new System.Drawing.Size(250, 50);
            this.ButtonAddMusicCDPageAdd.TabIndex = 15;
            this.ButtonAddMusicCDPageAdd.Text = "ADD";
            this.ButtonAddMusicCDPageAdd.UseVisualStyleBackColor = false;
            this.ButtonAddMusicCDPageAdd.Click += new System.EventHandler(this.ButtonAddMusicCDPageAdd_Click);
            // 
            // PanelAddMusicCDPageSidebarBottom
            // 
            this.PanelAddMusicCDPageSidebarBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMusicCDPageSidebarBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelAddMusicCDPageSidebarBottom.Location = new System.Drawing.Point(1, 502);
            this.PanelAddMusicCDPageSidebarBottom.Name = "PanelAddMusicCDPageSidebarBottom";
            this.PanelAddMusicCDPageSidebarBottom.Size = new System.Drawing.Size(780, 1);
            this.PanelAddMusicCDPageSidebarBottom.TabIndex = 2;
            // 
            // PanelAddMusicCDPageMiddle
            // 
            this.PanelAddMusicCDPageMiddle.BackColor = System.Drawing.Color.Silver;
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.ButtonAddMusicCDPageAddPicture);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.TextBoxAddMusicCDPagePrice);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.LabelAddMusicCDPagePrice);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.ComboBoxAddMusicCDPageType);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.LabelAddMusicCDPageType);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.TextBoxAddMusicCDPageSinger);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.LabelAddMusicCDPageSinger);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.TextBoxAddMusicCDPageName);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.LabelAddMusicCDPageName);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.PanelAddMusicCDPagePictureBox);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.ButtonAddMusicCDPageAdd);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.PanelAddMusicCDPageSidebarBottom);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.PanelAddMusicCDPageSidebarRight);
            this.PanelAddMusicCDPageMiddle.Controls.Add(this.PanelAddMusicCDPageSidebarLeft);
            this.PanelAddMusicCDPageMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelAddMusicCDPageMiddle.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.PanelAddMusicCDPageMiddle.Location = new System.Drawing.Point(0, 0);
            this.PanelAddMusicCDPageMiddle.Name = "PanelAddMusicCDPageMiddle";
            this.PanelAddMusicCDPageMiddle.Size = new System.Drawing.Size(782, 503);
            this.PanelAddMusicCDPageMiddle.TabIndex = 8;
            // 
            // ButtonAddMusicCDPageAddPicture
            // 
            this.ButtonAddMusicCDPageAddPicture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonAddMusicCDPageAddPicture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddMusicCDPageAddPicture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddMusicCDPageAddPicture.FlatAppearance.BorderSize = 0;
            this.ButtonAddMusicCDPageAddPicture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddMusicCDPageAddPicture.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAddMusicCDPageAddPicture.ForeColor = System.Drawing.Color.White;
            this.ButtonAddMusicCDPageAddPicture.Location = new System.Drawing.Point(424, 309);
            this.ButtonAddMusicCDPageAddPicture.Name = "ButtonAddMusicCDPageAddPicture";
            this.ButtonAddMusicCDPageAddPicture.Size = new System.Drawing.Size(250, 50);
            this.ButtonAddMusicCDPageAddPicture.TabIndex = 26;
            this.ButtonAddMusicCDPageAddPicture.Text = "Add Picture";
            this.ButtonAddMusicCDPageAddPicture.UseVisualStyleBackColor = false;
            this.ButtonAddMusicCDPageAddPicture.Click += new System.EventHandler(this.ButtonAddMusicCDPageAddPicture_Click);
            // 
            // FormAddMusic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 503);
            this.Controls.Add(this.PanelAddMusicCDPageTop);
            this.Controls.Add(this.PanelAddMusicCDPageMiddle);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAddMusic";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddMusicCDPage";
            this.Load += new System.EventHandler(this.AddMusicCDPage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAddMusicCDPage)).EndInit();
            this.PanelAddMusicCDPagePictureBox.ResumeLayout(false);
            this.PanelAddMusicCDPageTop.ResumeLayout(false);
            this.PanelAddMusicCDPageTop.PerformLayout();
            this.PanelAddMusicCDPageMiddle.ResumeLayout(false);
            this.PanelAddMusicCDPageMiddle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LabelAddMusicCDPagePrice;
        private System.Windows.Forms.ComboBox ComboBoxAddMusicCDPageType;
        private System.Windows.Forms.Label LabelAddMusicCDPageType;
        private System.Windows.Forms.PictureBox PictureBoxAddMusicCDPage;
        private System.Windows.Forms.TextBox TextBoxAddMusicCDPagePrice;
        private System.Windows.Forms.TextBox TextBoxAddMusicCDPageSinger;
        private System.Windows.Forms.Panel PanelAddMusicCDPageSidebarRight;
        private System.Windows.Forms.Panel PanelAddMusicCDPageSidebarLeft;
        private System.Windows.Forms.Label LabelAddMusicCDPageTop;
        private System.Windows.Forms.Label LabelAddMusicCDPageSinger;
        private System.Windows.Forms.TextBox TextBoxAddMusicCDPageName;
        private System.Windows.Forms.Label LabelAddMusicCDPageName;
        private System.Windows.Forms.Panel PanelAddMusicCDPagePictureBox;
        private System.Windows.Forms.Panel PanelAddMusicCDPageTop;
        private System.Windows.Forms.Button ButtonAddMusicCDPageExit;
        private System.Windows.Forms.Button ButtonAddMusicCDPageAdd;
        private System.Windows.Forms.Panel PanelAddMusicCDPageSidebarBottom;
        private System.Windows.Forms.Panel PanelAddMusicCDPageMiddle;
        private System.Windows.Forms.Button ButtonAddMusicCDPageAddPicture;
    }
}