﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Kullanici formu.
    /// </summary>
    public partial class FormUser : Form {

        ClassCustomer customer = ClassCustomer.InstanceCustomer;
        ClassDatabase database = ClassDatabase.connect();
        ISubjectUser subjectUser;
        ArrayList orderList = new ArrayList();
        ClassDatabaseOrderItem classOrderItem = new ClassDatabaseOrderItem();
        ClassProductType classProductType = new ClassProductType();//benim oluşturuduğum class singıl responsibility yeaah 

        public FormUser() {
            InitializeComponent();
        }

        public FormUser(ClassCustomer _customer) {
            InitializeComponent();
            customer = _customer;
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormUser.", customer.Username, nameof(FormUser));
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonUserPageExit_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is exited from the FormUser.", customer.Username, nameof(ButtonUserPageExit_Click));
            this.Close();
        }

        private void UserPage_Load(object sender, EventArgs e) {
            LabelUserPageNameTop.Text = customer.Username;
            TextBoxUserPageUsername.Text = customer.Username;
            TextBoxUserPagePassword.Text = customer.Password;
            TextBoxUserPageConfirmPassword.Text = customer.Password;
            TextBoxUserPageName.Text = customer.Name;
            TextBoxUserPageEmail.Text = customer.Email;
            MultiTextBoxUserPageAddress.Text = customer.Address;            

            AddOrder();            
        }

        /// <summary>
        /// Kullanicinin girdigi yeni bilgileri gunceller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonUserPageUpdate_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is updated the user information.", customer.Username, nameof(ButtonUserPageUpdate_Click));
            if (string.Equals(TextBoxUserPagePassword.Text, TextBoxUserPageConfirmPassword.Text)) {
                database.UpdateUserData(customer.ID, TextBoxUserPagePassword.Text, TextBoxUserPageEmail.Text, MultiTextBoxUserPageAddress.Text);
                this.Close();
            } else {
                LabelUserPageNotConfirmPassword.Visible = true;
            }
        }

        private void FormUser_FormClosing(object sender, FormClosingEventArgs e) {

        }
        
        /// <summary>
        /// Urun tipine bakar.
        /// </summary>
        /// <param name="type">Urun tipinin ismini parametre olarak alir.</param>
        /// <returns>Urunun tipini dondurur.</returns>
        private string GetTypeString(string type)
        {
            if(type == "Book"){

                return "books/";
            }
            else if(type == "Magazine")
            {
                return "magazines/";
            }
            else if(type == "MusicCD")
            {

                return "musicCDs/";
            }
            return "";
        }

        /// <summary>
        /// DataGridView'e, kullanicinin gecmiste satin aldigi urunler eklenir.
        /// </summary>
        private void AddOrder()
        {
            if (database.Count("[OrderTable]") > 0)
            {
                DataGridViewImageColumn dataGridViewImageColumn = new DataGridViewImageColumn();
                dataGridViewImageColumn.ImageLayout = DataGridViewImageCellLayout.Zoom;
                DataGridViewOrders.Columns.Add(dataGridViewImageColumn);

                DataTable dataTable = new DataTable();
                dataTable = database.GetOrderTwoData(customer.ID);

                dataTable.Columns["TYPE"].DataType = typeof(string);

                DataGridViewOrders.DataSource = dataTable;


                foreach (DataGridViewColumn dgvc in DataGridViewOrders.Columns)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.NotSortable;
                }

                string type = string.Empty;
                string datatabletype = string.Empty;
             
                foreach (DataGridViewRow item in DataGridViewOrders.Rows)
                {
                    
                    type = item.Cells["TYPE"].Value.ToString();
                    datatabletype = type;
                    type = type.Trim(' ');
                    type = classProductType.GetTypeString(type);
                    //type = GetTypeString(type);
                    item.Cells[0].Value = Image.FromFile(type + (item.Cells[8].Value) + ".jpg");
                  
                    item.Cells["TYPE"].Value = datatabletype;
                }

                DataGridViewOrders.Columns[1].Visible = false;
                DataGridViewOrders.Columns[8].Visible = false;
                DataGridViewOrders.Columns["CUSTOMERID"].Visible = false;
                DataGridViewOrders.Columns["UNITPRICE"].DefaultCellStyle.Format = "#.#0";
                DataGridViewOrders.Columns["TOTALPRICE"].DefaultCellStyle.Format = "#.#0";
            }            
        }
    }
}
