﻿namespace onlinestore {
    partial class FormBook {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBook));
            this.PanelBookPageTop = new System.Windows.Forms.Panel();
            this.LabelMyCartCount = new System.Windows.Forms.Label();
            this.ButtonBookPageMyCart = new System.Windows.Forms.Button();
            this.ButtonBookPageTopUsername = new System.Windows.Forms.Button();
            this.ButtonBookPageExit = new System.Windows.Forms.Button();
            this.LabelBookPageTop = new System.Windows.Forms.Label();
            this.PictureBoxBookPageTop = new System.Windows.Forms.PictureBox();
            this.PanelDetails = new System.Windows.Forms.Panel();
            this.PanelSeperator = new System.Windows.Forms.Panel();
            this.PanelSeperatorPage = new System.Windows.Forms.Panel();
            this.PanelSeperatorAuthor = new System.Windows.Forms.Panel();
            this.PanelSeperatorPublisher = new System.Windows.Forms.Panel();
            this.PanelSeperatorName = new System.Windows.Forms.Panel();
            this.PanelSeperatorISBN = new System.Windows.Forms.Panel();
            this.LabelDetailsPriceText = new System.Windows.Forms.Label();
            this.LabelDetailsPublisher = new System.Windows.Forms.Label();
            this.LabelDetailsAuthorText = new System.Windows.Forms.Label();
            this.LabelDetailsPageText = new System.Windows.Forms.Label();
            this.LabelDetailsNameText = new System.Windows.Forms.Label();
            this.LabelDetailsISBNText = new System.Windows.Forms.Label();
            this.LabelDetailsPrice = new System.Windows.Forms.Label();
            this.LabelDetailsPage = new System.Windows.Forms.Label();
            this.LabelDetailsISBN = new System.Windows.Forms.Label();
            this.LabelDetailsName = new System.Windows.Forms.Label();
            this.LabelDetailsAuthor = new System.Windows.Forms.Label();
            this.LabelDetailsPublisherText = new System.Windows.Forms.Label();
            this.PictureBoxDetails = new System.Windows.Forms.PictureBox();
            this.LabelPanelDetailsText = new System.Windows.Forms.Label();
            this.LabelTableLayoutText = new System.Windows.Forms.Label();
            this.GroupBoxBooks = new System.Windows.Forms.GroupBox();
            this.TableLayoutPanelBooks = new System.Windows.Forms.TableLayoutPanel();
            this.PanelBookPageBottom = new System.Windows.Forms.Panel();
            this.TimerRefreshEventsBookPage = new System.Windows.Forms.Timer(this.components);
            this.PanelBookPageTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBookPageTop)).BeginInit();
            this.PanelDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDetails)).BeginInit();
            this.GroupBoxBooks.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelBookPageTop
            // 
            this.PanelBookPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelBookPageTop.Controls.Add(this.LabelMyCartCount);
            this.PanelBookPageTop.Controls.Add(this.ButtonBookPageMyCart);
            this.PanelBookPageTop.Controls.Add(this.ButtonBookPageTopUsername);
            this.PanelBookPageTop.Controls.Add(this.ButtonBookPageExit);
            this.PanelBookPageTop.Controls.Add(this.LabelBookPageTop);
            this.PanelBookPageTop.Controls.Add(this.PictureBoxBookPageTop);
            this.PanelBookPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelBookPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelBookPageTop.Name = "PanelBookPageTop";
            this.PanelBookPageTop.Size = new System.Drawing.Size(900, 56);
            this.PanelBookPageTop.TabIndex = 2;
            // 
            // LabelMyCartCount
            // 
            this.LabelMyCartCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelMyCartCount.AutoSize = true;
            this.LabelMyCartCount.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMyCartCount.ForeColor = System.Drawing.Color.White;
            this.LabelMyCartCount.Location = new System.Drawing.Point(678, 6);
            this.LabelMyCartCount.Name = "LabelMyCartCount";
            this.LabelMyCartCount.Size = new System.Drawing.Size(15, 16);
            this.LabelMyCartCount.TabIndex = 5;
            this.LabelMyCartCount.Text = "0";
            // 
            // ButtonBookPageMyCart
            // 
            this.ButtonBookPageMyCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonBookPageMyCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonBookPageMyCart.FlatAppearance.BorderSize = 0;
            this.ButtonBookPageMyCart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonBookPageMyCart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonBookPageMyCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBookPageMyCart.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonBookPageMyCart.ForeColor = System.Drawing.Color.White;
            this.ButtonBookPageMyCart.Image = ((System.Drawing.Image)(resources.GetObject("ButtonBookPageMyCart.Image")));
            this.ButtonBookPageMyCart.Location = new System.Drawing.Point(645, 3);
            this.ButtonBookPageMyCart.Name = "ButtonBookPageMyCart";
            this.ButtonBookPageMyCart.Size = new System.Drawing.Size(58, 50);
            this.ButtonBookPageMyCart.TabIndex = 4;
            this.ButtonBookPageMyCart.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ButtonBookPageMyCart.UseVisualStyleBackColor = true;
            this.ButtonBookPageMyCart.Click += new System.EventHandler(this.ButtonBookPageMyCart_Click);
            // 
            // ButtonBookPageTopUsername
            // 
            this.ButtonBookPageTopUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonBookPageTopUsername.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonBookPageTopUsername.FlatAppearance.BorderSize = 0;
            this.ButtonBookPageTopUsername.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonBookPageTopUsername.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonBookPageTopUsername.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBookPageTopUsername.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonBookPageTopUsername.ForeColor = System.Drawing.Color.White;
            this.ButtonBookPageTopUsername.Image = ((System.Drawing.Image)(resources.GetObject("ButtonBookPageTopUsername.Image")));
            this.ButtonBookPageTopUsername.Location = new System.Drawing.Point(714, 3);
            this.ButtonBookPageTopUsername.Name = "ButtonBookPageTopUsername";
            this.ButtonBookPageTopUsername.Size = new System.Drawing.Size(124, 50);
            this.ButtonBookPageTopUsername.TabIndex = 3;
            this.ButtonBookPageTopUsername.Text = "Beta User";
            this.ButtonBookPageTopUsername.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonBookPageTopUsername.UseVisualStyleBackColor = true;
            this.ButtonBookPageTopUsername.Click += new System.EventHandler(this.ButtonBookPageTopUsername_Click);
            // 
            // ButtonBookPageExit
            // 
            this.ButtonBookPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonBookPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonBookPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonBookPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonBookPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonBookPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonBookPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonBookPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonBookPageExit.Image")));
            this.ButtonBookPageExit.Location = new System.Drawing.Point(844, 4);
            this.ButtonBookPageExit.Name = "ButtonBookPageExit";
            this.ButtonBookPageExit.Size = new System.Drawing.Size(53, 49);
            this.ButtonBookPageExit.TabIndex = 1;
            this.ButtonBookPageExit.UseVisualStyleBackColor = true;
            this.ButtonBookPageExit.Click += new System.EventHandler(this.ButtonBookPageExit_Click);
            // 
            // LabelBookPageTop
            // 
            this.LabelBookPageTop.AutoSize = true;
            this.LabelBookPageTop.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelBookPageTop.ForeColor = System.Drawing.Color.White;
            this.LabelBookPageTop.Location = new System.Drawing.Point(74, 16);
            this.LabelBookPageTop.Name = "LabelBookPageTop";
            this.LabelBookPageTop.Size = new System.Drawing.Size(191, 25);
            this.LabelBookPageTop.TabIndex = 2;
            this.LabelBookPageTop.Text = "Online Book Store";
            // 
            // PictureBoxBookPageTop
            // 
            this.PictureBoxBookPageTop.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxBookPageTop.Image")));
            this.PictureBoxBookPageTop.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxBookPageTop.Name = "PictureBoxBookPageTop";
            this.PictureBoxBookPageTop.Size = new System.Drawing.Size(68, 56);
            this.PictureBoxBookPageTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxBookPageTop.TabIndex = 1;
            this.PictureBoxBookPageTop.TabStop = false;
            // 
            // PanelDetails
            // 
            this.PanelDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelDetails.BackColor = System.Drawing.Color.White;
            this.PanelDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelDetails.Controls.Add(this.PanelSeperator);
            this.PanelDetails.Controls.Add(this.PanelSeperatorPage);
            this.PanelDetails.Controls.Add(this.PanelSeperatorAuthor);
            this.PanelDetails.Controls.Add(this.PanelSeperatorPublisher);
            this.PanelDetails.Controls.Add(this.PanelSeperatorName);
            this.PanelDetails.Controls.Add(this.PanelSeperatorISBN);
            this.PanelDetails.Controls.Add(this.LabelDetailsPriceText);
            this.PanelDetails.Controls.Add(this.LabelDetailsPublisher);
            this.PanelDetails.Controls.Add(this.LabelDetailsAuthorText);
            this.PanelDetails.Controls.Add(this.LabelDetailsPageText);
            this.PanelDetails.Controls.Add(this.LabelDetailsNameText);
            this.PanelDetails.Controls.Add(this.LabelDetailsISBNText);
            this.PanelDetails.Controls.Add(this.LabelDetailsPrice);
            this.PanelDetails.Controls.Add(this.LabelDetailsPage);
            this.PanelDetails.Controls.Add(this.LabelDetailsISBN);
            this.PanelDetails.Controls.Add(this.LabelDetailsName);
            this.PanelDetails.Controls.Add(this.LabelDetailsAuthor);
            this.PanelDetails.Controls.Add(this.LabelDetailsPublisherText);
            this.PanelDetails.Controls.Add(this.PictureBoxDetails);
            this.PanelDetails.Location = new System.Drawing.Point(435, 83);
            this.PanelDetails.Name = "PanelDetails";
            this.PanelDetails.Size = new System.Drawing.Size(431, 333);
            this.PanelDetails.TabIndex = 4;
            this.PanelDetails.Visible = false;
            // 
            // PanelSeperator
            // 
            this.PanelSeperator.BackColor = System.Drawing.Color.Black;
            this.PanelSeperator.Location = new System.Drawing.Point(235, 304);
            this.PanelSeperator.Name = "PanelSeperator";
            this.PanelSeperator.Size = new System.Drawing.Size(200, 1);
            this.PanelSeperator.TabIndex = 21;
            // 
            // PanelSeperatorPage
            // 
            this.PanelSeperatorPage.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorPage.Location = new System.Drawing.Point(7, 304);
            this.PanelSeperatorPage.Name = "PanelSeperatorPage";
            this.PanelSeperatorPage.Size = new System.Drawing.Size(200, 1);
            this.PanelSeperatorPage.TabIndex = 20;
            // 
            // PanelSeperatorAuthor
            // 
            this.PanelSeperatorAuthor.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorAuthor.Location = new System.Drawing.Point(235, 215);
            this.PanelSeperatorAuthor.Name = "PanelSeperatorAuthor";
            this.PanelSeperatorAuthor.Size = new System.Drawing.Size(200, 1);
            this.PanelSeperatorAuthor.TabIndex = 21;
            // 
            // PanelSeperatorPublisher
            // 
            this.PanelSeperatorPublisher.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorPublisher.Location = new System.Drawing.Point(235, 262);
            this.PanelSeperatorPublisher.Name = "PanelSeperatorPublisher";
            this.PanelSeperatorPublisher.Size = new System.Drawing.Size(200, 1);
            this.PanelSeperatorPublisher.TabIndex = 20;
            // 
            // PanelSeperatorName
            // 
            this.PanelSeperatorName.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorName.Location = new System.Drawing.Point(7, 262);
            this.PanelSeperatorName.Name = "PanelSeperatorName";
            this.PanelSeperatorName.Size = new System.Drawing.Size(200, 1);
            this.PanelSeperatorName.TabIndex = 19;
            // 
            // PanelSeperatorISBN
            // 
            this.PanelSeperatorISBN.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorISBN.Location = new System.Drawing.Point(7, 215);
            this.PanelSeperatorISBN.Name = "PanelSeperatorISBN";
            this.PanelSeperatorISBN.Size = new System.Drawing.Size(200, 1);
            this.PanelSeperatorISBN.TabIndex = 18;
            // 
            // LabelDetailsPriceText
            // 
            this.LabelDetailsPriceText.AutoSize = true;
            this.LabelDetailsPriceText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPriceText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsPriceText.Location = new System.Drawing.Point(167, 37);
            this.LabelDetailsPriceText.Name = "LabelDetailsPriceText";
            this.LabelDetailsPriceText.Size = new System.Drawing.Size(35, 15);
            this.LabelDetailsPriceText.TabIndex = 17;
            this.LabelDetailsPriceText.Text = "Price";
            // 
            // LabelDetailsPublisher
            // 
            this.LabelDetailsPublisher.AutoEllipsis = true;
            this.LabelDetailsPublisher.AutoSize = true;
            this.LabelDetailsPublisher.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPublisher.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsPublisher.Location = new System.Drawing.Point(231, 280);
            this.LabelDetailsPublisher.Name = "LabelDetailsPublisher";
            this.LabelDetailsPublisher.Size = new System.Drawing.Size(157, 21);
            this.LabelDetailsPublisher.TabIndex = 16;
            this.LabelDetailsPublisher.Text = "Yapı Kredi Yayınları";
            // 
            // LabelDetailsAuthorText
            // 
            this.LabelDetailsAuthorText.AutoSize = true;
            this.LabelDetailsAuthorText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsAuthorText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsAuthorText.Location = new System.Drawing.Point(232, 219);
            this.LabelDetailsAuthorText.Name = "LabelDetailsAuthorText";
            this.LabelDetailsAuthorText.Size = new System.Drawing.Size(43, 15);
            this.LabelDetailsAuthorText.TabIndex = 15;
            this.LabelDetailsAuthorText.Text = "Author";
            // 
            // LabelDetailsPageText
            // 
            this.LabelDetailsPageText.AutoSize = true;
            this.LabelDetailsPageText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPageText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsPageText.Location = new System.Drawing.Point(4, 265);
            this.LabelDetailsPageText.Name = "LabelDetailsPageText";
            this.LabelDetailsPageText.Size = new System.Drawing.Size(35, 15);
            this.LabelDetailsPageText.TabIndex = 14;
            this.LabelDetailsPageText.Text = "Page";
            // 
            // LabelDetailsNameText
            // 
            this.LabelDetailsNameText.AutoSize = true;
            this.LabelDetailsNameText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsNameText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsNameText.Location = new System.Drawing.Point(4, 219);
            this.LabelDetailsNameText.Name = "LabelDetailsNameText";
            this.LabelDetailsNameText.Size = new System.Drawing.Size(39, 15);
            this.LabelDetailsNameText.TabIndex = 13;
            this.LabelDetailsNameText.Text = "Name";
            // 
            // LabelDetailsISBNText
            // 
            this.LabelDetailsISBNText.AutoSize = true;
            this.LabelDetailsISBNText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsISBNText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsISBNText.Location = new System.Drawing.Point(4, 171);
            this.LabelDetailsISBNText.Name = "LabelDetailsISBNText";
            this.LabelDetailsISBNText.Size = new System.Drawing.Size(31, 15);
            this.LabelDetailsISBNText.TabIndex = 12;
            this.LabelDetailsISBNText.Text = "ISBN";
            // 
            // LabelDetailsPrice
            // 
            this.LabelDetailsPrice.AutoSize = true;
            this.LabelDetailsPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelDetailsPrice.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelDetailsPrice.Location = new System.Drawing.Point(170, 52);
            this.LabelDetailsPrice.Name = "LabelDetailsPrice";
            this.LabelDetailsPrice.Size = new System.Drawing.Size(145, 58);
            this.LabelDetailsPrice.TabIndex = 11;
            this.LabelDetailsPrice.Text = "5,63₺";
            // 
            // LabelDetailsPage
            // 
            this.LabelDetailsPage.AutoSize = true;
            this.LabelDetailsPage.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPage.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsPage.Location = new System.Drawing.Point(3, 280);
            this.LabelDetailsPage.Name = "LabelDetailsPage";
            this.LabelDetailsPage.Size = new System.Drawing.Size(37, 21);
            this.LabelDetailsPage.TabIndex = 9;
            this.LabelDetailsPage.Text = "164";
            // 
            // LabelDetailsISBN
            // 
            this.LabelDetailsISBN.AutoSize = true;
            this.LabelDetailsISBN.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsISBN.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsISBN.Location = new System.Drawing.Point(3, 186);
            this.LabelDetailsISBN.Name = "LabelDetailsISBN";
            this.LabelDetailsISBN.Size = new System.Drawing.Size(127, 21);
            this.LabelDetailsISBN.TabIndex = 8;
            this.LabelDetailsISBN.Text = "9789753638029";
            // 
            // LabelDetailsName
            // 
            this.LabelDetailsName.AutoEllipsis = true;
            this.LabelDetailsName.AutoSize = true;
            this.LabelDetailsName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsName.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsName.Location = new System.Drawing.Point(3, 234);
            this.LabelDetailsName.Name = "LabelDetailsName";
            this.LabelDetailsName.Size = new System.Drawing.Size(194, 21);
            this.LabelDetailsName.TabIndex = 7;
            this.LabelDetailsName.Text = "Kürk Mantolu Madonna";
            // 
            // LabelDetailsAuthor
            // 
            this.LabelDetailsAuthor.AutoEllipsis = true;
            this.LabelDetailsAuthor.AutoSize = true;
            this.LabelDetailsAuthor.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsAuthor.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsAuthor.Location = new System.Drawing.Point(231, 234);
            this.LabelDetailsAuthor.Name = "LabelDetailsAuthor";
            this.LabelDetailsAuthor.Size = new System.Drawing.Size(122, 21);
            this.LabelDetailsAuthor.TabIndex = 6;
            this.LabelDetailsAuthor.Text = "Sabahattin Ali";
            // 
            // LabelDetailsPublisherText
            // 
            this.LabelDetailsPublisherText.AutoSize = true;
            this.LabelDetailsPublisherText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPublisherText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsPublisherText.Location = new System.Drawing.Point(232, 265);
            this.LabelDetailsPublisherText.Name = "LabelDetailsPublisherText";
            this.LabelDetailsPublisherText.Size = new System.Drawing.Size(57, 15);
            this.LabelDetailsPublisherText.TabIndex = 5;
            this.LabelDetailsPublisherText.Text = "Publisher";
            // 
            // PictureBoxDetails
            // 
            this.PictureBoxDetails.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxDetails.Image")));
            this.PictureBoxDetails.Location = new System.Drawing.Point(4, 4);
            this.PictureBoxDetails.Name = "PictureBoxDetails";
            this.PictureBoxDetails.Size = new System.Drawing.Size(160, 160);
            this.PictureBoxDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxDetails.TabIndex = 0;
            this.PictureBoxDetails.TabStop = false;
            // 
            // LabelPanelDetailsText
            // 
            this.LabelPanelDetailsText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelPanelDetailsText.AutoEllipsis = true;
            this.LabelPanelDetailsText.AutoSize = true;
            this.LabelPanelDetailsText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelPanelDetailsText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelPanelDetailsText.Location = new System.Drawing.Point(435, 61);
            this.LabelPanelDetailsText.Name = "LabelPanelDetailsText";
            this.LabelPanelDetailsText.Size = new System.Drawing.Size(59, 19);
            this.LabelPanelDetailsText.TabIndex = 18;
            this.LabelPanelDetailsText.Text = "Details";
            this.LabelPanelDetailsText.Visible = false;
            // 
            // LabelTableLayoutText
            // 
            this.LabelTableLayoutText.AutoSize = true;
            this.LabelTableLayoutText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelTableLayoutText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelTableLayoutText.Location = new System.Drawing.Point(15, 61);
            this.LabelTableLayoutText.Name = "LabelTableLayoutText";
            this.LabelTableLayoutText.Size = new System.Drawing.Size(53, 19);
            this.LabelTableLayoutText.TabIndex = 20;
            this.LabelTableLayoutText.Text = "Books";
            // 
            // GroupBoxBooks
            // 
            this.GroupBoxBooks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxBooks.Controls.Add(this.TableLayoutPanelBooks);
            this.GroupBoxBooks.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GroupBoxBooks.Location = new System.Drawing.Point(18, 83);
            this.GroupBoxBooks.Name = "GroupBoxBooks";
            this.GroupBoxBooks.Size = new System.Drawing.Size(411, 357);
            this.GroupBoxBooks.TabIndex = 0;
            this.GroupBoxBooks.TabStop = false;
            // 
            // TableLayoutPanelBooks
            // 
            this.TableLayoutPanelBooks.AutoScroll = true;
            this.TableLayoutPanelBooks.AutoSize = true;
            this.TableLayoutPanelBooks.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanelBooks.ColumnCount = 4;
            this.TableLayoutPanelBooks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelBooks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelBooks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelBooks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelBooks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanelBooks.Location = new System.Drawing.Point(3, 16);
            this.TableLayoutPanelBooks.Name = "TableLayoutPanelBooks";
            this.TableLayoutPanelBooks.RowCount = 2;
            this.TableLayoutPanelBooks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelBooks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelBooks.Size = new System.Drawing.Size(405, 338);
            this.TableLayoutPanelBooks.TabIndex = 0;
            // 
            // PanelBookPageBottom
            // 
            this.PanelBookPageBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelBookPageBottom.Location = new System.Drawing.Point(0, 533);
            this.PanelBookPageBottom.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PanelBookPageBottom.Name = "PanelBookPageBottom";
            this.PanelBookPageBottom.Size = new System.Drawing.Size(900, 36);
            this.PanelBookPageBottom.TabIndex = 21;
            // 
            // TimerRefreshEventsBookPage
            // 
            this.TimerRefreshEventsBookPage.Enabled = true;
            this.TimerRefreshEventsBookPage.Tick += new System.EventHandler(this.TimerRefreshEventsBookPage_Tick);
            // 
            // FormBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 569);
            this.Controls.Add(this.PanelBookPageBottom);
            this.Controls.Add(this.GroupBoxBooks);
            this.Controls.Add(this.LabelTableLayoutText);
            this.Controls.Add(this.LabelPanelDetailsText);
            this.Controls.Add(this.PanelDetails);
            this.Controls.Add(this.PanelBookPageTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBook";
            this.Text = "BookPage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BookPage_FormClosing);
            this.Load += new System.EventHandler(this.BookPage_Load);
            this.DoubleClick += new System.EventHandler(this.BookPage_DoubleClick);
            this.PanelBookPageTop.ResumeLayout(false);
            this.PanelBookPageTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBookPageTop)).EndInit();
            this.PanelDetails.ResumeLayout(false);
            this.PanelDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDetails)).EndInit();
            this.GroupBoxBooks.ResumeLayout(false);
            this.GroupBoxBooks.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelBookPageTop;
        private System.Windows.Forms.Label LabelMyCartCount;
        private System.Windows.Forms.Button ButtonBookPageMyCart;
        private System.Windows.Forms.Button ButtonBookPageTopUsername;
        private System.Windows.Forms.Button ButtonBookPageExit;
        private System.Windows.Forms.Label LabelBookPageTop;
        private System.Windows.Forms.PictureBox PictureBoxBookPageTop;
        private System.Windows.Forms.Panel PanelDetails;
        private System.Windows.Forms.Label LabelDetailsISBN;
        private System.Windows.Forms.Label LabelDetailsName;
        private System.Windows.Forms.Label LabelDetailsAuthor;
        private System.Windows.Forms.Label LabelDetailsPublisherText;
        private System.Windows.Forms.PictureBox PictureBoxDetails;
        private System.Windows.Forms.Label LabelDetailsISBNText;
        private System.Windows.Forms.Label LabelDetailsPrice;
        private System.Windows.Forms.Label LabelDetailsPage;
        private System.Windows.Forms.Label LabelDetailsNameText;
        private System.Windows.Forms.Label LabelDetailsPageText;
        private System.Windows.Forms.Label LabelDetailsAuthorText;
        private System.Windows.Forms.Label LabelDetailsPublisher;
        private System.Windows.Forms.Label LabelDetailsPriceText;
        private System.Windows.Forms.Label LabelPanelDetailsText;
        private System.Windows.Forms.Label LabelTableLayoutText;
        private System.Windows.Forms.GroupBox GroupBoxBooks;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanelBooks;
        private System.Windows.Forms.Panel PanelBookPageBottom;
        private System.Windows.Forms.Timer TimerRefreshEventsBookPage;
        private System.Windows.Forms.Panel PanelSeperatorPublisher;
        private System.Windows.Forms.Panel PanelSeperatorName;
        private System.Windows.Forms.Panel PanelSeperatorISBN;
        private System.Windows.Forms.Panel PanelSeperator;
        private System.Windows.Forms.Panel PanelSeperatorPage;
        private System.Windows.Forms.Panel PanelSeperatorAuthor;
    }
}