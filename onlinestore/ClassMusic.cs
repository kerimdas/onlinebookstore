﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore
{
    /// <summary>
    /// Muzik urunu icin sinif.
    /// </summary>
    public class ClassMusic : ClassProduct
    {
        private string singer;
        private string genre;

        public enum type { HeavyMetal = 1, Rock = 2 , Romance = 3,  Country = 4, Rap = 5, Pop = 6 , Arabesque=7}

        public string Singer { set { singer = value; } get { return singer; } }
        public string Genre { set { genre = value; } get { return genre; } }
    }
}
