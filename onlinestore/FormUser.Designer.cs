﻿namespace onlinestore {
    partial class FormUser {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUser));
            this.PanelUserPageTop = new System.Windows.Forms.Panel();
            this.ButtonUserPageExit = new System.Windows.Forms.Button();
            this.LabelUserPageNameTop = new System.Windows.Forms.Label();
            this.PictureBoxUserPageTop = new System.Windows.Forms.PictureBox();
            this.LabelUserPageNotConfirmPassword = new System.Windows.Forms.Label();
            this.TextBoxUserPageName = new System.Windows.Forms.TextBox();
            this.LabelUserPageName = new System.Windows.Forms.Label();
            this.MultiTextBoxUserPageAddress = new System.Windows.Forms.TextBox();
            this.LabelUserPageAddress = new System.Windows.Forms.Label();
            this.TextBoxUserPageEmail = new System.Windows.Forms.TextBox();
            this.LabelUserPageEmail = new System.Windows.Forms.Label();
            this.TextBoxUserPageConfirmPassword = new System.Windows.Forms.TextBox();
            this.LabelUserPageConfirmPassword = new System.Windows.Forms.Label();
            this.TextBoxUserPagePassword = new System.Windows.Forms.TextBox();
            this.LabelUserPagePassword = new System.Windows.Forms.Label();
            this.TextBoxUserPageUsername = new System.Windows.Forms.TextBox();
            this.LabelUserPageUsername = new System.Windows.Forms.Label();
            this.ButtonUserPageUpdate = new System.Windows.Forms.Button();
            this.PanelUserPageUpdate = new System.Windows.Forms.Panel();
            this.PanelUserPageOrders = new System.Windows.Forms.Panel();
            this.DataGridViewOrders = new System.Windows.Forms.DataGridView();
            this.LabelUserPagePreviousOrder = new System.Windows.Forms.Label();
            this.PanelUserPageTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxUserPageTop)).BeginInit();
            this.PanelUserPageUpdate.SuspendLayout();
            this.PanelUserPageOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelUserPageTop
            // 
            this.PanelUserPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelUserPageTop.Controls.Add(this.ButtonUserPageExit);
            this.PanelUserPageTop.Controls.Add(this.LabelUserPageNameTop);
            this.PanelUserPageTop.Controls.Add(this.PictureBoxUserPageTop);
            this.PanelUserPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelUserPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelUserPageTop.Margin = new System.Windows.Forms.Padding(4);
            this.PanelUserPageTop.Name = "PanelUserPageTop";
            this.PanelUserPageTop.Size = new System.Drawing.Size(1500, 70);
            this.PanelUserPageTop.TabIndex = 1;
            // 
            // ButtonUserPageExit
            // 
            this.ButtonUserPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonUserPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonUserPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonUserPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonUserPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonUserPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonUserPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonUserPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonUserPageExit.Image")));
            this.ButtonUserPageExit.Location = new System.Drawing.Point(1430, 4);
            this.ButtonUserPageExit.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonUserPageExit.Name = "ButtonUserPageExit";
            this.ButtonUserPageExit.Size = new System.Drawing.Size(66, 61);
            this.ButtonUserPageExit.TabIndex = 3;
            this.ButtonUserPageExit.UseVisualStyleBackColor = true;
            this.ButtonUserPageExit.Click += new System.EventHandler(this.ButtonUserPageExit_Click);
            // 
            // LabelUserPageNameTop
            // 
            this.LabelUserPageNameTop.AutoSize = true;
            this.LabelUserPageNameTop.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelUserPageNameTop.ForeColor = System.Drawing.Color.White;
            this.LabelUserPageNameTop.Location = new System.Drawing.Point(92, 20);
            this.LabelUserPageNameTop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPageNameTop.Name = "LabelUserPageNameTop";
            this.LabelUserPageNameTop.Size = new System.Drawing.Size(138, 32);
            this.LabelUserPageNameTop.TabIndex = 2;
            this.LabelUserPageNameTop.Text = "Beta User";
            // 
            // PictureBoxUserPageTop
            // 
            this.PictureBoxUserPageTop.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxUserPageTop.Image")));
            this.PictureBoxUserPageTop.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxUserPageTop.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBoxUserPageTop.Name = "PictureBoxUserPageTop";
            this.PictureBoxUserPageTop.Size = new System.Drawing.Size(85, 70);
            this.PictureBoxUserPageTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxUserPageTop.TabIndex = 1;
            this.PictureBoxUserPageTop.TabStop = false;
            // 
            // LabelUserPageNotConfirmPassword
            // 
            this.LabelUserPageNotConfirmPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelUserPageNotConfirmPassword.AutoSize = true;
            this.LabelUserPageNotConfirmPassword.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold);
            this.LabelUserPageNotConfirmPassword.ForeColor = System.Drawing.Color.Red;
            this.LabelUserPageNotConfirmPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPageNotConfirmPassword.Location = new System.Drawing.Point(139, 132);
            this.LabelUserPageNotConfirmPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPageNotConfirmPassword.Name = "LabelUserPageNotConfirmPassword";
            this.LabelUserPageNotConfirmPassword.Size = new System.Drawing.Size(187, 17);
            this.LabelUserPageNotConfirmPassword.TabIndex = 42;
            this.LabelUserPageNotConfirmPassword.Text = " (Password did not match.)";
            this.LabelUserPageNotConfirmPassword.Visible = false;
            // 
            // TextBoxUserPageName
            // 
            this.TextBoxUserPageName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxUserPageName.Enabled = false;
            this.TextBoxUserPageName.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.TextBoxUserPageName.Location = new System.Drawing.Point(34, 261);
            this.TextBoxUserPageName.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TextBoxUserPageName.Name = "TextBoxUserPageName";
            this.TextBoxUserPageName.Size = new System.Drawing.Size(315, 37);
            this.TextBoxUserPageName.TabIndex = 41;
            // 
            // LabelUserPageName
            // 
            this.LabelUserPageName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelUserPageName.AutoSize = true;
            this.LabelUserPageName.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LabelUserPageName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelUserPageName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPageName.Location = new System.Drawing.Point(29, 229);
            this.LabelUserPageName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPageName.Name = "LabelUserPageName";
            this.LabelUserPageName.Size = new System.Drawing.Size(71, 23);
            this.LabelUserPageName.TabIndex = 40;
            this.LabelUserPageName.Text = "Name";
            // 
            // MultiTextBoxUserPageAddress
            // 
            this.MultiTextBoxUserPageAddress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.MultiTextBoxUserPageAddress.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.MultiTextBoxUserPageAddress.Location = new System.Drawing.Point(34, 469);
            this.MultiTextBoxUserPageAddress.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.MultiTextBoxUserPageAddress.Multiline = true;
            this.MultiTextBoxUserPageAddress.Name = "MultiTextBoxUserPageAddress";
            this.MultiTextBoxUserPageAddress.Size = new System.Drawing.Size(315, 142);
            this.MultiTextBoxUserPageAddress.TabIndex = 39;
            // 
            // LabelUserPageAddress
            // 
            this.LabelUserPageAddress.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelUserPageAddress.AutoSize = true;
            this.LabelUserPageAddress.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LabelUserPageAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelUserPageAddress.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPageAddress.Location = new System.Drawing.Point(29, 436);
            this.LabelUserPageAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPageAddress.Name = "LabelUserPageAddress";
            this.LabelUserPageAddress.Size = new System.Drawing.Size(88, 23);
            this.LabelUserPageAddress.TabIndex = 38;
            this.LabelUserPageAddress.Text = "Address";
            // 
            // TextBoxUserPageEmail
            // 
            this.TextBoxUserPageEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxUserPageEmail.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.TextBoxUserPageEmail.Location = new System.Drawing.Point(34, 365);
            this.TextBoxUserPageEmail.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TextBoxUserPageEmail.Name = "TextBoxUserPageEmail";
            this.TextBoxUserPageEmail.Size = new System.Drawing.Size(315, 37);
            this.TextBoxUserPageEmail.TabIndex = 37;
            // 
            // LabelUserPageEmail
            // 
            this.LabelUserPageEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelUserPageEmail.AutoSize = true;
            this.LabelUserPageEmail.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LabelUserPageEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelUserPageEmail.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPageEmail.Location = new System.Drawing.Point(29, 332);
            this.LabelUserPageEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPageEmail.Name = "LabelUserPageEmail";
            this.LabelUserPageEmail.Size = new System.Drawing.Size(62, 23);
            this.LabelUserPageEmail.TabIndex = 36;
            this.LabelUserPageEmail.Text = "Email";
            // 
            // TextBoxUserPageConfirmPassword
            // 
            this.TextBoxUserPageConfirmPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxUserPageConfirmPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TextBoxUserPageConfirmPassword.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.TextBoxUserPageConfirmPassword.Location = new System.Drawing.Point(358, 158);
            this.TextBoxUserPageConfirmPassword.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TextBoxUserPageConfirmPassword.Name = "TextBoxUserPageConfirmPassword";
            this.TextBoxUserPageConfirmPassword.Size = new System.Drawing.Size(316, 37);
            this.TextBoxUserPageConfirmPassword.TabIndex = 35;
            this.TextBoxUserPageConfirmPassword.UseSystemPasswordChar = true;
            // 
            // LabelUserPageConfirmPassword
            // 
            this.LabelUserPageConfirmPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelUserPageConfirmPassword.AutoSize = true;
            this.LabelUserPageConfirmPassword.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LabelUserPageConfirmPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelUserPageConfirmPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPageConfirmPassword.Location = new System.Drawing.Point(352, 126);
            this.LabelUserPageConfirmPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPageConfirmPassword.Name = "LabelUserPageConfirmPassword";
            this.LabelUserPageConfirmPassword.Size = new System.Drawing.Size(184, 23);
            this.LabelUserPageConfirmPassword.TabIndex = 34;
            this.LabelUserPageConfirmPassword.Text = "Confirm Password";
            // 
            // TextBoxUserPagePassword
            // 
            this.TextBoxUserPagePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxUserPagePassword.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.TextBoxUserPagePassword.Location = new System.Drawing.Point(34, 158);
            this.TextBoxUserPagePassword.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TextBoxUserPagePassword.Name = "TextBoxUserPagePassword";
            this.TextBoxUserPagePassword.Size = new System.Drawing.Size(315, 37);
            this.TextBoxUserPagePassword.TabIndex = 33;
            this.TextBoxUserPagePassword.UseSystemPasswordChar = true;
            // 
            // LabelUserPagePassword
            // 
            this.LabelUserPagePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelUserPagePassword.AutoSize = true;
            this.LabelUserPagePassword.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LabelUserPagePassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelUserPagePassword.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPagePassword.Location = new System.Drawing.Point(29, 125);
            this.LabelUserPagePassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPagePassword.Name = "LabelUserPagePassword";
            this.LabelUserPagePassword.Size = new System.Drawing.Size(103, 23);
            this.LabelUserPagePassword.TabIndex = 32;
            this.LabelUserPagePassword.Text = "Password";
            // 
            // TextBoxUserPageUsername
            // 
            this.TextBoxUserPageUsername.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxUserPageUsername.Enabled = false;
            this.TextBoxUserPageUsername.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.TextBoxUserPageUsername.Location = new System.Drawing.Point(34, 54);
            this.TextBoxUserPageUsername.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.TextBoxUserPageUsername.Name = "TextBoxUserPageUsername";
            this.TextBoxUserPageUsername.Size = new System.Drawing.Size(315, 37);
            this.TextBoxUserPageUsername.TabIndex = 31;
            // 
            // LabelUserPageUsername
            // 
            this.LabelUserPageUsername.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelUserPageUsername.AutoSize = true;
            this.LabelUserPageUsername.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LabelUserPageUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelUserPageUsername.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPageUsername.Location = new System.Drawing.Point(29, 21);
            this.LabelUserPageUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPageUsername.Name = "LabelUserPageUsername";
            this.LabelUserPageUsername.Size = new System.Drawing.Size(117, 23);
            this.LabelUserPageUsername.TabIndex = 30;
            this.LabelUserPageUsername.Text = "User Name";
            // 
            // ButtonUserPageUpdate
            // 
            this.ButtonUserPageUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonUserPageUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonUserPageUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonUserPageUpdate.FlatAppearance.BorderSize = 0;
            this.ButtonUserPageUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonUserPageUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonUserPageUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonUserPageUpdate.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold);
            this.ButtonUserPageUpdate.ForeColor = System.Drawing.Color.White;
            this.ButtonUserPageUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ButtonUserPageUpdate.Location = new System.Drawing.Point(34, 648);
            this.ButtonUserPageUpdate.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.ButtonUserPageUpdate.Name = "ButtonUserPageUpdate";
            this.ButtonUserPageUpdate.Size = new System.Drawing.Size(640, 45);
            this.ButtonUserPageUpdate.TabIndex = 43;
            this.ButtonUserPageUpdate.Text = "UPDATE";
            this.ButtonUserPageUpdate.UseVisualStyleBackColor = false;
            this.ButtonUserPageUpdate.Click += new System.EventHandler(this.ButtonUserPageUpdate_Click);
            // 
            // PanelUserPageUpdate
            // 
            this.PanelUserPageUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.PanelUserPageUpdate.AutoSize = true;
            this.PanelUserPageUpdate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelUserPageUpdate.Controls.Add(this.LabelUserPageUsername);
            this.PanelUserPageUpdate.Controls.Add(this.ButtonUserPageUpdate);
            this.PanelUserPageUpdate.Controls.Add(this.TextBoxUserPageUsername);
            this.PanelUserPageUpdate.Controls.Add(this.LabelUserPageNotConfirmPassword);
            this.PanelUserPageUpdate.Controls.Add(this.LabelUserPagePassword);
            this.PanelUserPageUpdate.Controls.Add(this.TextBoxUserPageName);
            this.PanelUserPageUpdate.Controls.Add(this.TextBoxUserPagePassword);
            this.PanelUserPageUpdate.Controls.Add(this.LabelUserPageName);
            this.PanelUserPageUpdate.Controls.Add(this.LabelUserPageConfirmPassword);
            this.PanelUserPageUpdate.Controls.Add(this.MultiTextBoxUserPageAddress);
            this.PanelUserPageUpdate.Controls.Add(this.TextBoxUserPageConfirmPassword);
            this.PanelUserPageUpdate.Controls.Add(this.LabelUserPageAddress);
            this.PanelUserPageUpdate.Controls.Add(this.LabelUserPageEmail);
            this.PanelUserPageUpdate.Controls.Add(this.TextBoxUserPageEmail);
            this.PanelUserPageUpdate.Location = new System.Drawing.Point(15, 90);
            this.PanelUserPageUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.PanelUserPageUpdate.Name = "PanelUserPageUpdate";
            this.PanelUserPageUpdate.Size = new System.Drawing.Size(712, 711);
            this.PanelUserPageUpdate.TabIndex = 44;
            // 
            // PanelUserPageOrders
            // 
            this.PanelUserPageOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelUserPageOrders.AutoSize = true;
            this.PanelUserPageOrders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelUserPageOrders.Controls.Add(this.LabelUserPagePreviousOrder);
            this.PanelUserPageOrders.Controls.Add(this.DataGridViewOrders);
            this.PanelUserPageOrders.Location = new System.Drawing.Point(772, 90);
            this.PanelUserPageOrders.Margin = new System.Windows.Forms.Padding(4);
            this.PanelUserPageOrders.Name = "PanelUserPageOrders";
            this.PanelUserPageOrders.Size = new System.Drawing.Size(713, 711);
            this.PanelUserPageOrders.TabIndex = 45;
            // 
            // DataGridViewOrders
            // 
            this.DataGridViewOrders.AllowUserToAddRows = false;
            this.DataGridViewOrders.AllowUserToDeleteRows = false;
            this.DataGridViewOrders.AllowUserToOrderColumns = true;
            this.DataGridViewOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataGridViewOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridViewOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewOrders.Location = new System.Drawing.Point(45, 54);
            this.DataGridViewOrders.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridViewOrders.Name = "DataGridViewOrders";
            this.DataGridViewOrders.ReadOnly = true;
            this.DataGridViewOrders.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DataGridViewOrders.RowTemplate.Height = 150;
            this.DataGridViewOrders.Size = new System.Drawing.Size(625, 596);
            this.DataGridViewOrders.TabIndex = 0;
            // 
            // LabelUserPagePreviousOrder
            // 
            this.LabelUserPagePreviousOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelUserPagePreviousOrder.AutoSize = true;
            this.LabelUserPagePreviousOrder.Font = new System.Drawing.Font("Century Gothic", 12F);
            this.LabelUserPagePreviousOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelUserPagePreviousOrder.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.LabelUserPagePreviousOrder.Location = new System.Drawing.Point(41, 21);
            this.LabelUserPagePreviousOrder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelUserPagePreviousOrder.Name = "LabelUserPagePreviousOrder";
            this.LabelUserPagePreviousOrder.Size = new System.Drawing.Size(152, 23);
            this.LabelUserPagePreviousOrder.TabIndex = 44;
            this.LabelUserPagePreviousOrder.Text = "Previous Order";
            // 
            // FormUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1500, 875);
            this.Controls.Add(this.PanelUserPageOrders);
            this.Controls.Add(this.PanelUserPageUpdate);
            this.Controls.Add(this.PanelUserPageTop);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormUser";
            this.Text = "UserPage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormUser_FormClosing);
            this.Load += new System.EventHandler(this.UserPage_Load);
            this.PanelUserPageTop.ResumeLayout(false);
            this.PanelUserPageTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxUserPageTop)).EndInit();
            this.PanelUserPageUpdate.ResumeLayout(false);
            this.PanelUserPageUpdate.PerformLayout();
            this.PanelUserPageOrders.ResumeLayout(false);
            this.PanelUserPageOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelUserPageTop;
        private System.Windows.Forms.Button ButtonUserPageExit;
        private System.Windows.Forms.Label LabelUserPageNameTop;
        private System.Windows.Forms.PictureBox PictureBoxUserPageTop;
        private System.Windows.Forms.Label LabelUserPageNotConfirmPassword;
        private System.Windows.Forms.TextBox TextBoxUserPageName;
        private System.Windows.Forms.Label LabelUserPageName;
        private System.Windows.Forms.TextBox MultiTextBoxUserPageAddress;
        private System.Windows.Forms.Label LabelUserPageAddress;
        private System.Windows.Forms.TextBox TextBoxUserPageEmail;
        private System.Windows.Forms.Label LabelUserPageEmail;
        private System.Windows.Forms.TextBox TextBoxUserPageConfirmPassword;
        private System.Windows.Forms.Label LabelUserPageConfirmPassword;
        private System.Windows.Forms.TextBox TextBoxUserPagePassword;
        private System.Windows.Forms.Label LabelUserPagePassword;
        private System.Windows.Forms.TextBox TextBoxUserPageUsername;
        private System.Windows.Forms.Label LabelUserPageUsername;
        private System.Windows.Forms.Button ButtonUserPageUpdate;
        private System.Windows.Forms.Panel PanelUserPageUpdate;
        private System.Windows.Forms.Panel PanelUserPageOrders;
        private System.Windows.Forms.DataGridView DataGridViewOrders;
        private System.Windows.Forms.Label LabelUserPagePreviousOrder;
    }
}