﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Panel tasarimi icin UserControl.
    /// </summary>
    public partial class ControlPanel : UserControl {

        private Form form;
        private Image image;
        private string title;
        private string info;
        private bool doubleClickPanel = false;

        public Form Form
        {
            set { form = value; }
            get { return form; }
        }

        public bool DoubleClickPanel
        {
            set { doubleClickPanel = value; }
            get { return doubleClickPanel; }
        }

        public string Info
        {
            get { return info; }
            set { info = value;  LabelPanelText.Text = value; }
        }
        
        public string Title
        {
            get { return title; }
            set { title = value; LabelPanelTitle.Text = value; }
        }
        
        public Image Image
        {
            get { return image; }
            set { image = value; PictureBoxPanel.Image = value; }
        }
        
        public ControlPanel() {
            InitializeComponent();
            LabelPanelTitle.Text = Title;
            LabelPanelText.Text = Text;
        }

        /// <summary>
        /// Tikladigimiz panelin basligina gore formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void Panel_DoubleClick(object sender, EventArgs e)
        {
            doubleClickPanel = true;
            form.ShowDialog();
        }

        /// <summary>
        /// Mouse'a gore renk degisimi.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void Panel_MouseEnter(object sender, EventArgs e)
        {
            this.BackColor = Color.MediumPurple;
        }

        /// <summary>
        /// Mouse'a gore renk degisimi.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void Panel_MouseLeave(object sender, EventArgs e)
        {
            this.BackColor = DefaultBackColor;
        }
    }
}
