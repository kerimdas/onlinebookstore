﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace onlinestore {
    /// <summary>
    /// Magazin icin form.
    /// </summary>
    public partial class FormMagazine : Form {

        ClassDatabase database = ClassDatabase.connect();
        public static FormMagazine InstanceMagazinePage;
        ArrayList productControls = new ArrayList();
        ClassCustomer customer = ClassCustomer.InstanceCustomer;
        ISubjectUser subjectUser;
        ClassProductType classProductType = new ClassProductType();//benim oluşturuduğum class singıl responsibility yeaah 

        public static ClassShoppingCart shoppingCart = new ClassShoppingCart();
        public FormMagazine() {
            InitializeComponent();
        }

        public FormMagazine(ClassShoppingCart shopping, ClassCustomer _customer)
        {
            InitializeComponent();
            InstanceMagazinePage = this;
            shoppingCart = shopping;
            customer = _customer;
            ButtonMagazinePageTopUsername.Text = customer.Username;
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormMagazine.", customer.Username, nameof(FormMagazine));
        }

        /// <summary>
        /// Detaylar butonu tiklandiginda urunun detaylarini gosterir.
        /// </summary>
        /// <param name="productControl">Urun tasarimini parametre olarak alir.</param>
        /// <param name="index">Indeksi parametre olarak alir.</param>
        private void DetailsButtonClicked2(ControlProduct productControl, int index)
        {
            if (productControl.ButtonViewDetailsClicked == true)
            {
                ClassMagazine magazine = new ClassMagazine();
                database.GetMagazineData(magazine, index);
                PanelDetails.Visible = true;
                LabelPanelDetailsText.Visible = true;
                PictureBoxDetails.Image = Image.FromFile("magazines/" + magazine.ID + ".jpg");
                LabelDetailsIssue.Text = magazine.Issue;
                LabelDetailsType.Text = magazine.typeName;
                LabelDetailsName.Text = magazine.Name;               
                LabelDetailsPrice.Text = magazine.Price.ToString("0.00");
                LabelDetailsPrice.Text += "₺";
            }
        }

        /// <summary>
        /// Detaylari gosterir.
        /// </summary>
        private void ViewDetail()
        {
            if (productControls.Count > 0)
            {
                for (int i = 0; i < productControls.Count; i++)
                {
                    //DetailsButtonClicked((ProductControl)productControls[i], magazineList, i);
                    DetailsButtonClicked2((ControlProduct)productControls[i], i + 1);
                    ((ControlProduct)productControls[i]).ButtonViewDetailsClicked = false;
                }
            }
        }

        /// <summary>
        /// Sepetim formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMagazinePageMyCart_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormMyCart.", customer.Username, nameof(ButtonMagazinePageMyCart_Click));
            FormMyCart myCartPage = new FormMyCart(shoppingCart, customer);
            myCartPage = FormMyCart.InstanceMyCart;
            myCartPage.ShowDialog();
        }

        private void MagazinePage_Load(object sender, EventArgs e)
        {
            FillTableLayoutPanel();
        }

        /// <summary>
        /// Toplam alinan urun adedini gosterir.
        /// </summary>
        private void MyCartCountLabel()
        {
            LabelMyCartCount.Text = shoppingCart.ItemsToPurchase.Count.ToString();
        }

        /// <summary>
        /// Zamani yeniler, sayfayi gunceller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TimerRefreshEventsMagazinePage_Tick(object sender, EventArgs e)
        {
            ViewDetail();
            MyCartCountLabel();
        }

        /// <summary>
        /// Detaylari kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void MagazinePage_DoubleClick(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is closed the details.", customer.Username, nameof(MagazinePage_DoubleClick));
            PanelDetails.Visible = false;
            LabelPanelDetailsText.Visible = false;
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMagazinePageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exited from the FormMagazine.", customer.Username, nameof(ButtonMagazinePageExit_Click));
            this.Close();
        }

        /// <summary>
        /// Kullanici formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMagazinePageTopUsername_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormUser.", customer.Username, nameof(ButtonMagazinePageTopUsername_Click));
            FormUser userPage = new FormUser(ClassCustomer.InstanceCustomer);
            userPage.ShowDialog();
        }

        /// <summary>
        /// Paneli veritabanindaki dergilerle doldurur.
        /// </summary>
        private void FillTableLayoutPanel()
        {
            if (database.Count("[MagazineTable]") > 0)
            {
                for (int i = 1; i <= database.Count("[MagazineTable]"); i++)
                {
                    int ID = 0;
                    string type = string.Empty;
                    ControlProduct productControl = new ControlProduct();
                    ClassMagazine magazine = new ClassMagazine();//bu yöntem yanlış muhtemelen
                    ID = database.ReturnID("[MagazineTable]", i);
                    database.GetMagazineData(magazine, ID);                    
                    type = classProductType.GetTypeString("Magazine");
                    magazine.CoverPicture = Image.FromFile(type + ID + ".jpg");
                    productControl.Product = magazine;
                    productControl.Product.TYPE = "Magazine";
                    productControl.NameItem = magazine.Name;
                    productControl.Price = magazine.Price;
                    productControl.ItemImage = magazine.CoverPicture;
                    productControls.Add(productControl);
                    TableLayoutPanelMagazine.Controls.Add(productControl);
                }
            }
        }

        /// <summary>
        /// Form kapanirken islemler yapar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void MagazinePage_FormClosing(object sender, FormClosingEventArgs e) {
            TableLayoutPanelMagazine.Controls.Clear();
            productControls.Clear();
        }
    }
}
