﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore
{
    /// <summary>
    /// Musteri icin bir sinif. Kullanici sinifindan turetilmistir.
    /// </summary>
    public class ClassCustomer : ClassUser
    {
        public static ClassCustomer InstanceCustomer;

        public void printCustomerDetails()
        {
            Console.WriteLine(this.ID + " " + this.Name + " " + this.Address + " " + this.Email + " ");
            //Username ile Password u da yazdırabilirsin.
        }

        /// <summary>
        /// Musterinin bilgilerini kullanici sinifindan gelen bilgilere esitler.
        /// </summary>
        public void saveCustomer()
        {
            //yerleri tam terside olabilir? ID=this.ID gibi
            this.ID = ID;
            this.Name = Name;
            this.Address = Address;
            this.Email = Email;
            //Username ve password şimdilik kalsın
        }
    }
}
