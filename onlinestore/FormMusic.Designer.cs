﻿namespace onlinestore {
    partial class FormMusic {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMusic));
            this.LabelDetailsType = new System.Windows.Forms.Label();
            this.PictureBoxDetails = new System.Windows.Forms.PictureBox();
            this.PanelMusicCDPageBottom = new System.Windows.Forms.Panel();
            this.GroupBoxMusicCD = new System.Windows.Forms.GroupBox();
            this.TableLayoutPanelMusicCD = new System.Windows.Forms.TableLayoutPanel();
            this.LabelTableLayoutText = new System.Windows.Forms.Label();
            this.LabelPanelDetailsText = new System.Windows.Forms.Label();
            this.LabelDetailsSingerText = new System.Windows.Forms.Label();
            this.LabelDetailsNameText = new System.Windows.Forms.Label();
            this.LabelDetailsPrice = new System.Windows.Forms.Label();
            this.LabelDetailsSinger = new System.Windows.Forms.Label();
            this.LabelDetailsName = new System.Windows.Forms.Label();
            this.ButtonMusicCDPageTopUsername = new System.Windows.Forms.Button();
            this.ButtonMusicCDPageExit = new System.Windows.Forms.Button();
            this.LabelMusicCDPageTop = new System.Windows.Forms.Label();
            this.PictureBoxMusicCDPageTop = new System.Windows.Forms.PictureBox();
            this.PanelMusicCDPageTop = new System.Windows.Forms.Panel();
            this.LabelMyCartCount = new System.Windows.Forms.Label();
            this.ButtonMusicCDPageMyCart = new System.Windows.Forms.Button();
            this.PanelDetails = new System.Windows.Forms.Panel();
            this.PanelSeperatorIssue = new System.Windows.Forms.Panel();
            this.PanelSeperatorTopRight = new System.Windows.Forms.Panel();
            this.PanelSeperatorType = new System.Windows.Forms.Panel();
            this.PanelSeperatorName = new System.Windows.Forms.Panel();
            this.PanelSeperatorTop = new System.Windows.Forms.Panel();
            this.LabelDetailsPriceText = new System.Windows.Forms.Label();
            this.LabelDetailsTypeText = new System.Windows.Forms.Label();
            this.TimerRefreshEventsMusicCDPage = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDetails)).BeginInit();
            this.GroupBoxMusicCD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMusicCDPageTop)).BeginInit();
            this.PanelMusicCDPageTop.SuspendLayout();
            this.PanelDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelDetailsType
            // 
            this.LabelDetailsType.AutoEllipsis = true;
            this.LabelDetailsType.AutoSize = true;
            this.LabelDetailsType.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsType.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsType.Location = new System.Drawing.Point(304, 243);
            this.LabelDetailsType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsType.Name = "LabelDetailsType";
            this.LabelDetailsType.Size = new System.Drawing.Size(146, 23);
            this.LabelDetailsType.TabIndex = 6;
            this.LabelDetailsType.Text = "Sabahattin Ali";
            // 
            // PictureBoxDetails
            // 
            this.PictureBoxDetails.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxDetails.Image")));
            this.PictureBoxDetails.Location = new System.Drawing.Point(5, 5);
            this.PictureBoxDetails.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBoxDetails.Name = "PictureBoxDetails";
            this.PictureBoxDetails.Size = new System.Drawing.Size(213, 197);
            this.PictureBoxDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxDetails.TabIndex = 0;
            this.PictureBoxDetails.TabStop = false;
            // 
            // PanelMusicCDPageBottom
            // 
            this.PanelMusicCDPageBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelMusicCDPageBottom.Location = new System.Drawing.Point(0, 609);
            this.PanelMusicCDPageBottom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelMusicCDPageBottom.Name = "PanelMusicCDPageBottom";
            this.PanelMusicCDPageBottom.Size = new System.Drawing.Size(1182, 44);
            this.PanelMusicCDPageBottom.TabIndex = 33;
            // 
            // GroupBoxMusicCD
            // 
            this.GroupBoxMusicCD.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxMusicCD.Controls.Add(this.TableLayoutPanelMusicCD);
            this.GroupBoxMusicCD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GroupBoxMusicCD.Location = new System.Drawing.Point(24, 102);
            this.GroupBoxMusicCD.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBoxMusicCD.Name = "GroupBoxMusicCD";
            this.GroupBoxMusicCD.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBoxMusicCD.Size = new System.Drawing.Size(548, 439);
            this.GroupBoxMusicCD.TabIndex = 28;
            this.GroupBoxMusicCD.TabStop = false;
            // 
            // TableLayoutPanelMusicCD
            // 
            this.TableLayoutPanelMusicCD.AutoScroll = true;
            this.TableLayoutPanelMusicCD.AutoSize = true;
            this.TableLayoutPanelMusicCD.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanelMusicCD.ColumnCount = 4;
            this.TableLayoutPanelMusicCD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMusicCD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMusicCD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMusicCD.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMusicCD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanelMusicCD.Location = new System.Drawing.Point(4, 20);
            this.TableLayoutPanelMusicCD.Margin = new System.Windows.Forms.Padding(4);
            this.TableLayoutPanelMusicCD.Name = "TableLayoutPanelMusicCD";
            this.TableLayoutPanelMusicCD.RowCount = 2;
            this.TableLayoutPanelMusicCD.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelMusicCD.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelMusicCD.Size = new System.Drawing.Size(540, 415);
            this.TableLayoutPanelMusicCD.TabIndex = 0;
            // 
            // LabelTableLayoutText
            // 
            this.LabelTableLayoutText.AutoSize = true;
            this.LabelTableLayoutText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelTableLayoutText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelTableLayoutText.Location = new System.Drawing.Point(20, 75);
            this.LabelTableLayoutText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelTableLayoutText.Name = "LabelTableLayoutText";
            this.LabelTableLayoutText.Size = new System.Drawing.Size(116, 23);
            this.LabelTableLayoutText.TabIndex = 32;
            this.LabelTableLayoutText.Text = "Music CD\'s";
            // 
            // LabelPanelDetailsText
            // 
            this.LabelPanelDetailsText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelPanelDetailsText.AutoEllipsis = true;
            this.LabelPanelDetailsText.AutoSize = true;
            this.LabelPanelDetailsText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelPanelDetailsText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelPanelDetailsText.Location = new System.Drawing.Point(580, 75);
            this.LabelPanelDetailsText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelPanelDetailsText.Name = "LabelPanelDetailsText";
            this.LabelPanelDetailsText.Size = new System.Drawing.Size(75, 23);
            this.LabelPanelDetailsText.TabIndex = 31;
            this.LabelPanelDetailsText.Text = "Details";
            this.LabelPanelDetailsText.Visible = false;
            // 
            // LabelDetailsSingerText
            // 
            this.LabelDetailsSingerText.AutoSize = true;
            this.LabelDetailsSingerText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsSingerText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsSingerText.Location = new System.Drawing.Point(1, 281);
            this.LabelDetailsSingerText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsSingerText.Name = "LabelDetailsSingerText";
            this.LabelDetailsSingerText.Size = new System.Drawing.Size(49, 17);
            this.LabelDetailsSingerText.TabIndex = 14;
            this.LabelDetailsSingerText.Text = "Singer";
            // 
            // LabelDetailsNameText
            // 
            this.LabelDetailsNameText.AutoSize = true;
            this.LabelDetailsNameText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsNameText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsNameText.Location = new System.Drawing.Point(1, 225);
            this.LabelDetailsNameText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsNameText.Name = "LabelDetailsNameText";
            this.LabelDetailsNameText.Size = new System.Drawing.Size(50, 17);
            this.LabelDetailsNameText.TabIndex = 13;
            this.LabelDetailsNameText.Text = "Name";
            // 
            // LabelDetailsPrice
            // 
            this.LabelDetailsPrice.AutoSize = true;
            this.LabelDetailsPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelDetailsPrice.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelDetailsPrice.Location = new System.Drawing.Point(227, 64);
            this.LabelDetailsPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsPrice.Name = "LabelDetailsPrice";
            this.LabelDetailsPrice.Size = new System.Drawing.Size(185, 72);
            this.LabelDetailsPrice.TabIndex = 11;
            this.LabelDetailsPrice.Text = "5,63₺";
            // 
            // LabelDetailsSinger
            // 
            this.LabelDetailsSinger.AutoSize = true;
            this.LabelDetailsSinger.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsSinger.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsSinger.Location = new System.Drawing.Point(0, 300);
            this.LabelDetailsSinger.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsSinger.Name = "LabelDetailsSinger";
            this.LabelDetailsSinger.Size = new System.Drawing.Size(43, 23);
            this.LabelDetailsSinger.TabIndex = 9;
            this.LabelDetailsSinger.Text = "164";
            // 
            // LabelDetailsName
            // 
            this.LabelDetailsName.AutoEllipsis = true;
            this.LabelDetailsName.AutoSize = true;
            this.LabelDetailsName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsName.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsName.Location = new System.Drawing.Point(0, 243);
            this.LabelDetailsName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsName.Name = "LabelDetailsName";
            this.LabelDetailsName.Size = new System.Drawing.Size(238, 23);
            this.LabelDetailsName.TabIndex = 7;
            this.LabelDetailsName.Text = "Kürk Mantolu Madonna";
            // 
            // ButtonMusicCDPageTopUsername
            // 
            this.ButtonMusicCDPageTopUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMusicCDPageTopUsername.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMusicCDPageTopUsername.FlatAppearance.BorderSize = 0;
            this.ButtonMusicCDPageTopUsername.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMusicCDPageTopUsername.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMusicCDPageTopUsername.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMusicCDPageTopUsername.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMusicCDPageTopUsername.ForeColor = System.Drawing.Color.White;
            this.ButtonMusicCDPageTopUsername.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMusicCDPageTopUsername.Image")));
            this.ButtonMusicCDPageTopUsername.Location = new System.Drawing.Point(934, 4);
            this.ButtonMusicCDPageTopUsername.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonMusicCDPageTopUsername.Name = "ButtonMusicCDPageTopUsername";
            this.ButtonMusicCDPageTopUsername.Size = new System.Drawing.Size(165, 62);
            this.ButtonMusicCDPageTopUsername.TabIndex = 3;
            this.ButtonMusicCDPageTopUsername.Text = "Beta User";
            this.ButtonMusicCDPageTopUsername.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonMusicCDPageTopUsername.UseVisualStyleBackColor = true;
            this.ButtonMusicCDPageTopUsername.Click += new System.EventHandler(this.ButtonMusicCDPageTopUsername_Click);
            // 
            // ButtonMusicCDPageExit
            // 
            this.ButtonMusicCDPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMusicCDPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMusicCDPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonMusicCDPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMusicCDPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMusicCDPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMusicCDPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonMusicCDPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMusicCDPageExit.Image")));
            this.ButtonMusicCDPageExit.Location = new System.Drawing.Point(1107, 5);
            this.ButtonMusicCDPageExit.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonMusicCDPageExit.Name = "ButtonMusicCDPageExit";
            this.ButtonMusicCDPageExit.Size = new System.Drawing.Size(71, 60);
            this.ButtonMusicCDPageExit.TabIndex = 1;
            this.ButtonMusicCDPageExit.UseVisualStyleBackColor = true;
            this.ButtonMusicCDPageExit.Click += new System.EventHandler(this.ButtonMusicCDPageExit_Click);
            // 
            // LabelMusicCDPageTop
            // 
            this.LabelMusicCDPageTop.AutoSize = true;
            this.LabelMusicCDPageTop.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMusicCDPageTop.ForeColor = System.Drawing.Color.White;
            this.LabelMusicCDPageTop.Location = new System.Drawing.Point(99, 20);
            this.LabelMusicCDPageTop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelMusicCDPageTop.Name = "LabelMusicCDPageTop";
            this.LabelMusicCDPageTop.Size = new System.Drawing.Size(246, 32);
            this.LabelMusicCDPageTop.TabIndex = 2;
            this.LabelMusicCDPageTop.Text = "Online Book Store";
            // 
            // PictureBoxMusicCDPageTop
            // 
            this.PictureBoxMusicCDPageTop.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxMusicCDPageTop.Image")));
            this.PictureBoxMusicCDPageTop.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxMusicCDPageTop.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBoxMusicCDPageTop.Name = "PictureBoxMusicCDPageTop";
            this.PictureBoxMusicCDPageTop.Size = new System.Drawing.Size(91, 69);
            this.PictureBoxMusicCDPageTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxMusicCDPageTop.TabIndex = 1;
            this.PictureBoxMusicCDPageTop.TabStop = false;
            // 
            // PanelMusicCDPageTop
            // 
            this.PanelMusicCDPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelMusicCDPageTop.Controls.Add(this.LabelMyCartCount);
            this.PanelMusicCDPageTop.Controls.Add(this.ButtonMusicCDPageMyCart);
            this.PanelMusicCDPageTop.Controls.Add(this.ButtonMusicCDPageTopUsername);
            this.PanelMusicCDPageTop.Controls.Add(this.ButtonMusicCDPageExit);
            this.PanelMusicCDPageTop.Controls.Add(this.LabelMusicCDPageTop);
            this.PanelMusicCDPageTop.Controls.Add(this.PictureBoxMusicCDPageTop);
            this.PanelMusicCDPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMusicCDPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelMusicCDPageTop.Margin = new System.Windows.Forms.Padding(4);
            this.PanelMusicCDPageTop.Name = "PanelMusicCDPageTop";
            this.PanelMusicCDPageTop.Size = new System.Drawing.Size(1182, 69);
            this.PanelMusicCDPageTop.TabIndex = 29;
            // 
            // LabelMyCartCount
            // 
            this.LabelMyCartCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelMyCartCount.AutoSize = true;
            this.LabelMyCartCount.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMyCartCount.ForeColor = System.Drawing.Color.White;
            this.LabelMyCartCount.Location = new System.Drawing.Point(886, 7);
            this.LabelMyCartCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelMyCartCount.Name = "LabelMyCartCount";
            this.LabelMyCartCount.Size = new System.Drawing.Size(19, 19);
            this.LabelMyCartCount.TabIndex = 5;
            this.LabelMyCartCount.Text = "0";
            // 
            // ButtonMusicCDPageMyCart
            // 
            this.ButtonMusicCDPageMyCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMusicCDPageMyCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMusicCDPageMyCart.FlatAppearance.BorderSize = 0;
            this.ButtonMusicCDPageMyCart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMusicCDPageMyCart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMusicCDPageMyCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMusicCDPageMyCart.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMusicCDPageMyCart.ForeColor = System.Drawing.Color.White;
            this.ButtonMusicCDPageMyCart.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMusicCDPageMyCart.Image")));
            this.ButtonMusicCDPageMyCart.Location = new System.Drawing.Point(842, 4);
            this.ButtonMusicCDPageMyCart.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonMusicCDPageMyCart.Name = "ButtonMusicCDPageMyCart";
            this.ButtonMusicCDPageMyCart.Size = new System.Drawing.Size(78, 62);
            this.ButtonMusicCDPageMyCart.TabIndex = 4;
            this.ButtonMusicCDPageMyCart.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ButtonMusicCDPageMyCart.UseVisualStyleBackColor = true;
            this.ButtonMusicCDPageMyCart.Click += new System.EventHandler(this.ButtonMusicCDPageMyCart_Click);
            // 
            // PanelDetails
            // 
            this.PanelDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelDetails.BackColor = System.Drawing.Color.White;
            this.PanelDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelDetails.Controls.Add(this.PanelSeperatorIssue);
            this.PanelDetails.Controls.Add(this.PanelSeperatorTopRight);
            this.PanelDetails.Controls.Add(this.PanelSeperatorType);
            this.PanelDetails.Controls.Add(this.PanelSeperatorName);
            this.PanelDetails.Controls.Add(this.PanelSeperatorTop);
            this.PanelDetails.Controls.Add(this.LabelDetailsPriceText);
            this.PanelDetails.Controls.Add(this.LabelDetailsTypeText);
            this.PanelDetails.Controls.Add(this.LabelDetailsSingerText);
            this.PanelDetails.Controls.Add(this.LabelDetailsNameText);
            this.PanelDetails.Controls.Add(this.LabelDetailsPrice);
            this.PanelDetails.Controls.Add(this.LabelDetailsSinger);
            this.PanelDetails.Controls.Add(this.LabelDetailsName);
            this.PanelDetails.Controls.Add(this.LabelDetailsType);
            this.PanelDetails.Controls.Add(this.PictureBoxDetails);
            this.PanelDetails.Location = new System.Drawing.Point(580, 102);
            this.PanelDetails.Margin = new System.Windows.Forms.Padding(4);
            this.PanelDetails.Name = "PanelDetails";
            this.PanelDetails.Size = new System.Drawing.Size(574, 409);
            this.PanelDetails.TabIndex = 30;
            this.PanelDetails.Visible = false;
            // 
            // PanelSeperatorIssue
            // 
            this.PanelSeperatorIssue.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorIssue.Location = new System.Drawing.Point(5, 329);
            this.PanelSeperatorIssue.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorIssue.Name = "PanelSeperatorIssue";
            this.PanelSeperatorIssue.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorIssue.TabIndex = 20;
            // 
            // PanelSeperatorTopRight
            // 
            this.PanelSeperatorTopRight.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorTopRight.Location = new System.Drawing.Point(309, 220);
            this.PanelSeperatorTopRight.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorTopRight.Name = "PanelSeperatorTopRight";
            this.PanelSeperatorTopRight.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorTopRight.TabIndex = 21;
            // 
            // PanelSeperatorType
            // 
            this.PanelSeperatorType.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorType.Location = new System.Drawing.Point(309, 277);
            this.PanelSeperatorType.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorType.Name = "PanelSeperatorType";
            this.PanelSeperatorType.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorType.TabIndex = 20;
            // 
            // PanelSeperatorName
            // 
            this.PanelSeperatorName.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorName.Location = new System.Drawing.Point(5, 277);
            this.PanelSeperatorName.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorName.Name = "PanelSeperatorName";
            this.PanelSeperatorName.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorName.TabIndex = 19;
            // 
            // PanelSeperatorTop
            // 
            this.PanelSeperatorTop.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorTop.Location = new System.Drawing.Point(5, 220);
            this.PanelSeperatorTop.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorTop.Name = "PanelSeperatorTop";
            this.PanelSeperatorTop.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorTop.TabIndex = 18;
            // 
            // LabelDetailsPriceText
            // 
            this.LabelDetailsPriceText.AutoSize = true;
            this.LabelDetailsPriceText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPriceText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsPriceText.Location = new System.Drawing.Point(223, 46);
            this.LabelDetailsPriceText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsPriceText.Name = "LabelDetailsPriceText";
            this.LabelDetailsPriceText.Size = new System.Drawing.Size(42, 17);
            this.LabelDetailsPriceText.TabIndex = 17;
            this.LabelDetailsPriceText.Text = "Price";
            // 
            // LabelDetailsTypeText
            // 
            this.LabelDetailsTypeText.AutoSize = true;
            this.LabelDetailsTypeText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsTypeText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsTypeText.Location = new System.Drawing.Point(305, 225);
            this.LabelDetailsTypeText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsTypeText.Name = "LabelDetailsTypeText";
            this.LabelDetailsTypeText.Size = new System.Drawing.Size(39, 17);
            this.LabelDetailsTypeText.TabIndex = 15;
            this.LabelDetailsTypeText.Text = "Type";
            // 
            // TimerRefreshEventsMusicCDPage
            // 
            this.TimerRefreshEventsMusicCDPage.Enabled = true;
            this.TimerRefreshEventsMusicCDPage.Tick += new System.EventHandler(this.TimerRefreshEventsMusicCDPage_Tick);
            // 
            // MusicCDPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 653);
            this.Controls.Add(this.PanelMusicCDPageBottom);
            this.Controls.Add(this.GroupBoxMusicCD);
            this.Controls.Add(this.LabelTableLayoutText);
            this.Controls.Add(this.LabelPanelDetailsText);
            this.Controls.Add(this.PanelMusicCDPageTop);
            this.Controls.Add(this.PanelDetails);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MusicCDPage";
            this.Text = "MusicCDPage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MusicCDPage_FormClosing);
            this.Load += new System.EventHandler(this.MusicCDPage_Load);
            this.DoubleClick += new System.EventHandler(this.MusicCDPage_DoubleClick);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDetails)).EndInit();
            this.GroupBoxMusicCD.ResumeLayout(false);
            this.GroupBoxMusicCD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMusicCDPageTop)).EndInit();
            this.PanelMusicCDPageTop.ResumeLayout(false);
            this.PanelMusicCDPageTop.PerformLayout();
            this.PanelDetails.ResumeLayout(false);
            this.PanelDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelDetailsType;
        private System.Windows.Forms.PictureBox PictureBoxDetails;
        private System.Windows.Forms.Panel PanelMusicCDPageBottom;
        private System.Windows.Forms.GroupBox GroupBoxMusicCD;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanelMusicCD;
        private System.Windows.Forms.Label LabelTableLayoutText;
        private System.Windows.Forms.Label LabelPanelDetailsText;
        private System.Windows.Forms.Label LabelDetailsSingerText;
        private System.Windows.Forms.Label LabelDetailsNameText;
        private System.Windows.Forms.Label LabelDetailsPrice;
        private System.Windows.Forms.Label LabelDetailsSinger;
        private System.Windows.Forms.Label LabelDetailsName;
        private System.Windows.Forms.Button ButtonMusicCDPageTopUsername;
        private System.Windows.Forms.Button ButtonMusicCDPageExit;
        private System.Windows.Forms.Label LabelMusicCDPageTop;
        private System.Windows.Forms.PictureBox PictureBoxMusicCDPageTop;
        private System.Windows.Forms.Panel PanelMusicCDPageTop;
        private System.Windows.Forms.Label LabelMyCartCount;
        private System.Windows.Forms.Button ButtonMusicCDPageMyCart;
        private System.Windows.Forms.Panel PanelDetails;
        private System.Windows.Forms.Panel PanelSeperatorIssue;
        private System.Windows.Forms.Panel PanelSeperatorTopRight;
        private System.Windows.Forms.Panel PanelSeperatorType;
        private System.Windows.Forms.Panel PanelSeperatorName;
        private System.Windows.Forms.Panel PanelSeperatorTop;
        private System.Windows.Forms.Label LabelDetailsPriceText;
        private System.Windows.Forms.Label LabelDetailsTypeText;
        private System.Windows.Forms.Timer TimerRefreshEventsMusicCDPage;
    }
}