﻿namespace onlinestore {
    partial class FormProductAdded {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProductAdded));
            this.PanelProductAddedTop = new System.Windows.Forms.Panel();
            this.ButtonExit = new System.Windows.Forms.Button();
            this.LabelProductAddedTopText = new System.Windows.Forms.Label();
            this.PanelProductAddedPagePictureBox = new System.Windows.Forms.Panel();
            this.PictureBoxProductAddedPageTick = new System.Windows.Forms.PictureBox();
            this.LabelProductAddedPageText = new System.Windows.Forms.Label();
            this.LabelProductAddedTextBottom = new System.Windows.Forms.Label();
            this.PanelProductAddedMiddle = new System.Windows.Forms.Panel();
            this.PanelProductAddedTop.SuspendLayout();
            this.PanelProductAddedPagePictureBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxProductAddedPageTick)).BeginInit();
            this.PanelProductAddedMiddle.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelProductAddedTop
            // 
            this.PanelProductAddedTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelProductAddedTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelProductAddedTop.Controls.Add(this.ButtonExit);
            this.PanelProductAddedTop.Controls.Add(this.LabelProductAddedTopText);
            this.PanelProductAddedTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelProductAddedTop.Location = new System.Drawing.Point(0, 0);
            this.PanelProductAddedTop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PanelProductAddedTop.Name = "PanelProductAddedTop";
            this.PanelProductAddedTop.Size = new System.Drawing.Size(1200, 46);
            this.PanelProductAddedTop.TabIndex = 2;
            // 
            // ButtonExit
            // 
            this.ButtonExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonExit.FlatAppearance.BorderSize = 0;
            this.ButtonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonExit.Image")));
            this.ButtonExit.Location = new System.Drawing.Point(1101, 4);
            this.ButtonExit.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonExit.Name = "ButtonExit";
            this.ButtonExit.Size = new System.Drawing.Size(85, 38);
            this.ButtonExit.TabIndex = 1;
            this.ButtonExit.UseVisualStyleBackColor = true;
            this.ButtonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // LabelProductAddedTopText
            // 
            this.LabelProductAddedTopText.AutoSize = true;
            this.LabelProductAddedTopText.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductAddedTopText.ForeColor = System.Drawing.Color.White;
            this.LabelProductAddedTopText.Location = new System.Drawing.Point(44, 9);
            this.LabelProductAddedTopText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelProductAddedTopText.Name = "LabelProductAddedTopText";
            this.LabelProductAddedTopText.Size = new System.Drawing.Size(105, 28);
            this.LabelProductAddedTopText.TabIndex = 0;
            this.LabelProductAddedTopText.Text = "Success";
            // 
            // PanelProductAddedPagePictureBox
            // 
            this.PanelProductAddedPagePictureBox.Controls.Add(this.PictureBoxProductAddedPageTick);
            this.PanelProductAddedPagePictureBox.Location = new System.Drawing.Point(34, 30);
            this.PanelProductAddedPagePictureBox.Name = "PanelProductAddedPagePictureBox";
            this.PanelProductAddedPagePictureBox.Size = new System.Drawing.Size(300, 300);
            this.PanelProductAddedPagePictureBox.TabIndex = 3;
            // 
            // PictureBoxProductAddedPageTick
            // 
            this.PictureBoxProductAddedPageTick.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxProductAddedPageTick.Image")));
            this.PictureBoxProductAddedPageTick.InitialImage = null;
            this.PictureBoxProductAddedPageTick.Location = new System.Drawing.Point(4, 4);
            this.PictureBoxProductAddedPageTick.Name = "PictureBoxProductAddedPageTick";
            this.PictureBoxProductAddedPageTick.Size = new System.Drawing.Size(293, 293);
            this.PictureBoxProductAddedPageTick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxProductAddedPageTick.TabIndex = 0;
            this.PictureBoxProductAddedPageTick.TabStop = false;
            // 
            // LabelProductAddedPageText
            // 
            this.LabelProductAddedPageText.AutoSize = true;
            this.LabelProductAddedPageText.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductAddedPageText.ForeColor = System.Drawing.Color.Green;
            this.LabelProductAddedPageText.Location = new System.Drawing.Point(355, 93);
            this.LabelProductAddedPageText.Name = "LabelProductAddedPageText";
            this.LabelProductAddedPageText.Size = new System.Drawing.Size(645, 47);
            this.LabelProductAddedPageText.TabIndex = 4;
            this.LabelProductAddedPageText.Text = "Products succesfully purchased.";
            // 
            // LabelProductAddedTextBottom
            // 
            this.LabelProductAddedTextBottom.AutoSize = true;
            this.LabelProductAddedTextBottom.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductAddedTextBottom.ForeColor = System.Drawing.Color.Green;
            this.LabelProductAddedTextBottom.Location = new System.Drawing.Point(359, 140);
            this.LabelProductAddedTextBottom.Name = "LabelProductAddedTextBottom";
            this.LabelProductAddedTextBottom.Size = new System.Drawing.Size(248, 19);
            this.LabelProductAddedTextBottom.TabIndex = 5;
            this.LabelProductAddedTextBottom.Text = "Keep continue shopping from us. :)";
            // 
            // PanelProductAddedMiddle
            // 
            this.PanelProductAddedMiddle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelProductAddedMiddle.Controls.Add(this.PanelProductAddedPagePictureBox);
            this.PanelProductAddedMiddle.Controls.Add(this.LabelProductAddedTextBottom);
            this.PanelProductAddedMiddle.Controls.Add(this.LabelProductAddedPageText);
            this.PanelProductAddedMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelProductAddedMiddle.Location = new System.Drawing.Point(0, 46);
            this.PanelProductAddedMiddle.Name = "PanelProductAddedMiddle";
            this.PanelProductAddedMiddle.Size = new System.Drawing.Size(1200, 654);
            this.PanelProductAddedMiddle.TabIndex = 6;
            // 
            // ProductAddedPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 700);
            this.Controls.Add(this.PanelProductAddedMiddle);
            this.Controls.Add(this.PanelProductAddedTop);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ProductAddedPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ProductAddedPage";
            this.PanelProductAddedTop.ResumeLayout(false);
            this.PanelProductAddedTop.PerformLayout();
            this.PanelProductAddedPagePictureBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxProductAddedPageTick)).EndInit();
            this.PanelProductAddedMiddle.ResumeLayout(false);
            this.PanelProductAddedMiddle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelProductAddedTop;
        private System.Windows.Forms.Button ButtonExit;
        private System.Windows.Forms.Label LabelProductAddedTopText;
        private System.Windows.Forms.Panel PanelProductAddedPagePictureBox;
        private System.Windows.Forms.PictureBox PictureBoxProductAddedPageTick;
        private System.Windows.Forms.Label LabelProductAddedPageText;
        private System.Windows.Forms.Label LabelProductAddedTextBottom;
        private System.Windows.Forms.Panel PanelProductAddedMiddle;
    }
}