﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore {
    /// <summary>
    /// Dergi veritabani sinifi icin arayuz.
    /// </summary>
    public interface IDatabaseMagazine {
        void GetMagazineData(ClassMagazine magazine, int index);
        void SetMagazineData(string name, string issue, int type, float price);
    }
}
