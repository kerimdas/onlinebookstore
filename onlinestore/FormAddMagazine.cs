﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace onlinestore {
    /// <summary>
    /// Magazin eklemek icin form.
    /// </summary>
    public partial class FormAddMagazine : Form {

        ClassDatabase database = ClassDatabase.connect();
        ISubjectUser subjectUser;

        string sourceImage;

        public FormAddMagazine() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormAddMagazine.", "admin", nameof(FormAddMagazine));
        }

        /// <summary>
        /// Magazini ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddMagazinePageAdd_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is added a new magazine to the application.", "admin", nameof(ButtonAddMagazinePageAdd_Click));
            try
            {
                string appPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\magazines\";

                string name = TextBoxAddMagazinePageName.Text;
                string issue = TextBoxAddMagazinePageIssue.Text;
                int type = ComboBoxAddMagazinePageType.SelectedIndex + 1;
                double price = Convert.ToDouble(TextBoxAddMagazinePagePrice.Text);

                database.SetMagazineData(name, issue, type, (float)price);

                int ID = 0;
                ID = database.GetLastRecordID("[MagazineTable]");//yukarıda kaydettiğimizin ID sini çekeceğiz

                File.Copy(sourceImage, appPath + ID + ".jpg");//ID isimli foto kayıt olmus olacak
            }
            catch (Exception)
            {
                
            }

            this.Close();
        }

        private void AddMagazinePage_Load(object sender, EventArgs e)
        {
            foreach (var item in Enum.GetValues(typeof(ClassMagazine.type)))
            {
                ComboBoxAddMagazinePageType.Items.Add(item);
            }
        }

        /// <summary>
        /// Magazin icin resim ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddMagazinePageAddPicture_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is added a new picture to the new magazine.", "admin", nameof(ButtonAddMagazinePageAddPicture_Click));
            OpenFileDialog open = new OpenFileDialog();
            
            open.Title = "Title";
            open.Filter = "jpg files(.*jpg)|*.jpg|PNG files(*.png)|*.png|All files(.*)|.*";

            try
            {
                if (open.ShowDialog() == DialogResult.OK)
                {
                    sourceImage = open.FileName;
                    PictureBoxAddMagazinePage.Image = new Bitmap(sourceImage);
                    
                }
            }
            catch (Exception)
            {
                
            }
            
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddMagazinePageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exited from the FormAddMagazine.", "admin", nameof(ButtonAddMagazinePageExit_Click));
            this.Close();
        }

        /// <summary>
        /// Sayi disinda karakter girilmesini engeller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TextBoxAddMagazinePagePrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }
        }
    }
}
