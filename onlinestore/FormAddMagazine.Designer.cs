﻿namespace onlinestore {
    partial class FormAddMagazine {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddMagazine));
            this.ButtonAddMagazinePageAdd = new System.Windows.Forms.Button();
            this.ButtonAddMagazinePageExit = new System.Windows.Forms.Button();
            this.PanelAddMagazinePageTop = new System.Windows.Forms.Panel();
            this.LabelAddMagazinePageTop = new System.Windows.Forms.Label();
            this.PanelAddMagazinePageSidebarBottom = new System.Windows.Forms.Panel();
            this.PanelAddMagazinePageSidebarRight = new System.Windows.Forms.Panel();
            this.PanelAddMagazinePageSidebarLeft = new System.Windows.Forms.Panel();
            this.PanelAddMagazinePageMiddle = new System.Windows.Forms.Panel();
            this.ButtonAddMagazinePageAddPicture = new System.Windows.Forms.Button();
            this.TextBoxAddMagazinePagePrice = new System.Windows.Forms.TextBox();
            this.LabelAddMagazinePagePrice = new System.Windows.Forms.Label();
            this.ComboBoxAddMagazinePageType = new System.Windows.Forms.ComboBox();
            this.LabelAddMagazinePageType = new System.Windows.Forms.Label();
            this.TextBoxAddMagazinePageIssue = new System.Windows.Forms.TextBox();
            this.LabelAddMagazinePageIssue = new System.Windows.Forms.Label();
            this.TextBoxAddMagazinePageName = new System.Windows.Forms.TextBox();
            this.LabelAddMagazinePageName = new System.Windows.Forms.Label();
            this.PanelAddMagazinePagePictureBox = new System.Windows.Forms.Panel();
            this.PictureBoxAddMagazinePage = new System.Windows.Forms.PictureBox();
            this.PanelAddMagazinePageTop.SuspendLayout();
            this.PanelAddMagazinePageMiddle.SuspendLayout();
            this.PanelAddMagazinePagePictureBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAddMagazinePage)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonAddMagazinePageAdd
            // 
            this.ButtonAddMagazinePageAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonAddMagazinePageAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddMagazinePageAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddMagazinePageAdd.FlatAppearance.BorderSize = 0;
            this.ButtonAddMagazinePageAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddMagazinePageAdd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAddMagazinePageAdd.ForeColor = System.Drawing.Color.White;
            this.ButtonAddMagazinePageAdd.Location = new System.Drawing.Point(270, 448);
            this.ButtonAddMagazinePageAdd.Name = "ButtonAddMagazinePageAdd";
            this.ButtonAddMagazinePageAdd.Size = new System.Drawing.Size(250, 50);
            this.ButtonAddMagazinePageAdd.TabIndex = 15;
            this.ButtonAddMagazinePageAdd.Text = "ADD";
            this.ButtonAddMagazinePageAdd.UseVisualStyleBackColor = false;
            this.ButtonAddMagazinePageAdd.Click += new System.EventHandler(this.ButtonAddMagazinePageAdd_Click);
            // 
            // ButtonAddMagazinePageExit
            // 
            this.ButtonAddMagazinePageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonAddMagazinePageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddMagazinePageExit.FlatAppearance.BorderSize = 0;
            this.ButtonAddMagazinePageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonAddMagazinePageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonAddMagazinePageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddMagazinePageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonAddMagazinePageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAddMagazinePageExit.Image")));
            this.ButtonAddMagazinePageExit.Location = new System.Drawing.Point(739, 3);
            this.ButtonAddMagazinePageExit.Name = "ButtonAddMagazinePageExit";
            this.ButtonAddMagazinePageExit.Size = new System.Drawing.Size(53, 46);
            this.ButtonAddMagazinePageExit.TabIndex = 4;
            this.ButtonAddMagazinePageExit.UseVisualStyleBackColor = true;
            this.ButtonAddMagazinePageExit.Click += new System.EventHandler(this.ButtonAddMagazinePageExit_Click);
            // 
            // PanelAddMagazinePageTop
            // 
            this.PanelAddMagazinePageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMagazinePageTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAddMagazinePageTop.Controls.Add(this.ButtonAddMagazinePageExit);
            this.PanelAddMagazinePageTop.Controls.Add(this.LabelAddMagazinePageTop);
            this.PanelAddMagazinePageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelAddMagazinePageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelAddMagazinePageTop.Name = "PanelAddMagazinePageTop";
            this.PanelAddMagazinePageTop.Size = new System.Drawing.Size(800, 50);
            this.PanelAddMagazinePageTop.TabIndex = 5;
            // 
            // LabelAddMagazinePageTop
            // 
            this.LabelAddMagazinePageTop.AutoSize = true;
            this.LabelAddMagazinePageTop.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMagazinePageTop.ForeColor = System.Drawing.Color.White;
            this.LabelAddMagazinePageTop.Location = new System.Drawing.Point(23, 10);
            this.LabelAddMagazinePageTop.Name = "LabelAddMagazinePageTop";
            this.LabelAddMagazinePageTop.Size = new System.Drawing.Size(185, 28);
            this.LabelAddMagazinePageTop.TabIndex = 0;
            this.LabelAddMagazinePageTop.Text = "Add Magazine";
            // 
            // PanelAddMagazinePageSidebarBottom
            // 
            this.PanelAddMagazinePageSidebarBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMagazinePageSidebarBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelAddMagazinePageSidebarBottom.Location = new System.Drawing.Point(1, 549);
            this.PanelAddMagazinePageSidebarBottom.Name = "PanelAddMagazinePageSidebarBottom";
            this.PanelAddMagazinePageSidebarBottom.Size = new System.Drawing.Size(798, 1);
            this.PanelAddMagazinePageSidebarBottom.TabIndex = 2;
            // 
            // PanelAddMagazinePageSidebarRight
            // 
            this.PanelAddMagazinePageSidebarRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMagazinePageSidebarRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelAddMagazinePageSidebarRight.Location = new System.Drawing.Point(799, 0);
            this.PanelAddMagazinePageSidebarRight.Name = "PanelAddMagazinePageSidebarRight";
            this.PanelAddMagazinePageSidebarRight.Size = new System.Drawing.Size(1, 550);
            this.PanelAddMagazinePageSidebarRight.TabIndex = 1;
            // 
            // PanelAddMagazinePageSidebarLeft
            // 
            this.PanelAddMagazinePageSidebarLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddMagazinePageSidebarLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelAddMagazinePageSidebarLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelAddMagazinePageSidebarLeft.Name = "PanelAddMagazinePageSidebarLeft";
            this.PanelAddMagazinePageSidebarLeft.Size = new System.Drawing.Size(1, 550);
            this.PanelAddMagazinePageSidebarLeft.TabIndex = 0;
            // 
            // PanelAddMagazinePageMiddle
            // 
            this.PanelAddMagazinePageMiddle.BackColor = System.Drawing.Color.Silver;
            this.PanelAddMagazinePageMiddle.Controls.Add(this.ButtonAddMagazinePageAddPicture);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.TextBoxAddMagazinePagePrice);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.LabelAddMagazinePagePrice);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.ComboBoxAddMagazinePageType);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.LabelAddMagazinePageType);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.TextBoxAddMagazinePageIssue);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.LabelAddMagazinePageIssue);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.TextBoxAddMagazinePageName);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.LabelAddMagazinePageName);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.PanelAddMagazinePagePictureBox);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.ButtonAddMagazinePageAdd);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.PanelAddMagazinePageSidebarBottom);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.PanelAddMagazinePageSidebarRight);
            this.PanelAddMagazinePageMiddle.Controls.Add(this.PanelAddMagazinePageSidebarLeft);
            this.PanelAddMagazinePageMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelAddMagazinePageMiddle.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.PanelAddMagazinePageMiddle.Location = new System.Drawing.Point(0, 0);
            this.PanelAddMagazinePageMiddle.Name = "PanelAddMagazinePageMiddle";
            this.PanelAddMagazinePageMiddle.Size = new System.Drawing.Size(800, 550);
            this.PanelAddMagazinePageMiddle.TabIndex = 6;
            // 
            // ButtonAddMagazinePageAddPicture
            // 
            this.ButtonAddMagazinePageAddPicture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonAddMagazinePageAddPicture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddMagazinePageAddPicture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddMagazinePageAddPicture.FlatAppearance.BorderSize = 0;
            this.ButtonAddMagazinePageAddPicture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddMagazinePageAddPicture.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAddMagazinePageAddPicture.ForeColor = System.Drawing.Color.White;
            this.ButtonAddMagazinePageAddPicture.Location = new System.Drawing.Point(419, 346);
            this.ButtonAddMagazinePageAddPicture.Name = "ButtonAddMagazinePageAddPicture";
            this.ButtonAddMagazinePageAddPicture.Size = new System.Drawing.Size(250, 50);
            this.ButtonAddMagazinePageAddPicture.TabIndex = 26;
            this.ButtonAddMagazinePageAddPicture.Text = "Add Picture";
            this.ButtonAddMagazinePageAddPicture.UseVisualStyleBackColor = false;
            this.ButtonAddMagazinePageAddPicture.Click += new System.EventHandler(this.ButtonAddMagazinePageAddPicture_Click);
            // 
            // TextBoxAddMagazinePagePrice
            // 
            this.TextBoxAddMagazinePagePrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddMagazinePagePrice.Location = new System.Drawing.Point(98, 386);
            this.TextBoxAddMagazinePagePrice.Name = "TextBoxAddMagazinePagePrice";
            this.TextBoxAddMagazinePagePrice.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddMagazinePagePrice.TabIndex = 25;
            this.TextBoxAddMagazinePagePrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxAddMagazinePagePrice_KeyPress);
            // 
            // LabelAddMagazinePagePrice
            // 
            this.LabelAddMagazinePagePrice.AutoSize = true;
            this.LabelAddMagazinePagePrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMagazinePagePrice.ForeColor = System.Drawing.Color.White;
            this.LabelAddMagazinePagePrice.Location = new System.Drawing.Point(94, 359);
            this.LabelAddMagazinePagePrice.Name = "LabelAddMagazinePagePrice";
            this.LabelAddMagazinePagePrice.Size = new System.Drawing.Size(57, 23);
            this.LabelAddMagazinePagePrice.TabIndex = 24;
            this.LabelAddMagazinePagePrice.Text = "Price";
            // 
            // ComboBoxAddMagazinePageType
            // 
            this.ComboBoxAddMagazinePageType.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ComboBoxAddMagazinePageType.FormattingEnabled = true;
            this.ComboBoxAddMagazinePageType.Location = new System.Drawing.Point(98, 311);
            this.ComboBoxAddMagazinePageType.Name = "ComboBoxAddMagazinePageType";
            this.ComboBoxAddMagazinePageType.Size = new System.Drawing.Size(250, 31);
            this.ComboBoxAddMagazinePageType.TabIndex = 23;
            // 
            // LabelAddMagazinePageType
            // 
            this.LabelAddMagazinePageType.AutoSize = true;
            this.LabelAddMagazinePageType.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMagazinePageType.ForeColor = System.Drawing.Color.White;
            this.LabelAddMagazinePageType.Location = new System.Drawing.Point(94, 285);
            this.LabelAddMagazinePageType.Name = "LabelAddMagazinePageType";
            this.LabelAddMagazinePageType.Size = new System.Drawing.Size(56, 23);
            this.LabelAddMagazinePageType.TabIndex = 22;
            this.LabelAddMagazinePageType.Text = "Type";
            // 
            // TextBoxAddMagazinePageIssue
            // 
            this.TextBoxAddMagazinePageIssue.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddMagazinePageIssue.Location = new System.Drawing.Point(98, 227);
            this.TextBoxAddMagazinePageIssue.Name = "TextBoxAddMagazinePageIssue";
            this.TextBoxAddMagazinePageIssue.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddMagazinePageIssue.TabIndex = 21;
            // 
            // LabelAddMagazinePageIssue
            // 
            this.LabelAddMagazinePageIssue.AutoSize = true;
            this.LabelAddMagazinePageIssue.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMagazinePageIssue.ForeColor = System.Drawing.Color.White;
            this.LabelAddMagazinePageIssue.Location = new System.Drawing.Point(94, 200);
            this.LabelAddMagazinePageIssue.Name = "LabelAddMagazinePageIssue";
            this.LabelAddMagazinePageIssue.Size = new System.Drawing.Size(56, 23);
            this.LabelAddMagazinePageIssue.TabIndex = 20;
            this.LabelAddMagazinePageIssue.Text = "Issue";
            // 
            // TextBoxAddMagazinePageName
            // 
            this.TextBoxAddMagazinePageName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddMagazinePageName.Location = new System.Drawing.Point(98, 145);
            this.TextBoxAddMagazinePageName.Name = "TextBoxAddMagazinePageName";
            this.TextBoxAddMagazinePageName.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddMagazinePageName.TabIndex = 19;
            // 
            // LabelAddMagazinePageName
            // 
            this.LabelAddMagazinePageName.AutoSize = true;
            this.LabelAddMagazinePageName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddMagazinePageName.ForeColor = System.Drawing.Color.White;
            this.LabelAddMagazinePageName.Location = new System.Drawing.Point(94, 118);
            this.LabelAddMagazinePageName.Name = "LabelAddMagazinePageName";
            this.LabelAddMagazinePageName.Size = new System.Drawing.Size(71, 23);
            this.LabelAddMagazinePageName.TabIndex = 18;
            this.LabelAddMagazinePageName.Text = "Name";
            // 
            // PanelAddMagazinePagePictureBox
            // 
            this.PanelAddMagazinePagePictureBox.Controls.Add(this.PictureBoxAddMagazinePage);
            this.PanelAddMagazinePagePictureBox.Location = new System.Drawing.Point(450, 125);
            this.PanelAddMagazinePagePictureBox.Name = "PanelAddMagazinePagePictureBox";
            this.PanelAddMagazinePagePictureBox.Size = new System.Drawing.Size(200, 200);
            this.PanelAddMagazinePagePictureBox.TabIndex = 17;
            // 
            // PictureBoxAddMagazinePage
            // 
            this.PictureBoxAddMagazinePage.Location = new System.Drawing.Point(10, 10);
            this.PictureBoxAddMagazinePage.Name = "PictureBoxAddMagazinePage";
            this.PictureBoxAddMagazinePage.Size = new System.Drawing.Size(180, 180);
            this.PictureBoxAddMagazinePage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxAddMagazinePage.TabIndex = 0;
            this.PictureBoxAddMagazinePage.TabStop = false;
            // 
            // FormAddMagazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 550);
            this.Controls.Add(this.PanelAddMagazinePageTop);
            this.Controls.Add(this.PanelAddMagazinePageMiddle);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAddMagazine";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddMagazinePage";
            this.Load += new System.EventHandler(this.AddMagazinePage_Load);
            this.PanelAddMagazinePageTop.ResumeLayout(false);
            this.PanelAddMagazinePageTop.PerformLayout();
            this.PanelAddMagazinePageMiddle.ResumeLayout(false);
            this.PanelAddMagazinePageMiddle.PerformLayout();
            this.PanelAddMagazinePagePictureBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAddMagazinePage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonAddMagazinePageAdd;
        private System.Windows.Forms.Button ButtonAddMagazinePageExit;
        private System.Windows.Forms.Panel PanelAddMagazinePageTop;
        private System.Windows.Forms.Label LabelAddMagazinePageTop;
        private System.Windows.Forms.Panel PanelAddMagazinePageSidebarBottom;
        private System.Windows.Forms.Panel PanelAddMagazinePageSidebarRight;
        private System.Windows.Forms.Panel PanelAddMagazinePageSidebarLeft;
        private System.Windows.Forms.Panel PanelAddMagazinePageMiddle;
        private System.Windows.Forms.Panel PanelAddMagazinePagePictureBox;
        private System.Windows.Forms.PictureBox PictureBoxAddMagazinePage;
        private System.Windows.Forms.TextBox TextBoxAddMagazinePageIssue;
        private System.Windows.Forms.Label LabelAddMagazinePageIssue;
        private System.Windows.Forms.TextBox TextBoxAddMagazinePageName;
        private System.Windows.Forms.Label LabelAddMagazinePageName;
        private System.Windows.Forms.TextBox TextBoxAddMagazinePagePrice;
        private System.Windows.Forms.Label LabelAddMagazinePagePrice;
        private System.Windows.Forms.ComboBox ComboBoxAddMagazinePageType;
        private System.Windows.Forms.Label LabelAddMagazinePageType;
        private System.Windows.Forms.Button ButtonAddMagazinePageAddPicture;
    }
}