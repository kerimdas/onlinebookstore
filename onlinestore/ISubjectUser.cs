﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore {
    /// <summary>
    /// Subject sinifi icin arayuz.
    /// </summary>
    public interface ISubjectUser {
        void Attach(IObserverUser observerUser);
        void DeAttach(IObserverUser observerUser);
        void Notify(string data, string username, object _object);
    }
}
