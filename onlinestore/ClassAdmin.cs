﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore
{
    /// <summary>
    /// Admin icin sinif.
    /// </summary>
    class ClassAdmin
    {
        private int isAdmin = 1;

        public int IsAdmin { get { return isAdmin; } }

        public void addCustomer() { }
        public void addNewBook() { }
        public void addNewMagazine() { }
        public void addNewMusicCD() { }
    }
}
