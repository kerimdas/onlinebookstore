﻿namespace onlinestore {
    partial class FormForgotPassword {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormForgotPassword));
            this.PanelForgotPasswordTop = new System.Windows.Forms.Panel();
            this.ButtonForgotPasswordExit = new System.Windows.Forms.Button();
            this.LabelForgotPasswordTop = new System.Windows.Forms.Label();
            this.PictureBoxForgotPasswordTop = new System.Windows.Forms.PictureBox();
            this.TextBoxForgotPasswordEmail = new System.Windows.Forms.TextBox();
            this.LabelForgotPasswordEmail = new System.Windows.Forms.Label();
            this.TextBoxForgotPasswordPassword = new System.Windows.Forms.TextBox();
            this.LabelForgotPasswordPassword = new System.Windows.Forms.Label();
            this.ButtonForgotPasswordAccept = new System.Windows.Forms.Button();
            this.PanelForgotPasswordPageMiddle = new System.Windows.Forms.Panel();
            this.PanelForgotPasswordTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxForgotPasswordTop)).BeginInit();
            this.PanelForgotPasswordPageMiddle.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelForgotPasswordTop
            // 
            this.PanelForgotPasswordTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelForgotPasswordTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelForgotPasswordTop.Controls.Add(this.ButtonForgotPasswordExit);
            this.PanelForgotPasswordTop.Controls.Add(this.LabelForgotPasswordTop);
            this.PanelForgotPasswordTop.Controls.Add(this.PictureBoxForgotPasswordTop);
            this.PanelForgotPasswordTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelForgotPasswordTop.Location = new System.Drawing.Point(0, 0);
            this.PanelForgotPasswordTop.Name = "PanelForgotPasswordTop";
            this.PanelForgotPasswordTop.Size = new System.Drawing.Size(582, 56);
            this.PanelForgotPasswordTop.TabIndex = 1;
            // 
            // ButtonForgotPasswordExit
            // 
            this.ButtonForgotPasswordExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonForgotPasswordExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonForgotPasswordExit.FlatAppearance.BorderSize = 0;
            this.ButtonForgotPasswordExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonForgotPasswordExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonForgotPasswordExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonForgotPasswordExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonForgotPasswordExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonForgotPasswordExit.Image")));
            this.ButtonForgotPasswordExit.Location = new System.Drawing.Point(524, 3);
            this.ButtonForgotPasswordExit.Name = "ButtonForgotPasswordExit";
            this.ButtonForgotPasswordExit.Size = new System.Drawing.Size(53, 49);
            this.ButtonForgotPasswordExit.TabIndex = 3;
            this.ButtonForgotPasswordExit.UseVisualStyleBackColor = true;
            this.ButtonForgotPasswordExit.Click += new System.EventHandler(this.ButtonForgotPasswordExit_Click);
            // 
            // LabelForgotPasswordTop
            // 
            this.LabelForgotPasswordTop.AutoSize = true;
            this.LabelForgotPasswordTop.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelForgotPasswordTop.ForeColor = System.Drawing.Color.White;
            this.LabelForgotPasswordTop.Location = new System.Drawing.Point(74, 16);
            this.LabelForgotPasswordTop.Name = "LabelForgotPasswordTop";
            this.LabelForgotPasswordTop.Size = new System.Drawing.Size(228, 32);
            this.LabelForgotPasswordTop.TabIndex = 2;
            this.LabelForgotPasswordTop.Text = "Forgot Password";
            // 
            // PictureBoxForgotPasswordTop
            // 
            this.PictureBoxForgotPasswordTop.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxForgotPasswordTop.Image")));
            this.PictureBoxForgotPasswordTop.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxForgotPasswordTop.Name = "PictureBoxForgotPasswordTop";
            this.PictureBoxForgotPasswordTop.Size = new System.Drawing.Size(68, 56);
            this.PictureBoxForgotPasswordTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxForgotPasswordTop.TabIndex = 1;
            this.PictureBoxForgotPasswordTop.TabStop = false;
            // 
            // TextBoxForgotPasswordEmail
            // 
            this.TextBoxForgotPasswordEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxForgotPasswordEmail.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxForgotPasswordEmail.Location = new System.Drawing.Point(81, 61);
            this.TextBoxForgotPasswordEmail.Name = "TextBoxForgotPasswordEmail";
            this.TextBoxForgotPasswordEmail.Size = new System.Drawing.Size(420, 37);
            this.TextBoxForgotPasswordEmail.TabIndex = 7;
            // 
            // LabelForgotPasswordEmail
            // 
            this.LabelForgotPasswordEmail.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelForgotPasswordEmail.AutoSize = true;
            this.LabelForgotPasswordEmail.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelForgotPasswordEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelForgotPasswordEmail.Location = new System.Drawing.Point(77, 36);
            this.LabelForgotPasswordEmail.Name = "LabelForgotPasswordEmail";
            this.LabelForgotPasswordEmail.Size = new System.Drawing.Size(69, 23);
            this.LabelForgotPasswordEmail.TabIndex = 6;
            this.LabelForgotPasswordEmail.Text = "E-mail";
            // 
            // TextBoxForgotPasswordPassword
            // 
            this.TextBoxForgotPasswordPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxForgotPasswordPassword.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxForgotPasswordPassword.Location = new System.Drawing.Point(81, 135);
            this.TextBoxForgotPasswordPassword.Name = "TextBoxForgotPasswordPassword";
            this.TextBoxForgotPasswordPassword.Size = new System.Drawing.Size(420, 37);
            this.TextBoxForgotPasswordPassword.TabIndex = 9;
            this.TextBoxForgotPasswordPassword.UseSystemPasswordChar = true;
            // 
            // LabelForgotPasswordPassword
            // 
            this.LabelForgotPasswordPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelForgotPasswordPassword.AutoSize = true;
            this.LabelForgotPasswordPassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelForgotPasswordPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelForgotPasswordPassword.Location = new System.Drawing.Point(77, 111);
            this.LabelForgotPasswordPassword.Name = "LabelForgotPasswordPassword";
            this.LabelForgotPasswordPassword.Size = new System.Drawing.Size(103, 23);
            this.LabelForgotPasswordPassword.TabIndex = 8;
            this.LabelForgotPasswordPassword.Text = "Password";
            // 
            // ButtonForgotPasswordAccept
            // 
            this.ButtonForgotPasswordAccept.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonForgotPasswordAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonForgotPasswordAccept.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonForgotPasswordAccept.FlatAppearance.BorderSize = 0;
            this.ButtonForgotPasswordAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonForgotPasswordAccept.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonForgotPasswordAccept.ForeColor = System.Drawing.Color.White;
            this.ButtonForgotPasswordAccept.Location = new System.Drawing.Point(81, 197);
            this.ButtonForgotPasswordAccept.Name = "ButtonForgotPasswordAccept";
            this.ButtonForgotPasswordAccept.Size = new System.Drawing.Size(420, 38);
            this.ButtonForgotPasswordAccept.TabIndex = 11;
            this.ButtonForgotPasswordAccept.Text = "Accept";
            this.ButtonForgotPasswordAccept.UseVisualStyleBackColor = false;
            this.ButtonForgotPasswordAccept.Click += new System.EventHandler(this.ButtonForgotPasswordAccept_Click);
            // 
            // PanelForgotPasswordPageMiddle
            // 
            this.PanelForgotPasswordPageMiddle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelForgotPasswordPageMiddle.Controls.Add(this.LabelForgotPasswordEmail);
            this.PanelForgotPasswordPageMiddle.Controls.Add(this.ButtonForgotPasswordAccept);
            this.PanelForgotPasswordPageMiddle.Controls.Add(this.TextBoxForgotPasswordEmail);
            this.PanelForgotPasswordPageMiddle.Controls.Add(this.TextBoxForgotPasswordPassword);
            this.PanelForgotPasswordPageMiddle.Controls.Add(this.LabelForgotPasswordPassword);
            this.PanelForgotPasswordPageMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelForgotPasswordPageMiddle.Location = new System.Drawing.Point(0, 56);
            this.PanelForgotPasswordPageMiddle.Name = "PanelForgotPasswordPageMiddle";
            this.PanelForgotPasswordPageMiddle.Size = new System.Drawing.Size(582, 297);
            this.PanelForgotPasswordPageMiddle.TabIndex = 12;
            // 
            // ForgotPasswordPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 353);
            this.Controls.Add(this.PanelForgotPasswordPageMiddle);
            this.Controls.Add(this.PanelForgotPasswordTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ForgotPasswordPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ForgotPasswordPage";
            this.PanelForgotPasswordTop.ResumeLayout(false);
            this.PanelForgotPasswordTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxForgotPasswordTop)).EndInit();
            this.PanelForgotPasswordPageMiddle.ResumeLayout(false);
            this.PanelForgotPasswordPageMiddle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelForgotPasswordTop;
        private System.Windows.Forms.Button ButtonForgotPasswordExit;
        private System.Windows.Forms.Label LabelForgotPasswordTop;
        private System.Windows.Forms.PictureBox PictureBoxForgotPasswordTop;
        private System.Windows.Forms.TextBox TextBoxForgotPasswordEmail;
        private System.Windows.Forms.Label LabelForgotPasswordEmail;
        private System.Windows.Forms.TextBox TextBoxForgotPasswordPassword;
        private System.Windows.Forms.Label LabelForgotPasswordPassword;
        private System.Windows.Forms.Button ButtonForgotPasswordAccept;
        private System.Windows.Forms.Panel PanelForgotPasswordPageMiddle;
    }
}