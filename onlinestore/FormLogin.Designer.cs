﻿namespace onlinestore {
    partial class FormLogin {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLogin));
            this.PanelLoginPageTop = new System.Windows.Forms.Panel();
            this.ButtonLoginPageExit = new System.Windows.Forms.Button();
            this.LabelLoginPageTop = new System.Windows.Forms.Label();
            this.PictureBoxLoginPageTop = new System.Windows.Forms.PictureBox();
            this.PictureBoxLoginPageMiddle = new System.Windows.Forms.PictureBox();
            this.LabelLoginPage = new System.Windows.Forms.Label();
            this.LabelLoginPageUsername = new System.Windows.Forms.Label();
            this.TextBoxLoginPageUsername = new System.Windows.Forms.TextBox();
            this.TextBoxLoginPagePassword = new System.Windows.Forms.TextBox();
            this.LabelLoginPagePassword = new System.Windows.Forms.Label();
            this.LabelLoginPageForgotPassword = new System.Windows.Forms.Label();
            this.LabelLoginPageRegister = new System.Windows.Forms.Label();
            this.ButtonLoginPageLogin = new System.Windows.Forms.Button();
            this.PanelLoginPageTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLoginPageTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLoginPageMiddle)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelLoginPageTop
            // 
            this.PanelLoginPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelLoginPageTop.Controls.Add(this.ButtonLoginPageExit);
            this.PanelLoginPageTop.Controls.Add(this.LabelLoginPageTop);
            this.PanelLoginPageTop.Controls.Add(this.PictureBoxLoginPageTop);
            this.PanelLoginPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelLoginPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelLoginPageTop.Name = "PanelLoginPageTop";
            this.PanelLoginPageTop.Size = new System.Drawing.Size(1200, 56);
            this.PanelLoginPageTop.TabIndex = 99;
            // 
            // ButtonLoginPageExit
            // 
            this.ButtonLoginPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonLoginPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonLoginPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonLoginPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonLoginPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonLoginPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLoginPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonLoginPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonLoginPageExit.Image")));
            this.ButtonLoginPageExit.Location = new System.Drawing.Point(1144, 3);
            this.ButtonLoginPageExit.Name = "ButtonLoginPageExit";
            this.ButtonLoginPageExit.Size = new System.Drawing.Size(53, 49);
            this.ButtonLoginPageExit.TabIndex = 21;
            this.ButtonLoginPageExit.UseVisualStyleBackColor = true;
            this.ButtonLoginPageExit.Click += new System.EventHandler(this.ButtonLoginPageExit_Click);
            // 
            // LabelLoginPageTop
            // 
            this.LabelLoginPageTop.AutoSize = true;
            this.LabelLoginPageTop.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelLoginPageTop.ForeColor = System.Drawing.Color.White;
            this.LabelLoginPageTop.Location = new System.Drawing.Point(74, 16);
            this.LabelLoginPageTop.Name = "LabelLoginPageTop";
            this.LabelLoginPageTop.Size = new System.Drawing.Size(246, 32);
            this.LabelLoginPageTop.TabIndex = 25;
            this.LabelLoginPageTop.Text = "Online Book Store";
            // 
            // PictureBoxLoginPageTop
            // 
            this.PictureBoxLoginPageTop.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxLoginPageTop.Image")));
            this.PictureBoxLoginPageTop.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxLoginPageTop.Name = "PictureBoxLoginPageTop";
            this.PictureBoxLoginPageTop.Size = new System.Drawing.Size(68, 56);
            this.PictureBoxLoginPageTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxLoginPageTop.TabIndex = 1;
            this.PictureBoxLoginPageTop.TabStop = false;
            // 
            // PictureBoxLoginPageMiddle
            // 
            this.PictureBoxLoginPageMiddle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PictureBoxLoginPageMiddle.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxLoginPageMiddle.Image")));
            this.PictureBoxLoginPageMiddle.Location = new System.Drawing.Point(526, 119);
            this.PictureBoxLoginPageMiddle.Name = "PictureBoxLoginPageMiddle";
            this.PictureBoxLoginPageMiddle.Size = new System.Drawing.Size(117, 112);
            this.PictureBoxLoginPageMiddle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxLoginPageMiddle.TabIndex = 3;
            this.PictureBoxLoginPageMiddle.TabStop = false;
            // 
            // LabelLoginPage
            // 
            this.LabelLoginPage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelLoginPage.AutoSize = true;
            this.LabelLoginPage.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelLoginPage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelLoginPage.Location = new System.Drawing.Point(485, 247);
            this.LabelLoginPage.Name = "LabelLoginPage";
            this.LabelLoginPage.Size = new System.Drawing.Size(246, 32);
            this.LabelLoginPage.TabIndex = 29;
            this.LabelLoginPage.Text = "Online Book Store";
            // 
            // LabelLoginPageUsername
            // 
            this.LabelLoginPageUsername.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelLoginPageUsername.AutoSize = true;
            this.LabelLoginPageUsername.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelLoginPageUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelLoginPageUsername.Location = new System.Drawing.Point(361, 320);
            this.LabelLoginPageUsername.Name = "LabelLoginPageUsername";
            this.LabelLoginPageUsername.Size = new System.Drawing.Size(117, 23);
            this.LabelLoginPageUsername.TabIndex = 99;
            this.LabelLoginPageUsername.Text = "User Name";
            // 
            // TextBoxLoginPageUsername
            // 
            this.TextBoxLoginPageUsername.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxLoginPageUsername.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxLoginPageUsername.Location = new System.Drawing.Point(365, 344);
            this.TextBoxLoginPageUsername.Name = "TextBoxLoginPageUsername";
            this.TextBoxLoginPageUsername.Size = new System.Drawing.Size(420, 37);
            this.TextBoxLoginPageUsername.TabIndex = 0;
            // 
            // TextBoxLoginPagePassword
            // 
            this.TextBoxLoginPagePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextBoxLoginPagePassword.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxLoginPagePassword.Location = new System.Drawing.Point(365, 419);
            this.TextBoxLoginPagePassword.Name = "TextBoxLoginPagePassword";
            this.TextBoxLoginPagePassword.Size = new System.Drawing.Size(420, 37);
            this.TextBoxLoginPagePassword.TabIndex = 1;
            this.TextBoxLoginPagePassword.UseSystemPasswordChar = true;
            // 
            // LabelLoginPagePassword
            // 
            this.LabelLoginPagePassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelLoginPagePassword.AutoSize = true;
            this.LabelLoginPagePassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelLoginPagePassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelLoginPagePassword.Location = new System.Drawing.Point(361, 395);
            this.LabelLoginPagePassword.Name = "LabelLoginPagePassword";
            this.LabelLoginPagePassword.Size = new System.Drawing.Size(103, 23);
            this.LabelLoginPagePassword.TabIndex = 99;
            this.LabelLoginPagePassword.Text = "Password";
            // 
            // LabelLoginPageForgotPassword
            // 
            this.LabelLoginPageForgotPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelLoginPageForgotPassword.AutoSize = true;
            this.LabelLoginPageForgotPassword.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelLoginPageForgotPassword.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelLoginPageForgotPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelLoginPageForgotPassword.Location = new System.Drawing.Point(361, 460);
            this.LabelLoginPageForgotPassword.Name = "LabelLoginPageForgotPassword";
            this.LabelLoginPageForgotPassword.Size = new System.Drawing.Size(178, 23);
            this.LabelLoginPageForgotPassword.TabIndex = 3;
            this.LabelLoginPageForgotPassword.Text = "Forgot Password?";
            this.LabelLoginPageForgotPassword.Click += new System.EventHandler(this.LabelLoginPageForgotPassword_Click);
            // 
            // LabelLoginPageRegister
            // 
            this.LabelLoginPageRegister.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelLoginPageRegister.AutoSize = true;
            this.LabelLoginPageRegister.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelLoginPageRegister.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelLoginPageRegister.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelLoginPageRegister.Location = new System.Drawing.Point(565, 460);
            this.LabelLoginPageRegister.Name = "LabelLoginPageRegister";
            this.LabelLoginPageRegister.Size = new System.Drawing.Size(278, 23);
            this.LabelLoginPageRegister.TabIndex = 4;
            this.LabelLoginPageRegister.Text = "Are you new here? Sign up!";
            this.LabelLoginPageRegister.Click += new System.EventHandler(this.LabelLoginPageRegister_Click);
            // 
            // ButtonLoginPageLogin
            // 
            this.ButtonLoginPageLogin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonLoginPageLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonLoginPageLogin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonLoginPageLogin.FlatAppearance.BorderSize = 0;
            this.ButtonLoginPageLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonLoginPageLogin.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonLoginPageLogin.ForeColor = System.Drawing.Color.White;
            this.ButtonLoginPageLogin.Location = new System.Drawing.Point(365, 545);
            this.ButtonLoginPageLogin.Name = "ButtonLoginPageLogin";
            this.ButtonLoginPageLogin.Size = new System.Drawing.Size(420, 38);
            this.ButtonLoginPageLogin.TabIndex = 2;
            this.ButtonLoginPageLogin.Text = "Login";
            this.ButtonLoginPageLogin.UseVisualStyleBackColor = false;
            this.ButtonLoginPageLogin.Click += new System.EventHandler(this.ButtonLoginPageLogin_Click);
            // 
            // LoginPage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1200, 700);
            this.Controls.Add(this.ButtonLoginPageLogin);
            this.Controls.Add(this.LabelLoginPageRegister);
            this.Controls.Add(this.LabelLoginPageForgotPassword);
            this.Controls.Add(this.TextBoxLoginPagePassword);
            this.Controls.Add(this.LabelLoginPagePassword);
            this.Controls.Add(this.TextBoxLoginPageUsername);
            this.Controls.Add(this.LabelLoginPageUsername);
            this.Controls.Add(this.LabelLoginPage);
            this.Controls.Add(this.PictureBoxLoginPageMiddle);
            this.Controls.Add(this.PanelLoginPageTop);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginPage";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.PanelLoginPageTop.ResumeLayout(false);
            this.PanelLoginPageTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLoginPageTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLoginPageMiddle)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelLoginPageTop;
        private System.Windows.Forms.PictureBox PictureBoxLoginPageTop;
        private System.Windows.Forms.Label LabelLoginPageTop;
        private System.Windows.Forms.PictureBox PictureBoxLoginPageMiddle;
        private System.Windows.Forms.Label LabelLoginPage;
        private System.Windows.Forms.Label LabelLoginPageUsername;
        private System.Windows.Forms.TextBox TextBoxLoginPageUsername;
        private System.Windows.Forms.TextBox TextBoxLoginPagePassword;
        private System.Windows.Forms.Label LabelLoginPagePassword;
        private System.Windows.Forms.Label LabelLoginPageForgotPassword;
        private System.Windows.Forms.Label LabelLoginPageRegister;
        private System.Windows.Forms.Button ButtonLoginPageLogin;
        private System.Windows.Forms.Button ButtonLoginPageExit;
    }
}

