﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore
{
    /// <summary>
    /// Kullanici icin sinif.
    /// </summary>
    public abstract class ClassUser
    {
        private int id;
        private string name;
        private string address;
        private string email;
        private string username;//Kullanıcı bi kere username belirleyecek bi kere set edecek singleton?
        private string password;

        public int ID { set { id = value; } get { return id; } }
        public string Name { set { name = value; } get { return name; } }
        public string Address { set { address = value; } get { return address; } }
        public string Email { set { email = value; } get { return email; } }
        public string Username { set { username = value; } get { return username; } }
        public string Password { set { password = value; } get { return password; } }
    }
}
