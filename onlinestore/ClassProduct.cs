﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace onlinestore
{
    /// <summary>
    /// Urun icin sinif.
    /// </summary>
    public abstract class ClassProduct
    {
        private string name;
        private int id;
        private double price;
        private Image coverPicture;
        private string type;//tipini tutsun diye

        virtual public string Name { set { name = value; } get { return name; } }
        virtual public int ID { set { id = value; } get { return id; } }
        virtual public double Price { set { price = value; } get { return price; } }
        virtual public Image CoverPicture { set { coverPicture = value; } get { return coverPicture; } }
        
        virtual public string TYPE { set { type = value; } get { return type; } }

        public void printProperties()
        {
            Console.WriteLine(this.Name + " " + this.ID + " " + this.Price);
        }
    }
}
