﻿namespace onlinestore {
    partial class FormRegister {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRegister));
            this.ButtonRegisterPageSignUp = new System.Windows.Forms.Button();
            this.TextBoxRegisterPagePassword = new System.Windows.Forms.TextBox();
            this.LabelRegisterPagePassword = new System.Windows.Forms.Label();
            this.TextBoxRegisterPageUsername = new System.Windows.Forms.TextBox();
            this.LabelRegisterPageUsername = new System.Windows.Forms.Label();
            this.PanelRegisterPageTop = new System.Windows.Forms.Panel();
            this.ButtonRegisterPageExit = new System.Windows.Forms.Button();
            this.LabelRegisterText = new System.Windows.Forms.Label();
            this.TextBoxRegisterPageEmail = new System.Windows.Forms.TextBox();
            this.LabelRegisterPageEmail = new System.Windows.Forms.Label();
            this.TextBoxRegisterPageConfirmPassword = new System.Windows.Forms.TextBox();
            this.LabelRegisterPageConfirmPassword = new System.Windows.Forms.Label();
            this.MultiTextBoxRegisterPageAddress = new System.Windows.Forms.TextBox();
            this.LabelRegisterPageAddress = new System.Windows.Forms.Label();
            this.TextBoxRegisterPageName = new System.Windows.Forms.TextBox();
            this.LabelRegisterPageName = new System.Windows.Forms.Label();
            this.LabelRegisterPageNotConfirmPassword = new System.Windows.Forms.Label();
            this.TimerRefreshEventsRegisterPage = new System.Windows.Forms.Timer(this.components);
            this.PanelRegisterPageMiddle = new System.Windows.Forms.Panel();
            this.LabelRegisterPageTOS = new System.Windows.Forms.Label();
            this.PanelRegisterPageTop.SuspendLayout();
            this.PanelRegisterPageMiddle.SuspendLayout();
            this.SuspendLayout();
            // 
            // ButtonRegisterPageSignUp
            // 
            resources.ApplyResources(this.ButtonRegisterPageSignUp, "ButtonRegisterPageSignUp");
            this.ButtonRegisterPageSignUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonRegisterPageSignUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonRegisterPageSignUp.FlatAppearance.BorderSize = 0;
            this.ButtonRegisterPageSignUp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonRegisterPageSignUp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonRegisterPageSignUp.ForeColor = System.Drawing.Color.White;
            this.ButtonRegisterPageSignUp.Name = "ButtonRegisterPageSignUp";
            this.ButtonRegisterPageSignUp.UseVisualStyleBackColor = false;
            this.ButtonRegisterPageSignUp.Click += new System.EventHandler(this.ButtonRegisterPageSignUp_Click);
            // 
            // TextBoxRegisterPagePassword
            // 
            resources.ApplyResources(this.TextBoxRegisterPagePassword, "TextBoxRegisterPagePassword");
            this.TextBoxRegisterPagePassword.Name = "TextBoxRegisterPagePassword";
            this.TextBoxRegisterPagePassword.UseSystemPasswordChar = true;
            // 
            // LabelRegisterPagePassword
            // 
            resources.ApplyResources(this.LabelRegisterPagePassword, "LabelRegisterPagePassword");
            this.LabelRegisterPagePassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelRegisterPagePassword.Name = "LabelRegisterPagePassword";
            // 
            // TextBoxRegisterPageUsername
            // 
            resources.ApplyResources(this.TextBoxRegisterPageUsername, "TextBoxRegisterPageUsername");
            this.TextBoxRegisterPageUsername.Name = "TextBoxRegisterPageUsername";
            // 
            // LabelRegisterPageUsername
            // 
            resources.ApplyResources(this.LabelRegisterPageUsername, "LabelRegisterPageUsername");
            this.LabelRegisterPageUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelRegisterPageUsername.Name = "LabelRegisterPageUsername";
            // 
            // PanelRegisterPageTop
            // 
            this.PanelRegisterPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelRegisterPageTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRegisterPageTop.Controls.Add(this.ButtonRegisterPageExit);
            this.PanelRegisterPageTop.Controls.Add(this.LabelRegisterText);
            resources.ApplyResources(this.PanelRegisterPageTop, "PanelRegisterPageTop");
            this.PanelRegisterPageTop.Name = "PanelRegisterPageTop";
            // 
            // ButtonRegisterPageExit
            // 
            resources.ApplyResources(this.ButtonRegisterPageExit, "ButtonRegisterPageExit");
            this.ButtonRegisterPageExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonRegisterPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonRegisterPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonRegisterPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonRegisterPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonRegisterPageExit.ForeColor = System.Drawing.Color.White;
            this.ButtonRegisterPageExit.Name = "ButtonRegisterPageExit";
            this.ButtonRegisterPageExit.UseVisualStyleBackColor = false;
            this.ButtonRegisterPageExit.Click += new System.EventHandler(this.ButtonRegisterPageExit_Click);
            // 
            // LabelRegisterText
            // 
            resources.ApplyResources(this.LabelRegisterText, "LabelRegisterText");
            this.LabelRegisterText.ForeColor = System.Drawing.Color.White;
            this.LabelRegisterText.Name = "LabelRegisterText";
            // 
            // TextBoxRegisterPageEmail
            // 
            resources.ApplyResources(this.TextBoxRegisterPageEmail, "TextBoxRegisterPageEmail");
            this.TextBoxRegisterPageEmail.Name = "TextBoxRegisterPageEmail";
            // 
            // LabelRegisterPageEmail
            // 
            resources.ApplyResources(this.LabelRegisterPageEmail, "LabelRegisterPageEmail");
            this.LabelRegisterPageEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelRegisterPageEmail.Name = "LabelRegisterPageEmail";
            // 
            // TextBoxRegisterPageConfirmPassword
            // 
            resources.ApplyResources(this.TextBoxRegisterPageConfirmPassword, "TextBoxRegisterPageConfirmPassword");
            this.TextBoxRegisterPageConfirmPassword.Name = "TextBoxRegisterPageConfirmPassword";
            this.TextBoxRegisterPageConfirmPassword.UseSystemPasswordChar = true;
            // 
            // LabelRegisterPageConfirmPassword
            // 
            resources.ApplyResources(this.LabelRegisterPageConfirmPassword, "LabelRegisterPageConfirmPassword");
            this.LabelRegisterPageConfirmPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelRegisterPageConfirmPassword.Name = "LabelRegisterPageConfirmPassword";
            // 
            // MultiTextBoxRegisterPageAddress
            // 
            resources.ApplyResources(this.MultiTextBoxRegisterPageAddress, "MultiTextBoxRegisterPageAddress");
            this.MultiTextBoxRegisterPageAddress.Name = "MultiTextBoxRegisterPageAddress";
            // 
            // LabelRegisterPageAddress
            // 
            resources.ApplyResources(this.LabelRegisterPageAddress, "LabelRegisterPageAddress");
            this.LabelRegisterPageAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelRegisterPageAddress.Name = "LabelRegisterPageAddress";
            // 
            // TextBoxRegisterPageName
            // 
            resources.ApplyResources(this.TextBoxRegisterPageName, "TextBoxRegisterPageName");
            this.TextBoxRegisterPageName.Name = "TextBoxRegisterPageName";
            // 
            // LabelRegisterPageName
            // 
            resources.ApplyResources(this.LabelRegisterPageName, "LabelRegisterPageName");
            this.LabelRegisterPageName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelRegisterPageName.Name = "LabelRegisterPageName";
            // 
            // LabelRegisterPageNotConfirmPassword
            // 
            resources.ApplyResources(this.LabelRegisterPageNotConfirmPassword, "LabelRegisterPageNotConfirmPassword");
            this.LabelRegisterPageNotConfirmPassword.ForeColor = System.Drawing.Color.Red;
            this.LabelRegisterPageNotConfirmPassword.Name = "LabelRegisterPageNotConfirmPassword";
            // 
            // TimerRefreshEventsRegisterPage
            // 
            this.TimerRefreshEventsRegisterPage.Enabled = true;
            this.TimerRefreshEventsRegisterPage.Interval = 1;
            this.TimerRefreshEventsRegisterPage.Tick += new System.EventHandler(this.TimerRefreshEventsRegisterPage_Tick);
            // 
            // PanelRegisterPageMiddle
            // 
            this.PanelRegisterPageMiddle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPageTOS);
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPageUsername);
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPageNotConfirmPassword);
            this.PanelRegisterPageMiddle.Controls.Add(this.TextBoxRegisterPageUsername);
            this.PanelRegisterPageMiddle.Controls.Add(this.TextBoxRegisterPageName);
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPagePassword);
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPageName);
            this.PanelRegisterPageMiddle.Controls.Add(this.TextBoxRegisterPagePassword);
            this.PanelRegisterPageMiddle.Controls.Add(this.MultiTextBoxRegisterPageAddress);
            this.PanelRegisterPageMiddle.Controls.Add(this.ButtonRegisterPageSignUp);
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPageAddress);
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPageConfirmPassword);
            this.PanelRegisterPageMiddle.Controls.Add(this.TextBoxRegisterPageEmail);
            this.PanelRegisterPageMiddle.Controls.Add(this.TextBoxRegisterPageConfirmPassword);
            this.PanelRegisterPageMiddle.Controls.Add(this.LabelRegisterPageEmail);
            resources.ApplyResources(this.PanelRegisterPageMiddle, "PanelRegisterPageMiddle");
            this.PanelRegisterPageMiddle.Name = "PanelRegisterPageMiddle";
            // 
            // LabelRegisterPageTOS
            // 
            resources.ApplyResources(this.LabelRegisterPageTOS, "LabelRegisterPageTOS");
            this.LabelRegisterPageTOS.Name = "LabelRegisterPageTOS";
            // 
            // FormRegister
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.PanelRegisterPageMiddle);
            this.Controls.Add(this.PanelRegisterPageTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormRegister";
            this.PanelRegisterPageTop.ResumeLayout(false);
            this.PanelRegisterPageTop.PerformLayout();
            this.PanelRegisterPageMiddle.ResumeLayout(false);
            this.PanelRegisterPageMiddle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonRegisterPageSignUp;
        private System.Windows.Forms.TextBox TextBoxRegisterPagePassword;
        private System.Windows.Forms.Label LabelRegisterPagePassword;
        private System.Windows.Forms.TextBox TextBoxRegisterPageUsername;
        private System.Windows.Forms.Label LabelRegisterPageUsername;
        private System.Windows.Forms.Panel PanelRegisterPageTop;
        private System.Windows.Forms.Label LabelRegisterText;
        private System.Windows.Forms.TextBox TextBoxRegisterPageEmail;
        private System.Windows.Forms.Label LabelRegisterPageEmail;
        private System.Windows.Forms.TextBox TextBoxRegisterPageConfirmPassword;
        private System.Windows.Forms.Label LabelRegisterPageConfirmPassword;
        private System.Windows.Forms.TextBox MultiTextBoxRegisterPageAddress;
        private System.Windows.Forms.Label LabelRegisterPageAddress;
        private System.Windows.Forms.TextBox TextBoxRegisterPageName;
        private System.Windows.Forms.Label LabelRegisterPageName;
        private System.Windows.Forms.Label LabelRegisterPageNotConfirmPassword;
        private System.Windows.Forms.Button ButtonRegisterPageExit;
        private System.Windows.Forms.Timer TimerRefreshEventsRegisterPage;
        private System.Windows.Forms.Panel PanelRegisterPageMiddle;
        private System.Windows.Forms.Label LabelRegisterPageTOS;
    }
}