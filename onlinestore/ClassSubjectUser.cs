﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore {
    /// <summary>
    /// Observer tasarimi icin Subject sinifi.
    /// </summary>
    public class ClassSubjectUser : ISubjectUser {
        IList<IObserverUser> observers;

        /// <summary>
        /// Observers listesi olusturur.
        /// </summary>
        public ClassSubjectUser() {
            observers = new List<IObserverUser>();
        }

        /// <summary>
        /// Observer'i listeye ekler.
        /// </summary>
        /// <param name="observerUser"></param>
        public void Attach(IObserverUser observerUser) {
            observers.Add(observerUser);
        }

        /// <summary>
        /// Observer'i listeden cikarir.
        /// </summary>
        /// <param name="observerUser"></param>
        public void DeAttach(IObserverUser observerUser) {
            observers.Remove(observerUser);
        }

        /// <summary>
        /// Observer'i bilgilendirir.
        /// </summary>
        /// <param name="data">Veriyi parametre olarak alir.</param>
        /// <param name="username">Kullanici ismini parametre olarak alir.</param>
        /// <param name="_object">Objeyi parametre olarak alir.</param>
        public void Notify(string data, string username, object _object) {
            foreach (IObserverUser observer in observers) {
                observer.Update(data, username, _object);
            }
        }
    }
}
