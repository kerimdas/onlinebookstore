﻿namespace onlinestore {
    partial class FormPurchase {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPurchase));
            this.PanelPurchaseTop = new System.Windows.Forms.Panel();
            this.ButtonExit = new System.Windows.Forms.Button();
            this.LabelPurchaseTopText = new System.Windows.Forms.Label();
            this.PanelPurchasePage = new System.Windows.Forms.Panel();
            this.PanelSeperatorName = new System.Windows.Forms.Panel();
            this.ButtonConfirm = new System.Windows.Forms.Button();
            this.ButtonDecrease = new System.Windows.Forms.Button();
            this.ButtonIncrease = new System.Windows.Forms.Button();
            this.TextBoxQuantity = new System.Windows.Forms.TextBox();
            this.LabelProductQuantity = new System.Windows.Forms.Label();
            this.LabelProductPrice = new System.Windows.Forms.Label();
            this.LabelProductPriceText = new System.Windows.Forms.Label();
            this.LabelProductName = new System.Windows.Forms.Label();
            this.LabelProductNameText = new System.Windows.Forms.Label();
            this.PictureBoxPurchase = new System.Windows.Forms.PictureBox();
            this.PanelPurchaseTop.SuspendLayout();
            this.PanelPurchasePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPurchase)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelPurchaseTop
            // 
            this.PanelPurchaseTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelPurchaseTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPurchaseTop.Controls.Add(this.ButtonExit);
            this.PanelPurchaseTop.Controls.Add(this.LabelPurchaseTopText);
            this.PanelPurchaseTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelPurchaseTop.Location = new System.Drawing.Point(0, 0);
            this.PanelPurchaseTop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PanelPurchaseTop.Name = "PanelPurchaseTop";
            this.PanelPurchaseTop.Size = new System.Drawing.Size(536, 37);
            this.PanelPurchaseTop.TabIndex = 0;
            // 
            // ButtonExit
            // 
            this.ButtonExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonExit.FlatAppearance.BorderSize = 0;
            this.ButtonExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonExit.Image")));
            this.ButtonExit.Location = new System.Drawing.Point(460, 3);
            this.ButtonExit.Name = "ButtonExit";
            this.ButtonExit.Size = new System.Drawing.Size(64, 31);
            this.ButtonExit.TabIndex = 1;
            this.ButtonExit.UseVisualStyleBackColor = true;
            this.ButtonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // LabelPurchaseTopText
            // 
            this.LabelPurchaseTopText.AutoSize = true;
            this.LabelPurchaseTopText.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelPurchaseTopText.ForeColor = System.Drawing.Color.White;
            this.LabelPurchaseTopText.Location = new System.Drawing.Point(33, 7);
            this.LabelPurchaseTopText.Name = "LabelPurchaseTopText";
            this.LabelPurchaseTopText.Size = new System.Drawing.Size(185, 28);
            this.LabelPurchaseTopText.TabIndex = 0;
            this.LabelPurchaseTopText.Text = "Purchase Page";
            // 
            // PanelPurchasePage
            // 
            this.PanelPurchasePage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPurchasePage.Controls.Add(this.PanelSeperatorName);
            this.PanelPurchasePage.Controls.Add(this.ButtonConfirm);
            this.PanelPurchasePage.Controls.Add(this.ButtonDecrease);
            this.PanelPurchasePage.Controls.Add(this.ButtonIncrease);
            this.PanelPurchasePage.Controls.Add(this.TextBoxQuantity);
            this.PanelPurchasePage.Controls.Add(this.LabelProductQuantity);
            this.PanelPurchasePage.Controls.Add(this.LabelProductPrice);
            this.PanelPurchasePage.Controls.Add(this.LabelProductPriceText);
            this.PanelPurchasePage.Controls.Add(this.LabelProductName);
            this.PanelPurchasePage.Controls.Add(this.LabelProductNameText);
            this.PanelPurchasePage.Controls.Add(this.PictureBoxPurchase);
            this.PanelPurchasePage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelPurchasePage.Location = new System.Drawing.Point(0, 37);
            this.PanelPurchasePage.Name = "PanelPurchasePage";
            this.PanelPurchasePage.Size = new System.Drawing.Size(536, 299);
            this.PanelPurchasePage.TabIndex = 1;
            // 
            // PanelSeperatorName
            // 
            this.PanelSeperatorName.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorName.Location = new System.Drawing.Point(37, 251);
            this.PanelSeperatorName.Name = "PanelSeperatorName";
            this.PanelSeperatorName.Size = new System.Drawing.Size(200, 1);
            this.PanelSeperatorName.TabIndex = 10;
            // 
            // ButtonConfirm
            // 
            this.ButtonConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonConfirm.FlatAppearance.BorderSize = 0;
            this.ButtonConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConfirm.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonConfirm.ForeColor = System.Drawing.Color.White;
            this.ButtonConfirm.Image = ((System.Drawing.Image)(resources.GetObject("ButtonConfirm.Image")));
            this.ButtonConfirm.Location = new System.Drawing.Point(429, 223);
            this.ButtonConfirm.Name = "ButtonConfirm";
            this.ButtonConfirm.Size = new System.Drawing.Size(72, 55);
            this.ButtonConfirm.TabIndex = 9;
            this.ButtonConfirm.UseVisualStyleBackColor = false;
            this.ButtonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // ButtonDecrease
            // 
            this.ButtonDecrease.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonDecrease.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDecrease.FlatAppearance.BorderSize = 0;
            this.ButtonDecrease.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonDecrease.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonDecrease.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonDecrease.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDecrease.Image")));
            this.ButtonDecrease.Location = new System.Drawing.Point(468, 171);
            this.ButtonDecrease.Name = "ButtonDecrease";
            this.ButtonDecrease.Size = new System.Drawing.Size(33, 27);
            this.ButtonDecrease.TabIndex = 8;
            this.ButtonDecrease.UseVisualStyleBackColor = false;
            this.ButtonDecrease.Click += new System.EventHandler(this.ButtonDecrease_Click);
            // 
            // ButtonIncrease
            // 
            this.ButtonIncrease.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonIncrease.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonIncrease.FlatAppearance.BorderSize = 0;
            this.ButtonIncrease.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonIncrease.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonIncrease.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonIncrease.Image = ((System.Drawing.Image)(resources.GetObject("ButtonIncrease.Image")));
            this.ButtonIncrease.Location = new System.Drawing.Point(429, 171);
            this.ButtonIncrease.Name = "ButtonIncrease";
            this.ButtonIncrease.Size = new System.Drawing.Size(33, 27);
            this.ButtonIncrease.TabIndex = 7;
            this.ButtonIncrease.UseVisualStyleBackColor = false;
            this.ButtonIncrease.Click += new System.EventHandler(this.ButtonIncrease_Click);
            // 
            // TextBoxQuantity
            // 
            this.TextBoxQuantity.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxQuantity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.TextBoxQuantity.Location = new System.Drawing.Point(260, 171);
            this.TextBoxQuantity.Name = "TextBoxQuantity";
            this.TextBoxQuantity.Size = new System.Drawing.Size(163, 32);
            this.TextBoxQuantity.TabIndex = 6;
            this.TextBoxQuantity.Text = "1";
            this.TextBoxQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextBoxQuantity.TextChanged += new System.EventHandler(this.TextBoxQuantity_TextChanged);
            this.TextBoxQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxQuantity_KeyPress);
            // 
            // LabelProductQuantity
            // 
            this.LabelProductQuantity.AutoSize = true;
            this.LabelProductQuantity.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductQuantity.Location = new System.Drawing.Point(257, 153);
            this.LabelProductQuantity.Name = "LabelProductQuantity";
            this.LabelProductQuantity.Size = new System.Drawing.Size(73, 17);
            this.LabelProductQuantity.TabIndex = 5;
            this.LabelProductQuantity.Text = "QUANTITY";
            // 
            // LabelProductPrice
            // 
            this.LabelProductPrice.AutoSize = true;
            this.LabelProductPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelProductPrice.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductPrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelProductPrice.Location = new System.Drawing.Point(260, 56);
            this.LabelProductPrice.Name = "LabelProductPrice";
            this.LabelProductPrice.Size = new System.Drawing.Size(321, 72);
            this.LabelProductPrice.TabIndex = 4;
            this.LabelProductPrice.Text = "11111,11₺";
            // 
            // LabelProductPriceText
            // 
            this.LabelProductPriceText.AutoSize = true;
            this.LabelProductPriceText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductPriceText.Location = new System.Drawing.Point(257, 41);
            this.LabelProductPriceText.Name = "LabelProductPriceText";
            this.LabelProductPriceText.Size = new System.Drawing.Size(46, 17);
            this.LabelProductPriceText.TabIndex = 3;
            this.LabelProductPriceText.Text = "PRICE";
            // 
            // LabelProductName
            // 
            this.LabelProductName.AutoSize = true;
            this.LabelProductName.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductName.Location = new System.Drawing.Point(33, 255);
            this.LabelProductName.Name = "LabelProductName";
            this.LabelProductName.Size = new System.Drawing.Size(284, 28);
            this.LabelProductName.TabIndex = 2;
            this.LabelProductName.Text = "Kürk Mantolu Madonna";
            // 
            // LabelProductNameText
            // 
            this.LabelProductNameText.AutoSize = true;
            this.LabelProductNameText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelProductNameText.Location = new System.Drawing.Point(34, 231);
            this.LabelProductNameText.Name = "LabelProductNameText";
            this.LabelProductNameText.Size = new System.Drawing.Size(48, 17);
            this.LabelProductNameText.TabIndex = 1;
            this.LabelProductNameText.Text = "NAME";
            // 
            // PictureBoxPurchase
            // 
            this.PictureBoxPurchase.Location = new System.Drawing.Point(33, 26);
            this.PictureBoxPurchase.Name = "PictureBoxPurchase";
            this.PictureBoxPurchase.Size = new System.Drawing.Size(200, 175);
            this.PictureBoxPurchase.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxPurchase.TabIndex = 0;
            this.PictureBoxPurchase.TabStop = false;
            // 
            // FormPurchase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 336);
            this.Controls.Add(this.PanelPurchasePage);
            this.Controls.Add(this.PanelPurchaseTop);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormPurchase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PurchasePage";
            this.PanelPurchaseTop.ResumeLayout(false);
            this.PanelPurchaseTop.PerformLayout();
            this.PanelPurchasePage.ResumeLayout(false);
            this.PanelPurchasePage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPurchase)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelPurchaseTop;
        private System.Windows.Forms.Button ButtonExit;
        private System.Windows.Forms.Label LabelPurchaseTopText;
        private System.Windows.Forms.Panel PanelPurchasePage;
        private System.Windows.Forms.Button ButtonConfirm;
        private System.Windows.Forms.Button ButtonDecrease;
        private System.Windows.Forms.TextBox TextBoxQuantity;
        private System.Windows.Forms.Label LabelProductQuantity;
        private System.Windows.Forms.Label LabelProductPrice;
        private System.Windows.Forms.Label LabelProductPriceText;
        private System.Windows.Forms.Label LabelProductName;
        private System.Windows.Forms.Label LabelProductNameText;
        private System.Windows.Forms.PictureBox PictureBoxPurchase;
        private System.Windows.Forms.Panel PanelSeperatorName;
        private System.Windows.Forms.Button ButtonIncrease;
    }
}