﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

//Bitbucket

namespace onlinestore {
    /// <summary>
    /// Kitap icin form.
    /// </summary>
    public partial class FormBook : Form {

        ClassDatabase database = ClassDatabase.connect();
        ArrayList productControls = new ArrayList();
        ISubjectUser subjectUser;
        ClassCustomer customer = ClassCustomer.InstanceCustomer;

        public static FormBook InstanceBookPage;        
        public static ClassShoppingCart shoppingCart = new ClassShoppingCart();

        ClassProductType classProductType = new ClassProductType();//benim oluşturuduğum class singıl responsibility yeaah 

        public FormBook() {
            InitializeComponent();
        }
      
        public FormBook(ClassShoppingCart ShoppingCart, ClassCustomer _customer)
        {
            InitializeComponent();
            InstanceBookPage = this;
            shoppingCart = ShoppingCart;            
            customer = _customer;
            ButtonBookPageTopUsername.Text = customer.Username;
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormBook.", customer.Username, nameof(FormBook));
        }

       /// <summary>
       /// Detaylar butonu tiklandiginda urunun detaylarini gosterir.
       /// </summary>
       /// <param name="productControl">Urun tasarimini parametre olarak alir.</param>
       /// <param name="index">Indeksi parametre olarak alir.</param>
        private void DetailsButtonClicked2(ControlProduct productControl, int index)
        {
            if (productControl.ButtonViewDetailsClicked == true)
            {
                ClassBook book = new ClassBook();
                database.GetBookData(book, index);
                PanelDetails.Visible = true;
                LabelPanelDetailsText.Visible = true;
                PictureBoxDetails.Image = Image.FromFile("books/" + book.ID+".jpg");
                LabelDetailsAuthor.Text = book.Author;
                LabelDetailsISBN.Text = book.ISBN;
                LabelDetailsName.Text = book.Name;
                LabelDetailsPage.Text = book.Page.ToString();
                LabelDetailsPublisher.Text = book.Publisher;
                LabelDetailsPrice.Text = book.Price.ToString("0.00");
                LabelDetailsPrice.Text += "₺";
            }
        }

        /// <summary>
        /// Detaylari gosterir.
        /// </summary>
        private void ViewDetail()
        {
            if (productControls.Count > 0)
            {
                for (int i = 0; i < productControls.Count; i++)
                {
                    // DetailsButtonClicked((ProductControl)productControls[i], bookList, i);
                    DetailsButtonClicked2((ControlProduct)productControls[i], i+1);
                    ((ControlProduct)productControls[i]).ButtonViewDetailsClicked = false;
                }
            }
        }

        /// <summary>
        /// Zamani yeniler, sayfayi gunceller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TimerRefreshEventsBookPage_Tick(object sender, EventArgs e)
        {
            ViewDetail();
            MyCartCountLabel();
        }

        /// <summary>
        /// Toplam alinan urun adedini gosterir.
        /// </summary>
        private void MyCartCountLabel()
        {
            LabelMyCartCount.Text = shoppingCart.ItemsToPurchase.Count.ToString();
        }

        /// <summary>
        /// Sepetim formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonBookPageMyCart_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering FormMyCart.", customer.Username, nameof(ButtonBookPageMyCart_Click));
            FormMyCart myCartPage = new FormMyCart(shoppingCart, customer);
            myCartPage = FormMyCart.InstanceMyCart;
            myCartPage.ShowDialog();
        }

        /// <summary>
        /// Detaylari kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void BookPage_DoubleClick(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is closed the details.", customer.Username, nameof(BookPage_DoubleClick));
            PanelDetails.Visible = false;
            LabelPanelDetailsText.Visible = false;
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonBookPageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exited from the FormBook.", customer.Username, nameof(ButtonBookPageExit_Click));
            this.Close();
        }

        /// <summary>
        /// Kullanici formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonBookPageTopUsername_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormUser.", customer.Username, nameof(ButtonBookPageTopUsername_Click));
            FormUser userPage = new FormUser(ClassCustomer.InstanceCustomer);
            userPage.ShowDialog();
        }

        private void BookPage_Load(object sender, EventArgs e)
        {
            FillTableLayoutPanel();
        }

        /// <summary>
        /// Paneli veritabanindaki kitaplarla doldurur.
        /// </summary>
        private void FillTableLayoutPanel()
        {
            if (database.Count("[BookTable]") > 0) {
                for (int i = 1; i <= database.Count("[BookTable]"); i++) {
                    int ID = 0;
                    string type = string.Empty;
                    ControlProduct productControl = new ControlProduct();
                    ClassBook book = new ClassBook();
                    ID = database.ReturnID("[BookTable]", i);
                    database.GetBookData(book, i);
                    type = classProductType.GetTypeString("Book");
                    book.CoverPicture = Image.FromFile(type + ID + ".jpg");                    
                    productControl.Product = book;
                    productControl.Product.TYPE = "Book";
                    productControl.NameItem = book.Name;
                    productControl.Price = book.Price;
                    productControl.ItemImage = book.CoverPicture;
                    productControls.Add(productControl);
                    TableLayoutPanelBooks.Controls.Add(productControl);
                }
            }
        }

        /// <summary>
        /// Form kapanirken islemler yapar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void BookPage_FormClosing(object sender, FormClosingEventArgs e) {
            TableLayoutPanelBooks.Controls.Clear();
            productControls.Clear();
        }
    }
}
