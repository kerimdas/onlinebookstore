﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace onlinestore {
    /// <summary>
    /// Magazin veritabani icin bir sinif.
    /// </summary>
    public class ClassDatabaseMagazine : IDatabaseMagazine {
        public static SqlConnection connection = ClassDatabase.connection;

        /// <summary>
        /// Veritabanina gonderilen indeksteki magazini bulup parametreye esitler.
        /// </summary>
        /// <param name="magazine">Magazini esitlemek icin parametre.</param>
        /// <param name="index">Istenilen magazini bulmak icin parametre.</param>
        public void GetMagazineData(ClassMagazine magazine, int index) {
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [MagazineTable] WHERE ID = " + index + "", connection)) {
                command.Parameters.AddWithValue("ID", index);

                using (SqlDataReader dr = command.ExecuteReader()) {
                    dr.Read();

                    magazine.ID = Convert.ToInt32(dr["ID"]);

                    magazine.Name = dr["NAME"].ToString();

                    magazine.Issue = dr["ISSUE"].ToString();
                    int type = Convert.ToInt32(dr["TYPE"]);
                    magazine.typeName = ((ClassMagazine.type)type).ToString();
                    magazine.Price = Convert.ToDouble(dr["PRICE"]);
                }
            }
            connection.Close();
        }

        /// <summary>
        /// Magazini veritabanina ekler.
        /// </summary>
        /// <param name="name">Ismini parametre olarak alir.</param>
        /// <param name="issue">Konusunu parametre olarak alir.</param>
        /// <param name="type">Tipini parametre olarak alir.</param>
        /// <param name="price">Fiyatini parametre olarak alir.</param>
        public void SetMagazineData(string name, string issue, int type, float price) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("INSERT INTO [MagazineTable] (NAME, ISSUE, TYPE, PRICE) values(@name, @issue, @type, @price)", connection)) {
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("issue", issue);
                command.Parameters.AddWithValue("type", type);
                command.Parameters.AddWithValue("price", price);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }
    }
}
