﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace onlinestore
{
    /// <summary>
    /// Muzik icin form.
    /// </summary>
    public partial class FormMusic : Form
    {
        ClassDatabase database = ClassDatabase.connect();
        ClassCustomer customer = ClassCustomer.InstanceCustomer;
        ISubjectUser subjectUser;
        public static FormMusic InstanceMusicCDPage;
        ArrayList productControls = new ArrayList();
        ClassProductType classProductType = new ClassProductType();//benim oluşturuduğum class singıl responsibility yeaah 
        public static ClassShoppingCart shoppingCart = new ClassShoppingCart();
        
        public FormMusic()
        {
            InitializeComponent();
        }

        public FormMusic(ClassShoppingCart ShoppingCart,ClassCustomer _customer)
        {
            InitializeComponent();
            InstanceMusicCDPage = this;
            shoppingCart = ShoppingCart;
            customer = _customer;
            ButtonMusicCDPageTopUsername.Text = customer.Username;
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormMusic.", customer.Username, nameof(FormMusic));
        }

        /// <summary>
        /// Detaylar butonu tiklandiginda urunun detaylarini gosterir.
        /// </summary>
        /// <param name="productControl">Urun tasarimini parametre olarak alir.</param>
        /// <param name="i">Indeksi parametre olarak alir.</param>
        private void DetailsButtonClicked2(ControlProduct productControl, int i)
        {
            if (productControl.ButtonViewDetailsClicked == true)
            {
                int ID = database.ReturnID("[MusicTable]", i);
                ClassMusic musicCD = new ClassMusic();
                database.GetMusicCDData(musicCD, ID);
                PanelDetails.Visible = true;
                LabelPanelDetailsText.Visible = true;

                PictureBoxDetails.Image = Image.FromFile("musicCDs/" + musicCD.ID + ".jpg");
                LabelDetailsSinger.Text = musicCD.Singer;
                LabelDetailsType.Text = musicCD.Genre;
                LabelDetailsName.Text = musicCD.Name;
                
                LabelDetailsPrice.Text = musicCD.Price.ToString("0.00");
                LabelDetailsPrice.Text += "₺";
            }
        }

        /// <summary>
        /// Detaylari gosterir.
        /// </summary>
        private void ViewDetail()
        {
            if (productControls.Count > 0)
            {
                for (int i = 0; i < database.Count("[MusicTable]"); i++)
                {                 
                    DetailsButtonClicked2((ControlProduct)productControls[i], i+1);
                    ((ControlProduct)productControls[i]).ButtonViewDetailsClicked = false;
                }               
            }
        }

        /// <summary>
        /// Toplam alinan urun adedini gosterir.
        /// </summary>
        private void MyCartCountLabel()
        {
            LabelMyCartCount.Text = shoppingCart.ItemsToPurchase.Count.ToString();
        }

        /// <summary>
        /// Zamani yeniler, sayfayi gunceller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TimerRefreshEventsMusicCDPage_Tick(object sender, EventArgs e)
        {
            ViewDetail();
            MyCartCountLabel();
        }

        /// <summary>
        /// Detaylari kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void MusicCDPage_DoubleClick(object sender, EventArgs e)
        {
            PanelDetails.Visible = false;
            LabelPanelDetailsText.Visible = false;
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMusicCDPageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exited from the FormMusic.", customer.Username, nameof(ButtonMusicCDPageExit_Click));
            this.Close();
        }

        /// <summary>
        /// Kullanici formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMusicCDPageTopUsername_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormUser.", customer.Username, nameof(ButtonMusicCDPageTopUsername_Click));
            FormUser userPage = new FormUser(ClassCustomer.InstanceCustomer);
            userPage.ShowDialog();
        }

        private void MusicCDPage_Load(object sender, EventArgs e)
        {
            FillTableLayoutPanel();
        }

        /// <summary>
        /// Sepetim formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMusicCDPageMyCart_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormMyCart.", customer.Username, nameof(ButtonMusicCDPageMyCart_Click));
            FormMyCart myCartPage = new FormMyCart(shoppingCart, customer);
            myCartPage = FormMyCart.InstanceMyCart;
            myCartPage.ShowDialog();
        }

        /// <summary>
        /// Paneli veritabanindaki kitaplarla doldurur.
        /// </summary>       
        private void FillTableLayoutPanel()
        {
            if (database.Count("[MusicTable]") > 0)
            {
                for (int i = 1; i <= database.Count("[MusicTable]"); i++)
                {
                    int ID = 0;
                    string type = string.Empty;
                    ControlProduct productControl = new ControlProduct();
                    ClassMusic musicCD = new ClassMusic();//bu yöntem yanlış muhtemelen
                    ID = database.ReturnID("[MusicTable]", i);
                    database.GetMusicCDData(musicCD, ID);               
                    type=classProductType.GetTypeString("MusicCD");
                    musicCD.CoverPicture = Image.FromFile(type + ID + ".jpg");
                    productControl.Product = musicCD;
                    productControl.Product.TYPE = "MusicCD";
                    productControl.NameItem = musicCD.Name;
                    productControl.Price = musicCD.Price;
                    productControl.ItemImage = musicCD.CoverPicture;

                    productControls.Add(productControl);
                    TableLayoutPanelMusicCD.Controls.Add(productControl);
                }
            }
        }

        /// <summary>
        /// Form kapanirken islemler yapar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void MusicCDPage_FormClosing(object sender, FormClosingEventArgs e) {
            TableLayoutPanelMusicCD.Controls.Clear();
            productControls.Clear();
        }
    }
}
