﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore
{
    /// <summary>
    /// Urun tipi icin sinif.
    /// </summary>
    class ClassProductType
    {
        public string GetTypeString(string type)
        {
            if (type == "Book")
            {
                return "books/";
            }
            else if (type == "Magazine")
            {
                return "magazines/";
            }
            else if (type == "MusicCD")
            {

                return "musicCDs/";
            }

            return "";
        }
    }
}
