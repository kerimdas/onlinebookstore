﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//bitbucket

namespace onlinestore
{
    /// <summary>
    /// Kitap icin bir sinif. Urun sinifindan turetilmistir.
    /// </summary>
    public class ClassBook : ClassProduct//public yaptım
    {
        private string isbn;
        private string author;
        private string publisher;
        private int page;

        public string ISBN { set { isbn = value; } get { return isbn; } }
        public string Author { set { author = value; } get { return author; } }
        public string Publisher { set { publisher = value; } get { return publisher; } }
        public int Page { set { page = value; } get { return page; } }

        //Silinecek
        public void printBookProperties()
        {
            Console.WriteLine(this.isbn + " " + this.author + " " + this.publisher + " " + this.page);
        }
    }
}
