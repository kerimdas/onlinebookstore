﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Mesaj kutusu icin ozel form.
    /// </summary>
    public partial class FormMessageBox : Form {
        ISubjectUser subjectUser;

        public FormMessageBox() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormMessageBox.", "system", nameof(FormMessageBox));
        }

        static FormMessageBox formMessageBox;
        static DialogResult dialogResult = DialogResult.No;

        /// <summary>
        /// Mesaj kutusunu gosterir.
        /// </summary>
        /// <param name="title">Mesaj kutusunun basligi.</param>
        /// <param name="text">Mesaj kutusunun mesaji.</param>
        /// <returns>Cevabi dondurur.</returns>
        public static DialogResult Show(string title, string text) {
            formMessageBox = new FormMessageBox();
            formMessageBox.LabelMessageBoxTitle.Text = title;
            formMessageBox.LabelMessageBoxText.Text = text;
            formMessageBox.ShowDialog();
            return dialogResult;
        }

        /// <summary>
        /// OK Butonu.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMessageBoxOK_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is accepted the FormMessageBox.", "system", nameof(ButtonMessageBoxOK_Click));
            dialogResult = DialogResult.Yes;
            formMessageBox.Close();
        }

        /// <summary>
        /// Iptal Butonu.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMessageBoxCancel_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is cancelled the FormMessageBox.", "system", nameof(ButtonMessageBoxCancel_Click));
            dialogResult = DialogResult.No;
            formMessageBox.Close();
        }

        /// <summary>
        /// Kapat Butonu.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMessageBoxExit_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is exited from the FormMessageBox.", "system", nameof(ButtonMessageBoxExit_Click));
            dialogResult = DialogResult.No;
            formMessageBox.Close();
        }
    }
}
