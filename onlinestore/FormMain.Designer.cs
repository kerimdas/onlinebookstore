﻿namespace onlinestore {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.PanelMainpageTop = new System.Windows.Forms.Panel();
            this.LabelMyCartCount = new System.Windows.Forms.Label();
            this.ButtonMainpageMyCart = new System.Windows.Forms.Button();
            this.ButtonMainpageTopUsername = new System.Windows.Forms.Button();
            this.ButtonMainpageExit = new System.Windows.Forms.Button();
            this.LabelMainpageTop = new System.Windows.Forms.Label();
            this.PictureBoxMainpageTop = new System.Windows.Forms.PictureBox();
            this.FlowLayoutPanelMainpage = new System.Windows.Forms.FlowLayoutPanel();
            this.TimerRefreshEventsMainPage = new System.Windows.Forms.Timer(this.components);
            this.PanelMainpageTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMainpageTop)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelMainpageTop
            // 
            this.PanelMainpageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelMainpageTop.Controls.Add(this.LabelMyCartCount);
            this.PanelMainpageTop.Controls.Add(this.ButtonMainpageMyCart);
            this.PanelMainpageTop.Controls.Add(this.ButtonMainpageTopUsername);
            this.PanelMainpageTop.Controls.Add(this.ButtonMainpageExit);
            this.PanelMainpageTop.Controls.Add(this.LabelMainpageTop);
            this.PanelMainpageTop.Controls.Add(this.PictureBoxMainpageTop);
            this.PanelMainpageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMainpageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelMainpageTop.Name = "PanelMainpageTop";
            this.PanelMainpageTop.Size = new System.Drawing.Size(1200, 56);
            this.PanelMainpageTop.TabIndex = 1;
            // 
            // LabelMyCartCount
            // 
            this.LabelMyCartCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelMyCartCount.AutoSize = true;
            this.LabelMyCartCount.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMyCartCount.ForeColor = System.Drawing.Color.White;
            this.LabelMyCartCount.Location = new System.Drawing.Point(933, 4);
            this.LabelMyCartCount.Name = "LabelMyCartCount";
            this.LabelMyCartCount.Size = new System.Drawing.Size(15, 16);
            this.LabelMyCartCount.TabIndex = 5;
            this.LabelMyCartCount.Text = "0";
            // 
            // ButtonMainpageMyCart
            // 
            this.ButtonMainpageMyCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMainpageMyCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMainpageMyCart.FlatAppearance.BorderSize = 0;
            this.ButtonMainpageMyCart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMainpageMyCart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMainpageMyCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMainpageMyCart.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMainpageMyCart.ForeColor = System.Drawing.Color.White;
            this.ButtonMainpageMyCart.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMainpageMyCart.Image")));
            this.ButtonMainpageMyCart.Location = new System.Drawing.Point(892, 4);
            this.ButtonMainpageMyCart.Name = "ButtonMainpageMyCart";
            this.ButtonMainpageMyCart.Size = new System.Drawing.Size(70, 49);
            this.ButtonMainpageMyCart.TabIndex = 4;
            this.ButtonMainpageMyCart.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ButtonMainpageMyCart.UseVisualStyleBackColor = true;
            this.ButtonMainpageMyCart.Click += new System.EventHandler(this.ButtonMainpageMyCart_Click);
            // 
            // ButtonMainpageTopUsername
            // 
            this.ButtonMainpageTopUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMainpageTopUsername.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMainpageTopUsername.FlatAppearance.BorderSize = 0;
            this.ButtonMainpageTopUsername.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMainpageTopUsername.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMainpageTopUsername.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMainpageTopUsername.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMainpageTopUsername.ForeColor = System.Drawing.Color.White;
            this.ButtonMainpageTopUsername.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMainpageTopUsername.Image")));
            this.ButtonMainpageTopUsername.Location = new System.Drawing.Point(968, 4);
            this.ButtonMainpageTopUsername.Name = "ButtonMainpageTopUsername";
            this.ButtonMainpageTopUsername.Size = new System.Drawing.Size(170, 49);
            this.ButtonMainpageTopUsername.TabIndex = 3;
            this.ButtonMainpageTopUsername.Text = "Beta User";
            this.ButtonMainpageTopUsername.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonMainpageTopUsername.UseVisualStyleBackColor = true;
            this.ButtonMainpageTopUsername.Click += new System.EventHandler(this.ButtonMainpageTopUsername_Click);
            // 
            // ButtonMainpageExit
            // 
            this.ButtonMainpageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMainpageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMainpageExit.FlatAppearance.BorderSize = 0;
            this.ButtonMainpageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMainpageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMainpageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMainpageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonMainpageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMainpageExit.Image")));
            this.ButtonMainpageExit.Location = new System.Drawing.Point(1144, 4);
            this.ButtonMainpageExit.Name = "ButtonMainpageExit";
            this.ButtonMainpageExit.Size = new System.Drawing.Size(53, 49);
            this.ButtonMainpageExit.TabIndex = 1;
            this.ButtonMainpageExit.UseVisualStyleBackColor = true;
            this.ButtonMainpageExit.Click += new System.EventHandler(this.ButtonMainpageExit_Click);
            // 
            // LabelMainpageTop
            // 
            this.LabelMainpageTop.AutoSize = true;
            this.LabelMainpageTop.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMainpageTop.ForeColor = System.Drawing.Color.White;
            this.LabelMainpageTop.Location = new System.Drawing.Point(74, 16);
            this.LabelMainpageTop.Name = "LabelMainpageTop";
            this.LabelMainpageTop.Size = new System.Drawing.Size(191, 25);
            this.LabelMainpageTop.TabIndex = 2;
            this.LabelMainpageTop.Text = "Online Book Store";
            // 
            // PictureBoxMainpageTop
            // 
            this.PictureBoxMainpageTop.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxMainpageTop.Image")));
            this.PictureBoxMainpageTop.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxMainpageTop.Name = "PictureBoxMainpageTop";
            this.PictureBoxMainpageTop.Size = new System.Drawing.Size(68, 56);
            this.PictureBoxMainpageTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxMainpageTop.TabIndex = 1;
            this.PictureBoxMainpageTop.TabStop = false;
            // 
            // FlowLayoutPanelMainpage
            // 
            this.FlowLayoutPanelMainpage.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.FlowLayoutPanelMainpage.AutoScroll = true;
            this.FlowLayoutPanelMainpage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FlowLayoutPanelMainpage.Location = new System.Drawing.Point(96, 81);
            this.FlowLayoutPanelMainpage.Name = "FlowLayoutPanelMainpage";
            this.FlowLayoutPanelMainpage.Size = new System.Drawing.Size(1000, 585);
            this.FlowLayoutPanelMainpage.TabIndex = 2;
            // 
            // TimerRefreshEventsMainPage
            // 
            this.TimerRefreshEventsMainPage.Enabled = true;
            this.TimerRefreshEventsMainPage.Tick += new System.EventHandler(this.TimerRefreshEventsMainPage_Tick);
            // 
            // FormMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1200, 700);
            this.Controls.Add(this.FlowLayoutPanelMainpage);
            this.Controls.Add(this.PanelMainpageTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormMain";
            this.Text = "Mainpage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Mainpage_Load);
            this.PanelMainpageTop.ResumeLayout(false);
            this.PanelMainpageTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMainpageTop)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelMainpageTop;
        private System.Windows.Forms.Button ButtonMainpageExit;
        private System.Windows.Forms.Label LabelMainpageTop;
        private System.Windows.Forms.PictureBox PictureBoxMainpageTop;
        private System.Windows.Forms.Button ButtonMainpageTopUsername;
        private System.Windows.Forms.Button ButtonMainpageMyCart;
        private System.Windows.Forms.Label LabelMyCartCount;
        private System.Windows.Forms.FlowLayoutPanel FlowLayoutPanelMainpage;
        private System.Windows.Forms.Timer TimerRefreshEventsMainPage;
    }
}