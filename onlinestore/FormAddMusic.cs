﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace onlinestore {
    /// <summary>
    /// Muzik eklemek icin form.
    /// </summary>
    public partial class FormAddMusic : Form {

        ClassDatabase database = ClassDatabase.connect();
        ISubjectUser subjectUser;

        string sourceImage;

        public FormAddMusic() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormAddMusic.", "admin", nameof(FormAddMusic));
        }

        /// <summary>
        /// Muzik ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddMusicCDPageAdd_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is added new music cd to the application.", "admin", nameof(ButtonAddMusicCDPageAdd_Click));
            try
            {
                string appPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\musicCDs\";

                string name = TextBoxAddMusicCDPageName.Text;
                string singer = TextBoxAddMusicCDPageSinger.Text;
                int type = ComboBoxAddMusicCDPageType.SelectedIndex + 1;
                double price = Convert.ToDouble(TextBoxAddMusicCDPagePrice.Text);

                database.SetMusicCDData(name, singer, type, (float)price);
                int ID = 0;
                ID = database.GetLastRecordID("[MusicTable]");

                File.Copy(sourceImage, appPath + ID + ".jpg");
            }
            catch (Exception)
            {
                
            }

            this.Close();
        }

        private void AddMusicCDPage_Load(object sender, EventArgs e)
        {
            foreach (var item in Enum.GetValues(typeof(ClassMusic.type)))
            {
                ComboBoxAddMusicCDPageType.Items.Add(item);
            }
        }

        /// <summary>
        /// Muzik icin resim ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddMusicCDPageAddPicture_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is added a picture to the new music cd.", "admin", nameof(ButtonAddMusicCDPageAddPicture_Click));

            OpenFileDialog open = new OpenFileDialog(); 
            open.Title = "Title";
            open.Filter = "jpg files(.*jpg)|*.jpg|PNG files(*.png)|*.png|All files(.*)|.*";
            try
            {
                if (open.ShowDialog() == DialogResult.OK)
                {

                    sourceImage = open.FileName;
                    PictureBoxAddMusicCDPage.Image = new Bitmap(sourceImage);

                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Sayi disinda karakter girilmemesini saglar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TextBoxAddMusicCDPagePrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddMusicCDPageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exit from the FormAddMusic.", "admin", nameof(ButtonAddMusicCDPageExit_Click));
            this.Close();
        }
    }
}
