﻿namespace onlinestore {
    partial class FormMessageBox {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMessageBox));
            this.PanelMessageBoxTop = new System.Windows.Forms.Panel();
            this.LabelMessageBoxTitle = new System.Windows.Forms.Label();
            this.ButtonMessageBoxExit = new System.Windows.Forms.Button();
            this.PanelMessageBoxMiddle = new System.Windows.Forms.Panel();
            this.ButtonMessageBoxCancel = new System.Windows.Forms.Button();
            this.ButtonMessageBoxOK = new System.Windows.Forms.Button();
            this.LabelMessageBoxText = new System.Windows.Forms.Label();
            this.PanelMessageBoxTop.SuspendLayout();
            this.PanelMessageBoxMiddle.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelMessageBoxTop
            // 
            this.PanelMessageBoxTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelMessageBoxTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelMessageBoxTop.Controls.Add(this.LabelMessageBoxTitle);
            this.PanelMessageBoxTop.Controls.Add(this.ButtonMessageBoxExit);
            this.PanelMessageBoxTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMessageBoxTop.Location = new System.Drawing.Point(0, 0);
            this.PanelMessageBoxTop.Name = "PanelMessageBoxTop";
            this.PanelMessageBoxTop.Size = new System.Drawing.Size(400, 35);
            this.PanelMessageBoxTop.TabIndex = 0;
            // 
            // LabelMessageBoxTitle
            // 
            this.LabelMessageBoxTitle.AutoSize = true;
            this.LabelMessageBoxTitle.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMessageBoxTitle.ForeColor = System.Drawing.Color.White;
            this.LabelMessageBoxTitle.Location = new System.Drawing.Point(12, 8);
            this.LabelMessageBoxTitle.Name = "LabelMessageBoxTitle";
            this.LabelMessageBoxTitle.Size = new System.Drawing.Size(35, 17);
            this.LabelMessageBoxTitle.TabIndex = 1;
            this.LabelMessageBoxTitle.Text = "Title";
            // 
            // ButtonMessageBoxExit
            // 
            this.ButtonMessageBoxExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMessageBoxExit.FlatAppearance.BorderSize = 0;
            this.ButtonMessageBoxExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMessageBoxExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMessageBoxExit.Image")));
            this.ButtonMessageBoxExit.Location = new System.Drawing.Point(336, 0);
            this.ButtonMessageBoxExit.Name = "ButtonMessageBoxExit";
            this.ButtonMessageBoxExit.Size = new System.Drawing.Size(59, 33);
            this.ButtonMessageBoxExit.TabIndex = 0;
            this.ButtonMessageBoxExit.UseVisualStyleBackColor = true;
            this.ButtonMessageBoxExit.Click += new System.EventHandler(this.ButtonMessageBoxExit_Click);
            // 
            // PanelMessageBoxMiddle
            // 
            this.PanelMessageBoxMiddle.BackColor = System.Drawing.SystemColors.Control;
            this.PanelMessageBoxMiddle.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelMessageBoxMiddle.Controls.Add(this.ButtonMessageBoxCancel);
            this.PanelMessageBoxMiddle.Controls.Add(this.ButtonMessageBoxOK);
            this.PanelMessageBoxMiddle.Controls.Add(this.LabelMessageBoxText);
            this.PanelMessageBoxMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelMessageBoxMiddle.Location = new System.Drawing.Point(0, 35);
            this.PanelMessageBoxMiddle.Name = "PanelMessageBoxMiddle";
            this.PanelMessageBoxMiddle.Size = new System.Drawing.Size(400, 165);
            this.PanelMessageBoxMiddle.TabIndex = 1;
            // 
            // ButtonMessageBoxCancel
            // 
            this.ButtonMessageBoxCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMessageBoxCancel.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ButtonMessageBoxCancel.FlatAppearance.BorderSize = 0;
            this.ButtonMessageBoxCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMessageBoxCancel.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMessageBoxCancel.ForeColor = System.Drawing.Color.White;
            this.ButtonMessageBoxCancel.Location = new System.Drawing.Point(206, 124);
            this.ButtonMessageBoxCancel.Name = "ButtonMessageBoxCancel";
            this.ButtonMessageBoxCancel.Size = new System.Drawing.Size(181, 28);
            this.ButtonMessageBoxCancel.TabIndex = 2;
            this.ButtonMessageBoxCancel.Text = "Cancel";
            this.ButtonMessageBoxCancel.UseVisualStyleBackColor = false;
            this.ButtonMessageBoxCancel.Click += new System.EventHandler(this.ButtonMessageBoxCancel_Click);
            // 
            // ButtonMessageBoxOK
            // 
            this.ButtonMessageBoxOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMessageBoxOK.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.ButtonMessageBoxOK.FlatAppearance.BorderSize = 0;
            this.ButtonMessageBoxOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMessageBoxOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMessageBoxOK.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMessageBoxOK.ForeColor = System.Drawing.Color.White;
            this.ButtonMessageBoxOK.Location = new System.Drawing.Point(11, 124);
            this.ButtonMessageBoxOK.Name = "ButtonMessageBoxOK";
            this.ButtonMessageBoxOK.Size = new System.Drawing.Size(177, 28);
            this.ButtonMessageBoxOK.TabIndex = 1;
            this.ButtonMessageBoxOK.Text = "OK";
            this.ButtonMessageBoxOK.UseVisualStyleBackColor = false;
            this.ButtonMessageBoxOK.Click += new System.EventHandler(this.ButtonMessageBoxOK_Click);
            // 
            // LabelMessageBoxText
            // 
            this.LabelMessageBoxText.AutoEllipsis = true;
            this.LabelMessageBoxText.BackColor = System.Drawing.SystemColors.Control;
            this.LabelMessageBoxText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMessageBoxText.Location = new System.Drawing.Point(11, 18);
            this.LabelMessageBoxText.Name = "LabelMessageBoxText";
            this.LabelMessageBoxText.Size = new System.Drawing.Size(376, 90);
            this.LabelMessageBoxText.TabIndex = 0;
            this.LabelMessageBoxText.Text = "Text";
            // 
            // FormMessageBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 200);
            this.Controls.Add(this.PanelMessageBoxMiddle);
            this.Controls.Add(this.PanelMessageBoxTop);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMessageBox";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMessageBox";
            this.PanelMessageBoxTop.ResumeLayout(false);
            this.PanelMessageBoxTop.PerformLayout();
            this.PanelMessageBoxMiddle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelMessageBoxTop;
        private System.Windows.Forms.Label LabelMessageBoxTitle;
        private System.Windows.Forms.Button ButtonMessageBoxExit;
        private System.Windows.Forms.Panel PanelMessageBoxMiddle;
        private System.Windows.Forms.Button ButtonMessageBoxCancel;
        private System.Windows.Forms.Button ButtonMessageBoxOK;
        public System.Windows.Forms.Label LabelMessageBoxText;
    }
}