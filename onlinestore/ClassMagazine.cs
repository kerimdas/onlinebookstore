﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore
{
    /// <summary>
    /// Magazin urunu icin sinif.
    /// </summary>
    public class ClassMagazine : ClassProduct
    {
        private string issue;//konu
        public string typeName;// enum sıkıntılı doğru bi yöntemmi bilmiyorum.
        public enum type { Actual = 1, News = 2, Sport = 3, Computer = 4, Technology = 5 }
        
        public string Issue { set { issue = value; } get { return issue; } }

        public void printMagazineProperties()
        {
            Console.WriteLine(this.issue + " " + this.typeName);
        }      
    }
}
