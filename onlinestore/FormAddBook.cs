﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace onlinestore {
    /// <summary>
    /// Kitap eklemek icin form.
    /// </summary>
    public partial class FormAddBook : Form {

        ClassDatabase database = ClassDatabase.connect();
        ISubjectUser subjectUser;

        string sourceImage;
        
        public FormAddBook() {
            InitializeComponent();
            
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - Admin is entered to the FormAddBook.", "admin", nameof(FormAddBook));
        }

        /// <summary>
        /// Kitap ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddBookPageAdd_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - Admin is added a new book to the application.", "admin", nameof(ButtonAddBookPageAdd_Click));
            try
            {
                string appPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\books\";

                string isbn = TextBoxAddBookPageISBN.Text;
                string name = TextBookAddBookPageName.Text;
                string author = TextBoxAddBookPageAuthor.Text;
                string publisher = TextBoxAddBookPagePublisher.Text;
                string page = TextBoxAddBookPagePage.Text;
                double price = Convert.ToDouble(TextBoxAddBookPagePrice.Text);

                database.SetBookData(isbn, name, author, publisher, Convert.ToInt32(page), (float)price);//burada sql e kaydedecez
                int ID = 0;
                ID = database.GetLastRecordID("[BookTable]");//yukarıda kaydettiğimizin ID sini çekeceğiz

                File.Copy(sourceImage, appPath + ID + ".jpg");//ID isimli foto kayıt olmus olacak
            }
            catch (Exception)
            {

            }

            this.Close();
        }

        /// <summary>
        /// Sayi disinda karakter girilmemesini saglar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TextBoxAddBookPagePrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1) && e.KeyChar != '.' && e.KeyChar != ',')
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Sayi disinda karakter girilmemesini saglar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TextBoxAddBookPagePage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Kitap icin resim ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddBookPageAddPicture_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - Admin is added a picture to the new book.", "admin", nameof(ButtonAddBookPageAddPicture_Click));

            OpenFileDialog open = new OpenFileDialog();
            
            string appPath = Path.GetDirectoryName(Application.ExecutablePath) + @"\books\";

            open.Title = "Title";
            open.Filter = "jpg files(.*jpg)|*.jpg|PNG files(*.png)|*.png|All files(.*)|.*";

            try
            {
                if (open.ShowDialog() == DialogResult.OK)
                {
                    sourceImage = open.FileName;
                    PictureBoxAddBookPage.Image = new Bitmap(sourceImage);                    
                }
            }
            catch (Exception)
            {
                
            }
            
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddBookPageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - Admin is exited from the FormAddBook.", "admin", nameof(ButtonAddBookPageExit_Click));
            this.Close();
        }
    }

}