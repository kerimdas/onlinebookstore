﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace onlinestore {
    /// <summary>
    /// Kullanici veritabani icin sinif.
    /// </summary>
    public class ClassDatabaseUser : IDatabaseUser {
        public static SqlConnection connection = ClassDatabase.connection;

        /// <summary>
        /// Kullanicinin veritabaninda olup olmadigini kontrol eder.
        /// </summary>
        /// <param name="username">Kullaniciyi veritabaninda aratirken kullanici ismine gore aratiyoruz.</param>
        /// <returns>Dogru veya yanlis dondurur.</returns>
        public bool CheckUserExist(string username) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [UserTable] WHERE ([USERNAME] = @username)", connection)) {
                command.Parameters.AddWithValue("@username", username);
                int UserExist = (int)command.ExecuteScalar();
                if (UserExist > 0) {
                    connection.Close();
                    return true;
                } else {
                    connection.Close();
                    return false;
                }
            }
        }

        /// <summary>
        /// Kullanici kaydolurken bilgilerini gecici veritabanina ekler.
        /// </summary>
        /// <param name="username">Kullaniciyi gecici veritabanina eklerken kullanici adini parametre olarak alir.</param>
        /// <param name="password">Kullaniciyi gecici veritabanina eklerken sifresini parametre olarak alir.</param>
        /// <param name="name">Kullaniciyi gecici veritabanina eklerken ismini parametre olarak alir.</param>
        /// <param name="email">Kullaniciyi gecici veritabanina eklerken e-posta adresini parametre olarak alir.</param>
        /// <param name="address">Kullaniciyi gecici veritabanina eklerken adresini parametre olarak alir.</param>
        public void SetNotConfirmedUserData(string username, string password, string name, string email, string address) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("INSERT INTO [NotConfirmedUserTable] (USERNAME, PASSWORD, NAME, EMAIL, ADDRESS) values(@username, @password, @name, @email, @address)", connection)) {
                command.Parameters.AddWithValue("username", username);
                command.Parameters.AddWithValue("password", password);
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("email", email);
                command.Parameters.AddWithValue("address", address);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }

        /// <summary>
        /// Kullanicinin sisteme giris yapabilmesi icin kullanici adi, sifresi gibi verilerini kontrol eder. ID'sini ise gonderdigimiz parametreye esitler.
        /// </summary>
        /// <param name="username">Kullanici ismini kontrol etmek icin parametre olarak alır.</param>
        /// <param name="password">Sifresini kontrol etmek icin parametre olarak alır.</param>
        /// <param name="id">ID'sini almak icin parametre olarak alır.</param>
        /// <returns>Dogru veya yanlis dondurur.</returns>
        public bool GetUserDataToLogin(string username, string password, ref int id) {
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [UserTable] WHERE USERNAME=@username AND PASSWORD=@password", connection)) {
                command.Parameters.AddWithValue("username", username);
                command.Parameters.AddWithValue("password", password);

                SqlDataReader sqlDataReader = command.ExecuteReader();
                if (sqlDataReader.Read()) {
                    sqlDataReader.Close();
                    id = Convert.ToInt32(command.ExecuteScalar());
                    connection.Close();
                    return true;
                } else {
                    connection.Close();
                    return false;
                }
            }
        }

        /// <summary>
        /// Kullanicinin girmis oldugu e-postayi veritabaninda var mi diye kontrol eder.
        /// </summary>
        /// <param name="email">E-postayi kontrol etmek icin parametre olarak alir.</param>
        /// <returns>Dogru veya yanlis dondurur.</returns>
        public bool CheckUserEmailExist(string email) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM [UserTable] WHERE ([EMAIL] = @email)", connection)) {
                command.Parameters.AddWithValue("@email", email);
                int UserExist = (int)command.ExecuteScalar();
                if (UserExist > 0) {
                    connection.Close();
                    return true;
                } else {
                    connection.Close();
                    return false;
                }
            }
        }

        /// <summary>
        /// Veritabanina eklenen kullaniciyi, gecici veritabanindan silmek icin kullanilir.
        /// </summary>
        /// <param name="ID">Kullanici ID'si veritabaninda kullaniciyi bulmak için parametre olarak alinir.</param>
        public void RemoveNotConfirmedUserData(int ID) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("DELETE FROM [NotConfirmedUserTable] WHERE ID=" + ID + "", connection)) {
                //command.Parameters.AddWithValue("username", username);
                //command.Parameters.AddWithValue("password", password);
                //command.Parameters.AddWithValue("name", name);
                //command.Parameters.AddWithValue("email", email);
                //command.Parameters.AddWithValue("address", address);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }

        /// <summary>
        /// Kullanici kaydolurken bilgilerini veritabanina ekler.
        /// </summary>
        /// <param name="username">Kullaniciyi veritabanina eklerken kullanici adini parametre olarak alir.</param>
        /// <param name="password">Kullaniciyi veritabanina eklerken sifresini parametre olarak alir.</param>
        /// <param name="name">Kullaniciyi veritabanina eklerken ismini parametre olarak alir.</param>
        /// <param name="email">Kullaniciyi veritabanina eklerken e-posta adresini parametre olarak alir.</param>
        /// <param name="address">Kullaniciyi veritabanina eklerken adresini parametre olarak alir.</param>
        public void SetUserData(string username, string password, string name, string email, string address) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("INSERT INTO [UserTable] (USERNAME, PASSWORD, NAME, EMAIL, ADDRESS) values(@username, @password, @name, @email, @address)", connection)) {
                command.Parameters.AddWithValue("username", username);
                command.Parameters.AddWithValue("password", password);
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("email", email);
                command.Parameters.AddWithValue("address", address);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }

        /// <summary>
        /// Kullanicinin veritabanindaki bilgilerini guncellemesini saglar.
        /// </summary>
        /// <param name="id">Kullaniciyi veritabaninda bulmasi icin ID'si parametre olarak alinir.</param>
        /// <param name="password">Sifre degistirmek icin parametre olarak alınır.</param>
        /// <param name="email">E-posta adresi degistirmek icin parametre olarak alınır.</param>
        /// <param name="address">Adresi degistirmek icin parametre olarak alınır.</param>
        public void UpdateUserData(int id, string password, string email, string address) {
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"UPDATE [UserTable] SET PASSWORD=@password, EMAIL=@email, ADDRESS=@address WHERE ID=@id", connection)) {
                command.Parameters.AddWithValue("id", id);
                command.Parameters.AddWithValue("password", password);
                command.Parameters.AddWithValue("email", email);
                command.Parameters.AddWithValue("address", address);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }

        /// <summary>
        /// Kullanicinin sifresini guncellemesini saglar.
        /// </summary>
        /// <param name="id">Kullaniciyi veritabaninda bulmasi icin ID'si parametre olarak alinir.</param>
        /// <param name="password">Sifre degistirmek icin parametre olarak alinir.</param>
        public void UpdateUserPassword(int id, string password) {
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"UPDATE [UserTable] SET PASSWORD=@password WHERE ID=@id", connection)) {
                command.Parameters.AddWithValue("id", id);
                command.Parameters.AddWithValue("password", password);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }

        /// <summary>
        /// Gecici veritabanindaki kullanici bilgilerini DataTable olarak dondurur.
        /// </summary>
        /// <returns>DataTable tipinde veritabani tablosu dondurur.</returns>
        public DataTable GetNotConfirmedUserData() {
            DataTable dataTable = new DataTable();
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [NotConfirmedUserTable]", connection))
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command)) {
                sqlDataAdapter.Fill(dataTable);
            }
            connection.Close();
            return dataTable;
        }

        /// <summary>
        /// Kullanicinin veritabanindaki mevcut bilgileri ID'si kullanilarak gonderilen parametreye esitlenir.
        /// </summary>
        /// <param name="customer">Bilgileri esitlemek icin parametre.</param>
        /// <param name="id">Istenilen kullaniciyi bulmak icin ID'si parametre olarak alinir.</param>
        public void GetUserData(ref ClassCustomer customer, int id) {
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [UserTable] WHERE ID=@id", connection)) {
                command.Parameters.AddWithValue("id", id);

                using (SqlDataReader sqlDataReader = command.ExecuteReader()) {
                    sqlDataReader.Read();
                    customer.ID = Convert.ToInt32(sqlDataReader["ID"]);
                    customer.Username = sqlDataReader["USERNAME"].ToString();
                    customer.Password = sqlDataReader["PASSWORD"].ToString();
                    customer.Name = sqlDataReader["NAME"].ToString();
                    customer.Email = sqlDataReader["EMAIL"].ToString();
                    customer.Address = sqlDataReader["ADDRESS"].ToString();
                }
            }
            connection.Close();
        }

        /// <summary>
        /// Kullanicin e-posta adresiyle ID'sine ulasilir.
        /// </summary>
        /// <param name="email">Kullaniciyi veritabaninda bulmak icin kullanilir.</param>
        /// <returns>Kullanicin ID'sini dondurur.</returns>
        public int GetUserID(string email) {
            int IDCustomer;
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [UserTable] WHERE EMAIL=@email", connection)) {
                command.Parameters.AddWithValue("email", email);

                using (SqlDataReader sqlDataReader = command.ExecuteReader()) {
                    sqlDataReader.Read();
                    IDCustomer = Convert.ToInt32(sqlDataReader["ID"]);
                }
            }
            connection.Close();
            return IDCustomer;
        }
    }
}
