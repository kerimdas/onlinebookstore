﻿namespace onlinestore {
    partial class ControlPanel {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.PanelPictureBox = new System.Windows.Forms.Panel();
            this.PictureBoxPanel = new System.Windows.Forms.PictureBox();
            this.Panel = new System.Windows.Forms.Panel();
            this.PanelSeperator = new System.Windows.Forms.Panel();
            this.LabelPanelText = new System.Windows.Forms.Label();
            this.LabelPanelTitle = new System.Windows.Forms.Label();
            this.PanelPictureBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPanel)).BeginInit();
            this.Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelPictureBox
            // 
            this.PanelPictureBox.AutoSize = true;
            this.PanelPictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelPictureBox.Controls.Add(this.PictureBoxPanel);
            this.PanelPictureBox.Location = new System.Drawing.Point(0, 0);
            this.PanelPictureBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PanelPictureBox.Name = "PanelPictureBox";
            this.PanelPictureBox.Size = new System.Drawing.Size(213, 185);
            this.PanelPictureBox.TabIndex = 0;
            // 
            // PictureBoxPanel
            // 
            this.PictureBoxPanel.Location = new System.Drawing.Point(36, 26);
            this.PictureBoxPanel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PictureBoxPanel.Name = "PictureBoxPanel";
            this.PictureBoxPanel.Size = new System.Drawing.Size(140, 130);
            this.PictureBoxPanel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxPanel.TabIndex = 0;
            this.PictureBoxPanel.TabStop = false;
            // 
            // Panel
            // 
            this.Panel.AutoSize = true;
            this.Panel.Controls.Add(this.PanelSeperator);
            this.Panel.Controls.Add(this.LabelPanelText);
            this.Panel.Controls.Add(this.LabelPanelTitle);
            this.Panel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Panel.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Panel.Location = new System.Drawing.Point(213, 0);
            this.Panel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panel.Name = "Panel";
            this.Panel.Size = new System.Drawing.Size(1107, 185);
            this.Panel.TabIndex = 1;
            this.Panel.DoubleClick += new System.EventHandler(this.Panel_DoubleClick);
            this.Panel.MouseEnter += new System.EventHandler(this.Panel_MouseEnter);
            this.Panel.MouseLeave += new System.EventHandler(this.Panel_MouseLeave);
            // 
            // PanelSeperator
            // 
            this.PanelSeperator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelSeperator.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelSeperator.Location = new System.Drawing.Point(0, 184);
            this.PanelSeperator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PanelSeperator.Name = "PanelSeperator";
            this.PanelSeperator.Size = new System.Drawing.Size(1107, 1);
            this.PanelSeperator.TabIndex = 2;
            // 
            // LabelPanelText
            // 
            this.LabelPanelText.AutoSize = true;
            this.LabelPanelText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelPanelText.Location = new System.Drawing.Point(28, 90);
            this.LabelPanelText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelPanelText.Name = "LabelPanelText";
            this.LabelPanelText.Size = new System.Drawing.Size(379, 23);
            this.LabelPanelText.TabIndex = 1;
            this.LabelPanelText.Text = "Lorem ipsum bla bla.. Some text here.";
            this.LabelPanelText.Click += new System.EventHandler(this.Panel_DoubleClick);
            this.LabelPanelText.MouseEnter += new System.EventHandler(this.Panel_MouseEnter);
            this.LabelPanelText.MouseLeave += new System.EventHandler(this.Panel_MouseLeave);
            // 
            // LabelPanelTitle
            // 
            this.LabelPanelTitle.AutoSize = true;
            this.LabelPanelTitle.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelPanelTitle.Location = new System.Drawing.Point(28, 47);
            this.LabelPanelTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelPanelTitle.Name = "LabelPanelTitle";
            this.LabelPanelTitle.Size = new System.Drawing.Size(63, 32);
            this.LabelPanelTitle.TabIndex = 0;
            this.LabelPanelTitle.Text = "Title";
            this.LabelPanelTitle.Click += new System.EventHandler(this.Panel_DoubleClick);
            this.LabelPanelTitle.MouseEnter += new System.EventHandler(this.Panel_MouseEnter);
            this.LabelPanelTitle.MouseLeave += new System.EventHandler(this.Panel_MouseLeave);
            // 
            // PanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Panel);
            this.Controls.Add(this.PanelPictureBox);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PanelControl";
            this.Size = new System.Drawing.Size(975, 185);
            this.PanelPictureBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxPanel)).EndInit();
            this.Panel.ResumeLayout(false);
            this.Panel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PanelPictureBox;
        private System.Windows.Forms.PictureBox PictureBoxPanel;
        private System.Windows.Forms.Panel Panel;
        private System.Windows.Forms.Panel PanelSeperator;
        private System.Windows.Forms.Label LabelPanelText;
        private System.Windows.Forms.Label LabelPanelTitle;
    }
}
