﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace onlinestore {
    /// <summary>
    /// Muzik veritabani icin sinif.
    /// </summary>
    public class ClassDatabaseMusic : IDatabaseMusic {
        public static SqlConnection connection = ClassDatabase.connection;

        /// <summary>
        /// Veritabanina gonderilen indeksteki muzigi bulup parametreye esitler.
        /// </summary>
        /// <param name="musicCD">Muzigi esitlemek icin parametre.</param>
        /// <param name="index">Istenilen muzigi bulmak icin parametre.</param>
        public void GetMusicCDData(ClassMusic musicCD, int index) {
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [MusicTable] WHERE ID = " + index + "", connection)) {
                command.Parameters.AddWithValue("ID", index);

                using (SqlDataReader dr = command.ExecuteReader()) {
                    dr.Read();

                    musicCD.ID = Convert.ToInt32(dr["ID"]);

                    musicCD.Name = dr["NAME"].ToString();

                    musicCD.Singer = dr["SINGER"].ToString();
                    int type = Convert.ToInt32(dr["TYPE"]);
                    musicCD.Genre = ((ClassMusic.type)type).ToString();
                    musicCD.Price = Convert.ToDouble(dr["PRICE"]);
                }
            }
            connection.Close();
        }

        /// <summary>
        /// Muzigi veritabanina ekler.
        /// </summary>
        /// <param name="name">Muzigin ismini parametre olarak alir.</param>
        /// <param name="singer">Sanatcinin ismini parametre olarak alir.</param>
        /// <param name="type">Muzigin tipini parametre olarak alir.</param>
        /// <param name="price">Fiyatini parametre olarak alir.</param>
        public void SetMusicCDData(string name, string singer, int type, float price) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("INSERT INTO [MusicTable] (NAME, SINGER, TYPE, PRICE) values(@name, @singer, @type, @price)", connection)) {
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("singer", singer);
                command.Parameters.AddWithValue("type", type);
                command.Parameters.AddWithValue("price", price);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }
    }
}
