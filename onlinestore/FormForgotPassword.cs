﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Sifremi unuttum icin form.
    /// </summary>
    public partial class FormForgotPassword : Form {

        ClassDatabase database = ClassDatabase.connect();
        ISubjectUser subjectUser;

        public FormForgotPassword() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormForgotPassword.", "system", nameof(FormForgotPassword));
        }

        /// <summary>
        /// Sifreyi degistirir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonForgotPasswordAccept_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is changed the password.", "system", nameof(ButtonForgotPasswordAccept_Click));
            int IDCustomer = database.GetUserID(TextBoxForgotPasswordEmail.Text);
            database.UpdateUserPassword(IDCustomer, TextBoxForgotPasswordPassword.Text);
            this.Close();
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonForgotPasswordExit_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is exited from the FormForgotPassword.", "system", nameof(ButtonForgotPasswordExit_Click));
            this.Close();
        }
    }
}
