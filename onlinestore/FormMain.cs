﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

//Bitbucket
namespace onlinestore
{
    /// <summary>
    /// Anasayfa icin form.
    /// </summary>
    public partial class FormMain : Form
    {
        ControlPanel[] panelControlList = new ControlPanel[3];
        ClassCustomer customer = ClassCustomer.InstanceCustomer;
        ISubjectUser subjectUser;

        public static FormMain InstanceMainPage;
        public static ClassShoppingCart ShoppingCart = new ClassShoppingCart();

        public FormBook bookPage = new FormBook(ShoppingCart, ClassCustomer.InstanceCustomer);
        public FormMagazine magazinePage = new FormMagazine(ShoppingCart, ClassCustomer.InstanceCustomer);
        public FormMusic musicCDPage = new FormMusic(ShoppingCart, ClassCustomer.InstanceCustomer);        

        public FormMain()
        {
            InitializeComponent();
        }

        public FormMain(ClassCustomer _customer)
        {
            InitializeComponent();
            InstanceMainPage = this;    //  Verileri bu yöntemle kaybetmiyoruz
            customer = _customer;
            ButtonMainpageTopUsername.Text = customer.Username;
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormMain.", customer.Username, nameof(FormMain));
        }        
        
        private void Mainpage_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < panelControlList.Length; i++)
            {
                panelControlList[i] = new ControlPanel();             
                if (i == 0)
                {
                    panelControlList[i].Info = "Read!";
                    panelControlList[i].Title = "Book";
                    panelControlList[i].Image = Image.FromFile("png/icons_books_96px.png");
                    bookPage = FormBook.InstanceBookPage;
                    panelControlList[i].Form = bookPage;
                }
                else if (i == 1)
                {
                    panelControlList[i].Info = "Listen!";
                    panelControlList[i].Title = "MusicCD";
                    panelControlList[i].Image = Image.FromFile("png/icons_musicCD_96px.png");
                    musicCDPage = FormMusic.InstanceMusicCDPage;
                    panelControlList[i].Form = musicCDPage;
                }
                else if (i == 2)
                {
                    panelControlList[i].Info = "Learn!";
                    panelControlList[i].Title = "Magazine";
                    panelControlList[i].Image = Image.FromFile("png/icons_magazine_96px.png");
                    magazinePage = FormMagazine.InstanceMagazinePage;
                    panelControlList[i].Form = magazinePage;

                }
                FlowLayoutPanelMainpage.Controls.Add(panelControlList[i]);
            }
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMainpageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exited from the FormMain.", customer.Username, nameof(ButtonMainpageExit_Click));
            LabelMyCartCount.Text = "0";
            ShoppingCart.ItemsToPurchase.Clear();
            this.Dispose();
        }

        /// <summary>
        /// Sayfayi gunceller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TimerRefreshEventsMainPage_Tick(object sender, EventArgs e)
        {
            RefreshCartLabel();
        }

        /// <summary>
        /// Sepette olan toplam urun sayisini gosterir.
        /// </summary>
        private void RefreshCartLabel()
        {
            if (ShoppingCart.ItemsToPurchase.Count > 0)
            {
                LabelMyCartCount.Text = (ShoppingCart.ItemsToPurchase.Count).ToString();
            }
            else
            {
                LabelMyCartCount.Text = 0.ToString();
            }
        }

        /// <summary>
        /// Sepetime formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMainpageMyCart_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormMyCart.", customer.Username, nameof(ButtonMainpageMyCart_Click));
            FormMyCart myCartPage = new FormMyCart(ShoppingCart, customer);
            myCartPage = FormMyCart.InstanceMyCart;
            myCartPage.ShowDialog();
        }

        /// <summary>
        /// Kullanici formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonMainpageTopUsername_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is entering to the FormUser.", customer.Username, nameof(ButtonMainpageTopUsername_Click));
            FormUser userPage = new FormUser(customer);
            userPage.ShowDialog();
        }
    }
}
