﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore {
    /// <summary>
    /// Muzik veritabani sinifi icin arayuz.
    /// </summary>
    public interface IDatabaseMusic {
        void GetMusicCDData(ClassMusic musicCD, int index);
        void SetMusicCDData(string name, string singer, int type, float price);
    }
}
