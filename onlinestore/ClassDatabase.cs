﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace onlinestore {
    /// <summary>
    /// Veritabani icin bir sinif. Kullanici, kitap, magazin, muzik, siparis edilen urun sınıflarından turetilmistir.
    /// </summary>
    public class ClassDatabase : IDatabaseUser, IDatabaseBook, IDatabaseMagazine, IDatabaseMusic, IDatabaseOrderItem {
        public static ClassDatabase objectDatabase;

        ClassDatabaseUser databaseUser = new ClassDatabaseUser();
        ClassDatabaseBook databaseBook = new ClassDatabaseBook();
        ClassDatabaseMagazine databaseMagazine = new ClassDatabaseMagazine();
        ClassDatabaseMusic databaseMusic = new ClassDatabaseMusic();
        ClassDatabaseOrderItem databaseOrderItem = new ClassDatabaseOrderItem();


        //const string connectionString= "Data Source=DESKTOP-G2S2MP1\\SQLINSTANCE;Initial Catalog=onlinestore;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        const string connectionString= "Data Source=DESKTOP-G2S2MP1\\SQLINSTANCE;Initial Catalog=onlinestorefinal;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //const string connectionString = "Data Source=DESKTOP-8470BOK;Initial Catalog=onlinestore;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static SqlConnection connection = new SqlConnection(connectionString);

        private ClassDatabase() { }

        /// <summary>
        /// Veritabani sifinin nesnesini olusturur veya varsa mevcut nesneyi dondurur.
        /// </summary>
        /// <returns>Veritabani objesi dondurur.</returns>
        public static ClassDatabase connect() {
            if (objectDatabase == null) {
                objectDatabase = new ClassDatabase();
            }
            return objectDatabase;
        }

        /// <summary>
        /// Kullanici veritabaninda var mi yok mu diye kontrol etmek icin ust sinifina kullanici ismini gonderir. 
        /// </summary>
        /// <param name="username">Kullaniciyi veritabaninda aratirken kullanici ismine gore aratiyoruz.</param>
        /// <returns>Dogru veya yanlis dondurur.</returns>
        public bool CheckUserExist(string username) {
            return databaseUser.CheckUserExist(username);
        }

        /// <summary>
        /// Kullanici kaydolurken bilgilerini gecici veritabanina eklemek icin parametreleri ust sinifina gonderir.
        /// </summary>
        /// <param name="username">Kullaniciyi gecici veritabanina eklerken kullanici adini parametre olarak alir.</param>
        /// <param name="password">Kullaniciyi gecici veritabanina eklerken sifresini parametre olarak alir.</param>
        /// <param name="name">Kullaniciyi gecici veritabanina eklerken ismini parametre olarak alir.</param>
        /// <param name="email">Kullaniciyi gecici veritabanina eklerken e-posta adresini parametre olarak alir.</param>
        /// <param name="address">Kullaniciyi gecici veritabanina eklerken adresini parametre olarak alir.</param>
        public void SetNotConfirmedUserData(string username, string password, string name, string email, string address) {
            databaseUser.SetNotConfirmedUserData(username, password, name, email, address);
        }

        /// <summary>
        /// Kullanicinin sisteme giris yapabilmesi icin kullanici adi, sifresi gibi verilerini kontrol eder. ID'sini ise gonderdigimiz parametreye esitler. Parametreleri ust sinifina gonderir.
        /// </summary>
        /// <param name="username">Kullanici ismini kontrol etmek icin parametre olarak alır.</param>
        /// <param name="password">Sifresini kontrol etmek icin parametre olarak alır.</param>
        /// <param name="id">ID'sini almak icin parametre olarak alır.</param>
        /// <returns>Dogru veya yanlis dondurur.</returns>
        public bool GetUserDataToLogin(string username, string password, ref int id) {
            return databaseUser.GetUserDataToLogin(username, password, ref id);
        }

        /// <summary>
        /// Kullanicinin girmis oldugu e-postayi veritabaninda var mi diye kontrol eder. Parametreyi ust sinifa gonderir.
        /// </summary>
        /// <param name="email">E-postayi kontrol etmek icin parametre olarak alir.</param>
        /// <returns>Dogru veya yanlis dondurur.</returns>
        public bool CheckUserEmailExist(string email) {
            return databaseUser.CheckUserEmailExist(email);
        }

        /// <summary>
        /// Veritabanina eklenen kullaniciyi, gecici veritabanindan silmek icin kullanilir. Parametreyi ust sinifa gonderir.
        /// </summary>
        /// <param name="ID">Kullanici ID'si veritabaninda kullaniciyi bulmak için parametre olarak alinir.</param>
        public void RemoveNotConfirmedUserData(int ID) {
            databaseUser.RemoveNotConfirmedUserData(ID);
        }

        /// <summary>
        /// Kullanici kaydolurken bilgilerini veritabanina eklemek icin parametreleri ust sinifina gonderir.
        /// </summary>
        /// <param name="username">Kullaniciyi veritabanina eklerken kullanici adini parametre olarak alir.</param>
        /// <param name="password">Kullaniciyi veritabanina eklerken sifresini parametre olarak alir.</param>
        /// <param name="name">Kullaniciyi veritabanina eklerken ismini parametre olarak alir.</param>
        /// <param name="email">Kullaniciyi veritabanina eklerken e-posta adresini parametre olarak alir.</param>
        /// <param name="address">Kullaniciyi veritabanina eklerken adresini parametre olarak alir.</param>
        public void SetUserData(string username, string password, string name, string email, string address) {
            databaseUser.SetUserData(username, password, name, email, address);
        }

        /// <summary>
        /// Kullanicinin veritabanindaki bilgilerini guncellemesini saglar. Aldigi parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="id">Kullaniciyi veritabaninda bulmasi icin ID'si parametre olarak alinir.</param>
        /// <param name="password">Sifre degistirmek icin parametre olarak alınır.</param>
        /// <param name="email">E-posta adresi degistirmek icin parametre olarak alınır.</param>
        /// <param name="address">Adresi degistirmek icin parametre olarak alınır.</param>
        public void UpdateUserData(int id, string password, string email, string address) {
            databaseUser.UpdateUserData(id, password, email, address);
        }

        /// <summary>
        /// Kullanicinin sifresini guncellemesini saglar. Aldigi parametreleri ust sinifina gonderir.
        /// </summary>
        /// <param name="id">Kullaniciyi veritabaninda bulmasi icin ID'si parametre olarak alinir.</param>
        /// <param name="password">Sifre degistirmek icin parametre olarak alinir.</param>
        public void UpdateUserPassword(int id, string password) {
            databaseUser.UpdateUserPassword(id, password);
        }

        /// <summary>
        /// Gecici veritabanindaki kullanici bilgilerini DataTable olarak dondurur.
        /// </summary>
        /// <returns>DataTable tipinde veritabani tablosu dondurur.</returns>
        public DataTable GetNotConfirmedUserData() {
            return databaseUser.GetNotConfirmedUserData();
        }

        /// <summary>
        /// Kullanicinin veritabanindaki mevcut bilgileri ID'si kullanilarak gonderilen parametreye esitlenir. Aldigi parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="customer">Bilgileri esitlemek icin parametre.</param>
        /// <param name="id">Istenilen kullaniciyi bulmak icin ID'si parametre olarak alinir.</param>
        public void GetUserData(ref ClassCustomer customer, int id) {
            databaseUser.GetUserData(ref customer, id);
        }

        /// <summary>
        /// Kullanicin e-posta adresiyle ID'sine ulasilir. Aldigi parametreyi ust sinifa gonderir.
        /// </summary>
        /// <param name="email">Kullaniciyi veritabaninda bulmak icin kullanilir.</param>
        /// <returns>Kullanicin ID'sini dondurur.</returns>
        public int GetUserID(string email) {
            return databaseUser.GetUserID(email);
        }

        /// <summary>
        /// Kitap bilgilerini veritabanindan bulup aldigi parametreye esitler. Parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="book">Veritabanindaki bilgileri esitler.</param>
        /// <param name="index">Istenilen kitabin bulunmasini saglar.</param>
        public void GetBookData(ClassBook book, int index) {
            databaseBook.GetBookData(book, index);
        }

        /// <summary>
        /// Kitabi veritabanina ekler. Parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="isbn">Kitabin ISBN'i parametre olarak alir.</param>
        /// <param name="name">Kitabin ismini parametre olarak alir.</param>
        /// <param name="author">Kitabin yazarini parametre olarak alir.</param>
        /// <param name="publisher">Kitabin yayinicisi parametre olarak alir.</param>
        /// <param name="page">Kitabin sayfa sayisini parametre olarak alir.</param>
        /// <param name="price">Kitabin fiyatini parametre olarak alir.</param>
        public void SetBookData(string isbn, string name, string author, string publisher, int page, float price) {
            databaseBook.SetBookData(isbn, name, author, publisher, page, price);
        }

        /// <summary>
        /// Magazin bilgilerini veritabanindan bulup aldigi parametreye esitler. Parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="magazine">Veritabanindaki bilgileri esitler.</param>
        /// <param name="index">Istenilen kitabin bulunmasini saglar.</param>
        public void GetMagazineData(ClassMagazine magazine, int index) {
            databaseMagazine.GetMagazineData(magazine, index);
        }

        /// <summary>
        /// Magazini veritabanina ekler. Parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="name">Magazinin ismini parametre olarak alir.</param>
        /// <param name="issue">Magazinin konusunu parametre olarak alir.</param>
        /// <param name="type">Magazinin tipini parametre olarak alir.</param>
        /// <param name="price">Magazinin fiyatini parametre olarak alir.</param>
        public void SetMagazineData(string name, string issue, int type, float price) {
            databaseMagazine.SetMagazineData(name, issue, type, price);
        }

        /// <summary>
        /// Muzik bilgilerini veritabanindan bulup aldigi parametreye esitler. Parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="musicCD">Veritabanindaki bilgileri esitler.</param>
        /// <param name="index">Istenilen kitabin bulunmasini saglar.</param>
        public void GetMusicCDData(ClassMusic musicCD, int index) {
            databaseMusic.GetMusicCDData(musicCD, index);
        }

        /// <summary>
        /// Muzigi veritabanina ekler. Parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="name">Muzigin ismini parametre olarak alir.</param>
        /// <param name="singer">Muzigi soyleyenini parametre olarak alir.</param>
        /// <param name="type">Muzigin tipini parametre olarak alir.</param>
        /// <param name="price">Muzigin ucretini parametre olarak alir.</param>
        public void SetMusicCDData(string name, string singer, int type, float price) {
            databaseMusic.SetMusicCDData(name, singer, type, price);
        }

        /// <summary>
        /// Kullanicinin gecmiste yaptigi satin alimlari DataTable olarak dondurur. Parametreyi ust sinifa gonderir.
        /// </summary>
        /// <param name="customerid">Kullaniciyi veritabaninda bulabilmek icin ID'sini parametre olarak alir.</param>
        /// <returns>DataTable tipinde veritabani tablosu dondurur.</returns>
        public DataTable GetOrderData(int customerid) {
            return databaseOrderItem.GetOrderData(customerid);
        }

        /// <summary>
        /// Kullanicinin gecmiste yaptigi satin alimlarin 2 parametresini DataTable olarak dondurur. Parametreyi ust sinifa gonderir.
        /// </summary>
        /// <param name="customerid">Kullaniciyi veritabaninda bulabilmek icin ID'sini parametre olarak alir.</param>
        /// <returns>DataTable tipinde veritabani tablosu dondurur.</returns>
        public DataTable GetOrderTwoData(int customerid)
        {
            return databaseOrderItem.GetOrderData(customerid);
        }

        /// <summary>
        /// Kullanicinin alisveris sirasinda satin aldigi urunleri veritabanina ekler. Parametreleri ust sinifa gonderir.
        /// </summary>
        /// <param name="customerid">Kullanici ID'sini parametre olarak alir.</param>
        /// <param name="type">Urunun tipini parametre olarak alir.</param>
        /// <param name="name">Urunun ismini parametre olarak alir.</param>
        /// <param name="unitprice">Urunun birim fiyatini parametre olarak alir.</param>
        /// <param name="quantity">Urunden kac tane alindigini parametre olarak alir.</param>
        /// <param name="totalprice">Urunun toplam tutarini parametre olarak alir.</param>
        /// <param name="itemid">Urunun ID'sini parametre olarak alir.</param>
        public void SetOrderData(int customerid, string type, string name, float unitprice, int quantity, float totalprice, int itemid) {
            databaseOrderItem.SetOrderData(customerid, type, name, unitprice, quantity, totalprice, itemid);
        }

        /// <summary>
        /// Istenilen tablodan indeksini parametre alarak ID'si dondurulur.
        /// </summary>
        /// <param name="tableName">Tablo adini parametre olarak alir.</param>
        /// <param name="index">Istenilenin indeksini parametre olarak alir.</param>
        /// <returns></returns>
        public int ReturnID(string tableName,int index)
        {
            connection.Open();
            int ID = 0;
            if (index == 1)
            {
                using (SqlCommand command = new SqlCommand("SELECT TOP 1 * FROM "+tableName, connection))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        dr.Read();
                        ID = Convert.ToInt32(dr["ID"]);
                    }
                }
            }
            else
            {
                using (SqlCommand command = new SqlCommand("SELECT TOP "+index+" * FROM " + tableName+" EXCEPT SELECT TOP "+(index-1)+ " * FROM " + tableName, connection))
                {
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        dr.Read();
                        ID = Convert.ToInt32(dr["ID"]);
                    }
                }
            }
            connection.Close();
            return ID;
        }

        /// <summary>
        /// Tablodaki eleman sayisini dondurur.
        /// </summary>
        /// <param name="tableName">Tablo adini parametre olarak alir.</param>
        /// <returns>Eleman sayisini dondurur.</returns>
        public int Count(string tableName)
        {
            connection.Open();
            int count = 0;
            using (SqlCommand command = new SqlCommand("SELECT COUNT(*) FROM " + tableName, connection))
            {

                count = (Int32)command.ExecuteScalar();
            }
            connection.Close();
            return count;
        }

        /// <summary>
        /// Son kaydedilenin ID'sini dondurur.
        /// </summary>
        /// <param name="tableName">Tablo adini parametre olarak alir.</param>
        /// <returns>Bulunan ID'yi dondurur.</returns>
        public int GetLastRecordID(string tableName)
        {
            connection.Open();
            int ID = 0;
            using (SqlCommand command = new SqlCommand(@"SELECT TOP 1 * FROM "+tableName+ " ORDER BY ID DESC", connection))
            {
                using (SqlDataReader dr = command.ExecuteReader())
                {
                    dr.Read();
                    ID = Convert.ToInt32(dr["ID"]);                    
                }
            }
            connection.Close();
            return ID;
        }
    }
}