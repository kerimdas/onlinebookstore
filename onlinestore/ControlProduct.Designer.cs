﻿namespace onlinestore {
    partial class ControlProduct {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlProduct));
            this.PanelPictureBox = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PictureBoxProduct = new System.Windows.Forms.PictureBox();
            this.PanelSidebarBottom = new System.Windows.Forms.Panel();
            this.PanelSidebarLeftBottom = new System.Windows.Forms.Panel();
            this.PanelSidebarRightBottom = new System.Windows.Forms.Panel();
            this.LabelProductNameText = new System.Windows.Forms.Label();
            this.LabelProductName = new System.Windows.Forms.Label();
            this.LabelProductPrice = new System.Windows.Forms.Label();
            this.ButtonAddCart = new System.Windows.Forms.Button();
            this.ButtonDetails = new System.Windows.Forms.Button();
            this.PanelSidebarButton = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.PanelSidebarBottomTop = new System.Windows.Forms.Panel();
            this.PanelSidebarName = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.PanelPictureBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxProduct)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelPictureBox
            // 
            this.PanelPictureBox.BackColor = System.Drawing.SystemColors.Control;
            this.PanelPictureBox.Controls.Add(this.panel2);
            this.PanelPictureBox.Controls.Add(this.PictureBoxProduct);
            this.PanelPictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelPictureBox.Location = new System.Drawing.Point(0, 0);
            this.PanelPictureBox.Name = "PanelPictureBox";
            this.PanelPictureBox.Size = new System.Drawing.Size(250, 210);
            this.PanelPictureBox.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel2.Location = new System.Drawing.Point(6, -58);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 16);
            this.panel2.TabIndex = 4;
            // 
            // PictureBoxProduct
            // 
            this.PictureBoxProduct.Location = new System.Drawing.Point(50, 27);
            this.PictureBoxProduct.Name = "PictureBoxProduct";
            this.PictureBoxProduct.Size = new System.Drawing.Size(150, 150);
            this.PictureBoxProduct.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxProduct.TabIndex = 0;
            this.PictureBoxProduct.TabStop = false;
            // 
            // PanelSidebarBottom
            // 
            this.PanelSidebarBottom.BackColor = System.Drawing.Color.White;
            this.PanelSidebarBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelSidebarBottom.Location = new System.Drawing.Point(0, 376);
            this.PanelSidebarBottom.Name = "PanelSidebarBottom";
            this.PanelSidebarBottom.Size = new System.Drawing.Size(250, 1);
            this.PanelSidebarBottom.TabIndex = 1;
            // 
            // PanelSidebarLeftBottom
            // 
            this.PanelSidebarLeftBottom.BackColor = System.Drawing.Color.White;
            this.PanelSidebarLeftBottom.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelSidebarLeftBottom.Location = new System.Drawing.Point(0, 210);
            this.PanelSidebarLeftBottom.Name = "PanelSidebarLeftBottom";
            this.PanelSidebarLeftBottom.Size = new System.Drawing.Size(1, 166);
            this.PanelSidebarLeftBottom.TabIndex = 3;
            // 
            // PanelSidebarRightBottom
            // 
            this.PanelSidebarRightBottom.BackColor = System.Drawing.Color.White;
            this.PanelSidebarRightBottom.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelSidebarRightBottom.Location = new System.Drawing.Point(249, 210);
            this.PanelSidebarRightBottom.Name = "PanelSidebarRightBottom";
            this.PanelSidebarRightBottom.Size = new System.Drawing.Size(1, 166);
            this.PanelSidebarRightBottom.TabIndex = 4;
            // 
            // LabelProductNameText
            // 
            this.LabelProductNameText.AutoSize = true;
            this.LabelProductNameText.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProductNameText.ForeColor = System.Drawing.Color.White;
            this.LabelProductNameText.Location = new System.Drawing.Point(15, 99);
            this.LabelProductNameText.Name = "LabelProductNameText";
            this.LabelProductNameText.Size = new System.Drawing.Size(50, 17);
            this.LabelProductNameText.TabIndex = 5;
            this.LabelProductNameText.Text = "Name";
            // 
            // LabelProductName
            // 
            this.LabelProductName.AutoSize = true;
            this.LabelProductName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProductName.ForeColor = System.Drawing.Color.White;
            this.LabelProductName.Location = new System.Drawing.Point(15, 115);
            this.LabelProductName.Name = "LabelProductName";
            this.LabelProductName.Size = new System.Drawing.Size(238, 23);
            this.LabelProductName.TabIndex = 6;
            this.LabelProductName.Text = "Kürk Mantolu Madonna";
            // 
            // LabelProductPrice
            // 
            this.LabelProductPrice.AutoSize = true;
            this.LabelProductPrice.Font = new System.Drawing.Font("Century Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelProductPrice.ForeColor = System.Drawing.Color.White;
            this.LabelProductPrice.Location = new System.Drawing.Point(12, 30);
            this.LabelProductPrice.Name = "LabelProductPrice";
            this.LabelProductPrice.Size = new System.Drawing.Size(209, 49);
            this.LabelProductPrice.TabIndex = 7;
            this.LabelProductPrice.Text = "11111,11₺";
            // 
            // ButtonAddCart
            // 
            this.ButtonAddCart.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ButtonAddCart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonAddCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddCart.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ButtonAddCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddCart.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAddCart.Image")));
            this.ButtonAddCart.Location = new System.Drawing.Point(185, 9);
            this.ButtonAddCart.Name = "ButtonAddCart";
            this.ButtonAddCart.Size = new System.Drawing.Size(56, 37);
            this.ButtonAddCart.TabIndex = 8;
            this.ButtonAddCart.UseVisualStyleBackColor = false;
            this.ButtonAddCart.Click += new System.EventHandler(this.ButtonAddCart_Click);
            // 
            // ButtonDetails
            // 
            this.ButtonDetails.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.ButtonDetails.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonDetails.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDetails.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.ButtonDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonDetails.Image = ((System.Drawing.Image)(resources.GetObject("ButtonDetails.Image")));
            this.ButtonDetails.Location = new System.Drawing.Point(185, 52);
            this.ButtonDetails.Name = "ButtonDetails";
            this.ButtonDetails.Size = new System.Drawing.Size(56, 37);
            this.ButtonDetails.TabIndex = 9;
            this.ButtonDetails.UseVisualStyleBackColor = false;
            this.ButtonDetails.Click += new System.EventHandler(this.ButtonDetails_Click);
            // 
            // PanelSidebarButton
            // 
            this.PanelSidebarButton.BackColor = System.Drawing.Color.White;
            this.PanelSidebarButton.Location = new System.Drawing.Point(178, 9);
            this.PanelSidebarButton.Name = "PanelSidebarButton";
            this.PanelSidebarButton.Size = new System.Drawing.Size(1, 80);
            this.PanelSidebarButton.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel3.Controls.Add(this.PanelSidebarBottomTop);
            this.panel3.Controls.Add(this.PanelSidebarName);
            this.panel3.Controls.Add(this.PanelSidebarButton);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.ButtonDetails);
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.ButtonAddCart);
            this.panel3.Controls.Add(this.panel6);
            this.panel3.Controls.Add(this.LabelProductPrice);
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.LabelProductName);
            this.panel3.Controls.Add(this.LabelProductNameText);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(1, 210);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(248, 166);
            this.panel3.TabIndex = 5;
            // 
            // PanelSidebarBottomTop
            // 
            this.PanelSidebarBottomTop.BackColor = System.Drawing.Color.White;
            this.PanelSidebarBottomTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelSidebarBottomTop.Location = new System.Drawing.Point(1, 1);
            this.PanelSidebarBottomTop.Name = "PanelSidebarBottomTop";
            this.PanelSidebarBottomTop.Size = new System.Drawing.Size(246, 1);
            this.PanelSidebarBottomTop.TabIndex = 6;
            // 
            // PanelSidebarName
            // 
            this.PanelSidebarName.BackColor = System.Drawing.Color.White;
            this.PanelSidebarName.Location = new System.Drawing.Point(19, 95);
            this.PanelSidebarName.Name = "PanelSidebarName";
            this.PanelSidebarName.Size = new System.Drawing.Size(152, 1);
            this.PanelSidebarName.TabIndex = 5;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(247, 1);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1, 165);
            this.panel4.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 1);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1, 165);
            this.panel5.TabIndex = 4;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel6.Location = new System.Drawing.Point(6, -58);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(250, 16);
            this.panel6.TabIndex = 4;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(248, 1);
            this.panel7.TabIndex = 2;
            // 
            // ProductControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.PanelSidebarRightBottom);
            this.Controls.Add(this.PanelSidebarLeftBottom);
            this.Controls.Add(this.PanelSidebarBottom);
            this.Controls.Add(this.PanelPictureBox);
            this.Font = new System.Drawing.Font("Microsoft Tai Le", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Name = "ProductControl";
            this.Size = new System.Drawing.Size(250, 377);
            this.PanelPictureBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxProduct)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelPictureBox;
        private System.Windows.Forms.PictureBox PictureBoxProduct;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel PanelSidebarBottom;
        private System.Windows.Forms.Panel PanelSidebarLeftBottom;
        private System.Windows.Forms.Panel PanelSidebarRightBottom;
        private System.Windows.Forms.Label LabelProductNameText;
        private System.Windows.Forms.Label LabelProductName;
        private System.Windows.Forms.Label LabelProductPrice;
        private System.Windows.Forms.Button ButtonAddCart;
        private System.Windows.Forms.Button ButtonDetails;
        private System.Windows.Forms.Panel PanelSidebarButton;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel PanelSidebarBottomTop;
        private System.Windows.Forms.Panel PanelSidebarName;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
    }
}
