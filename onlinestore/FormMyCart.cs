﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Sepet icin form.
    /// </summary>
    public partial class FormMyCart : Form {

        public static FormMyCart InstanceMyCart;
        ClassShoppingCart shoppingCart = new ClassShoppingCart();
        ISubjectUser subjectUser;
        ClassCustomer customer = ClassCustomer.InstanceCustomer;
        ClassDatabaseOrderItem classOrderItem = new ClassDatabaseOrderItem();

        public FormMyCart()
        {
            InitializeComponent();
        }

        public FormMyCart(ClassShoppingCart cart, ClassCustomer _customer)
        {
            shoppingCart = cart;
            InstanceMyCart = this;
            customer = _customer;
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormMyCart.", customer.Username, nameof(FormMyCart));
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is ecited from the FormMyCart.", customer.Username, nameof(ButtonExit_Click));
            this.Close();
        }

        /// <summary>
        /// Sepetteki urunleri satin alir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonProceed_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is buying the items.", customer.Username, nameof(ButtonProceed_Click));
            FormProductAdded productAddedPage = new FormProductAdded();
            shoppingCart.ItemsToPurchase.Clear();
            productAddedPage.ShowDialog();
            AddOrder();
            
            this.Close();
        }

        private void MyCartPage_Load(object sender, EventArgs e)
        {
            LabelTotalPrice.Text = shoppingCart.PaymentAmount.ToString("0.00") + "₺";
            GroupBoxCreditCard.Visible = false;

            DataGridViewImageColumn dataGridViewImageColumn = new DataGridViewImageColumn();
            dataGridViewImageColumn.ImageLayout = DataGridViewImageCellLayout.Zoom;

            DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn = new DataGridViewCheckBoxColumn();


            DataGridViewMyCart.Columns.Add(dataGridViewImageColumn);
            DataGridViewMyCart.ColumnCount = 7;

            DataGridViewMyCart.Columns[1].Name = "Item Name";
            DataGridViewMyCart.Columns[2].Name = "Unit Price";
            DataGridViewMyCart.Columns[3].Name = "Quantity";
            DataGridViewMyCart.Columns[4].Name = "Total";           
           
            DataGridViewMyCart.Columns["Unit Price"].DefaultCellStyle.Format = "#.#0";
            DataGridViewMyCart.Columns["Total"].DefaultCellStyle.Format = "#.#0";

            DataGridViewMyCart.Columns[1].ReadOnly = true;
            DataGridViewMyCart.Columns[2].ReadOnly = true; ;
            DataGridViewMyCart.Columns[3].ReadOnly = true; ;
            DataGridViewMyCart.Columns[4].ReadOnly = true; ;

            DataGridViewMyCart.Columns[5].Name = "Item Type";
            DataGridViewMyCart.Columns[6].Name = "ID";

            dataGridViewCheckBoxColumn.HeaderText = "Buy Item";

            dataGridViewCheckBoxColumn.FalseValue = 0;
            dataGridViewCheckBoxColumn.TrueValue = 1;
            DataGridViewMyCart.Columns.Add(dataGridViewCheckBoxColumn);

            for (int i = 0; i < shoppingCart.ItemsToPurchase.Count; i++)
            {
                ClassItemToPurchase item = new ClassItemToPurchase();
                item = ((ClassItemToPurchase)shoppingCart.ItemsToPurchase[i]);

                DataGridViewRow dataRow = (DataGridViewRow)DataGridViewMyCart.Rows[0].Clone();

                dataRow.Cells[0].Value = item.Product.CoverPicture;
                dataRow.Cells[1].Value = item.Product.Name;
                dataRow.Cells[2].Value = item.Product.Price;
                dataRow.Cells[3].Value = item.Quantity;
                dataRow.Cells[4].Value = item.Product.Price * item.Quantity;
                dataRow.Cells[7].Value = true;
                dataRow.Cells[5].Value = item.Product.TYPE;/// invisible yap
                dataRow.Cells[6].Value = item.Product.ID;//invisible yap
                
                DataGridViewMyCart.Rows.Add(dataRow);
            }            
            DataGridViewMyCart.AllowUserToAddRows = false;
        }

        /// <summary>
        /// Odeme yonetimini ayarlar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void RadioButtonCreditCard_CheckedChanged(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is selecting payment method.", customer.Username, nameof(RadioButtonCreditCard_CheckedChanged));
            if (RadioButtonCreditCard.Checked == true)
            {
                GroupBoxCreditCard.Visible = true;
            }
            else
            {
                GroupBoxCreditCard.Visible = false;
            }
        }

        /// <summary>
        /// Sepetteki urune tiklar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void DataGridViewMyCart_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            subjectUser.Notify("Observer - User is selecting items to buy.", customer.Username, nameof(DataGridViewMyCart_CellContentClick));
            DataGridViewMyCart.CommitEdit(DataGridViewDataErrorContexts.Commit);
        }

        /// <summary>
        /// Sepetten urun cikarir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void DataGridViewMyCart_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            double result = 0;
            int i = 0;
            foreach (DataGridViewRow row in DataGridViewMyCart.Rows)
            {
                if (Convert.ToBoolean(row.Cells[7].Value) != false)
                {
                    result += Convert.ToDouble(row.Cells[4].Value);

                }
                i++;
            }
            DataGridViewMyCart.Update();
            LabelTotalPrice.Text = result.ToString("0.00") + "₺";
            shoppingCart.PaymentAmount = result;
        }

        /// <summary>
        /// Zamani gunceller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TimerRefreshMyCartEvents_Tick(object sender, EventArgs e)
        {
            double result = 0;
            int i = 0;
            foreach (DataGridViewRow row in DataGridViewMyCart.Rows)
            {
                if (Convert.ToBoolean(row.Cells[7].Value) != false)
                {
                    result += Convert.ToDouble(row.Cells[4].Value);
                }
                i++;
            }
            LabelTotalPrice.Text = result.ToString("0.00") + "₺";
            shoppingCart.PaymentAmount = result;
        }

        /// <summary>
        /// Siparisi kabul ederken verileri veritabanina ekler.
        /// </summary>
        private void AddOrder()
        {
            foreach (DataGridViewRow row in DataGridViewMyCart.Rows)
            {
                if (Convert.ToBoolean(row.Cells[7].Value) != false)
                {
                    string type = (row.Cells[5].Value.ToString());
                    string name = Convert.ToString(row.Cells[1].Value);
                    float unitprice = float.Parse(row.Cells[2].Value.ToString());
                    int quantity = Convert.ToInt32(row.Cells[3].Value);
                    float total = float.Parse(row.Cells[4].Value.ToString());
                    int itemid = Convert.ToInt32(row.Cells[6].Value);

                    classOrderItem.SetOrderData(customer.ID, type, name , unitprice ,quantity ,total ,itemid );
                }
            }
        }
    }
}
