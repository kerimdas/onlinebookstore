﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore {
    /// <summary>
    /// Observer sinifi icin arayuz.
    /// </summary>
    public interface IObserverUser {
        void Update(string data, string username, object _object);
        void Log(string username, object _object);
    }
}
