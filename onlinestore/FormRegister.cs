﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Kayit olma formu.
    /// </summary>
    public partial class FormRegister : Form {

        private bool passwordOK = false;
        ISubjectUser subjectUser;

        public FormRegister() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormRegister.", "system", nameof(FormRegister));
        }

        /// <summary>
        /// Kullanici kaydini yapar, veritabanina ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonRegisterPageSignUp_Click(object sender, EventArgs e) {
            ClassDatabase database = ClassDatabase.connect();           

            string username = TextBoxRegisterPageUsername.Text;
            string password = TextBoxRegisterPagePassword.Text;
            string confirmPassword = TextBoxRegisterPageConfirmPassword.Text;
            string name = TextBoxRegisterPageName.Text;
            string email = TextBoxRegisterPageEmail.Text;
            string address = MultiTextBoxRegisterPageAddress.Text;

            if (!database.CheckUserExist(username) && !database.CheckUserEmailExist(email)) {
                database.SetNotConfirmedUserData(username, password, name, email, address);
                subjectUser.Notify("Observer - User is signed up.", "system", nameof(ButtonRegisterPageSignUp_Click));
                this.Close();
            } else {
                FormMessageBox.Show("Error", "Username or E-mail exist in the system. Please try again!");
            }
        }

        /// <summary>
        /// Zamani gunceller.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TimerRefreshEventsRegisterPage_Tick(object sender, EventArgs e)
        {
            PasswordLength();
            if(TextBoxRegisterPagePassword.Text.Length >= 6)
            {
                if (TextBoxRegisterPageConfirmPassword.Text == TextBoxRegisterPagePassword.Text)
                {
                    passwordOK = true;
                    LabelRegisterPageNotConfirmPassword.ForeColor = Color.Green;
                    LabelRegisterPageNotConfirmPassword.Text = "(Password matched.)";
                }
                else
                {
                    passwordOK = false;
                    LabelRegisterPageNotConfirmPassword.ForeColor = Color.Red;
                    LabelRegisterPageNotConfirmPassword.Text = " (Password did not match.)";
                }
            }            
        }

        /// <summary>
        /// Sifre uzunluguna bakar.
        /// </summary>
        private void PasswordLength()
        {
            if (TextBoxRegisterPagePassword.Text.Length < 6)
            {
                LabelRegisterPageNotConfirmPassword.Text = "(Password minimum length should be 6)";
            }
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonRegisterPageExit_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is exited from the FormRegister.", "system", nameof(ButtonRegisterPageExit_Click));
            this.Close();
        }
    }
}
