﻿namespace onlinestore {
    partial class FormMagazine {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMagazine));
            this.TimerRefreshEventsMagazinePage = new System.Windows.Forms.Timer(this.components);
            this.TableLayoutPanelMagazine = new System.Windows.Forms.TableLayoutPanel();
            this.PanelMagazinePageBottom = new System.Windows.Forms.Panel();
            this.GroupBoxMagazine = new System.Windows.Forms.GroupBox();
            this.LabelTableLayoutText = new System.Windows.Forms.Label();
            this.LabelPanelDetailsText = new System.Windows.Forms.Label();
            this.PanelSeperatorIssue = new System.Windows.Forms.Panel();
            this.PanelSeperatorTopRight = new System.Windows.Forms.Panel();
            this.PanelSeperatorType = new System.Windows.Forms.Panel();
            this.PanelSeperatorName = new System.Windows.Forms.Panel();
            this.PanelSeperatorTop = new System.Windows.Forms.Panel();
            this.LabelDetailsPriceText = new System.Windows.Forms.Label();
            this.LabelDetailsTypeText = new System.Windows.Forms.Label();
            this.PanelMagazinePageTop = new System.Windows.Forms.Panel();
            this.LabelMyCartCount = new System.Windows.Forms.Label();
            this.ButtonMagazinePageMyCart = new System.Windows.Forms.Button();
            this.ButtonMagazinePageTopUsername = new System.Windows.Forms.Button();
            this.ButtonMagazinePageExit = new System.Windows.Forms.Button();
            this.LabelMagazinePageTop = new System.Windows.Forms.Label();
            this.PictureBoxMagazinePageTop = new System.Windows.Forms.PictureBox();
            this.LabelDetailsIssueText = new System.Windows.Forms.Label();
            this.LabelDetailsNameText = new System.Windows.Forms.Label();
            this.LabelDetailsPrice = new System.Windows.Forms.Label();
            this.LabelDetailsIssue = new System.Windows.Forms.Label();
            this.LabelDetailsName = new System.Windows.Forms.Label();
            this.LabelDetailsType = new System.Windows.Forms.Label();
            this.PanelDetails = new System.Windows.Forms.Panel();
            this.PictureBoxDetails = new System.Windows.Forms.PictureBox();
            this.GroupBoxMagazine.SuspendLayout();
            this.PanelMagazinePageTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMagazinePageTop)).BeginInit();
            this.PanelDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDetails)).BeginInit();
            this.SuspendLayout();
            // 
            // TimerRefreshEventsMagazinePage
            // 
            this.TimerRefreshEventsMagazinePage.Enabled = true;
            this.TimerRefreshEventsMagazinePage.Tick += new System.EventHandler(this.TimerRefreshEventsMagazinePage_Tick);
            // 
            // TableLayoutPanelMagazine
            // 
            this.TableLayoutPanelMagazine.AutoScroll = true;
            this.TableLayoutPanelMagazine.AutoSize = true;
            this.TableLayoutPanelMagazine.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.TableLayoutPanelMagazine.ColumnCount = 4;
            this.TableLayoutPanelMagazine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMagazine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMagazine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMagazine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.TableLayoutPanelMagazine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanelMagazine.Location = new System.Drawing.Point(4, 20);
            this.TableLayoutPanelMagazine.Margin = new System.Windows.Forms.Padding(4);
            this.TableLayoutPanelMagazine.Name = "TableLayoutPanelMagazine";
            this.TableLayoutPanelMagazine.RowCount = 2;
            this.TableLayoutPanelMagazine.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelMagazine.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanelMagazine.Size = new System.Drawing.Size(540, 415);
            this.TableLayoutPanelMagazine.TabIndex = 0;
            // 
            // PanelMagazinePageBottom
            // 
            this.PanelMagazinePageBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelMagazinePageBottom.Location = new System.Drawing.Point(0, 609);
            this.PanelMagazinePageBottom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelMagazinePageBottom.Name = "PanelMagazinePageBottom";
            this.PanelMagazinePageBottom.Size = new System.Drawing.Size(1182, 44);
            this.PanelMagazinePageBottom.TabIndex = 27;
            // 
            // GroupBoxMagazine
            // 
            this.GroupBoxMagazine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxMagazine.Controls.Add(this.TableLayoutPanelMagazine);
            this.GroupBoxMagazine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GroupBoxMagazine.Location = new System.Drawing.Point(24, 102);
            this.GroupBoxMagazine.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBoxMagazine.Name = "GroupBoxMagazine";
            this.GroupBoxMagazine.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBoxMagazine.Size = new System.Drawing.Size(548, 439);
            this.GroupBoxMagazine.TabIndex = 22;
            this.GroupBoxMagazine.TabStop = false;
            // 
            // LabelTableLayoutText
            // 
            this.LabelTableLayoutText.AutoSize = true;
            this.LabelTableLayoutText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelTableLayoutText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelTableLayoutText.Location = new System.Drawing.Point(20, 75);
            this.LabelTableLayoutText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelTableLayoutText.Name = "LabelTableLayoutText";
            this.LabelTableLayoutText.Size = new System.Drawing.Size(115, 23);
            this.LabelTableLayoutText.TabIndex = 26;
            this.LabelTableLayoutText.Text = "Magazines";
            // 
            // LabelPanelDetailsText
            // 
            this.LabelPanelDetailsText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelPanelDetailsText.AutoEllipsis = true;
            this.LabelPanelDetailsText.AutoSize = true;
            this.LabelPanelDetailsText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelPanelDetailsText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelPanelDetailsText.Location = new System.Drawing.Point(580, 75);
            this.LabelPanelDetailsText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelPanelDetailsText.Name = "LabelPanelDetailsText";
            this.LabelPanelDetailsText.Size = new System.Drawing.Size(75, 23);
            this.LabelPanelDetailsText.TabIndex = 25;
            this.LabelPanelDetailsText.Text = "Details";
            this.LabelPanelDetailsText.Visible = false;
            // 
            // PanelSeperatorIssue
            // 
            this.PanelSeperatorIssue.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorIssue.Location = new System.Drawing.Point(5, 329);
            this.PanelSeperatorIssue.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorIssue.Name = "PanelSeperatorIssue";
            this.PanelSeperatorIssue.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorIssue.TabIndex = 20;
            // 
            // PanelSeperatorTopRight
            // 
            this.PanelSeperatorTopRight.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorTopRight.Location = new System.Drawing.Point(309, 220);
            this.PanelSeperatorTopRight.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorTopRight.Name = "PanelSeperatorTopRight";
            this.PanelSeperatorTopRight.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorTopRight.TabIndex = 21;
            // 
            // PanelSeperatorType
            // 
            this.PanelSeperatorType.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorType.Location = new System.Drawing.Point(309, 277);
            this.PanelSeperatorType.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorType.Name = "PanelSeperatorType";
            this.PanelSeperatorType.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorType.TabIndex = 20;
            // 
            // PanelSeperatorName
            // 
            this.PanelSeperatorName.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorName.Location = new System.Drawing.Point(5, 277);
            this.PanelSeperatorName.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorName.Name = "PanelSeperatorName";
            this.PanelSeperatorName.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorName.TabIndex = 19;
            // 
            // PanelSeperatorTop
            // 
            this.PanelSeperatorTop.BackColor = System.Drawing.Color.Black;
            this.PanelSeperatorTop.Location = new System.Drawing.Point(5, 220);
            this.PanelSeperatorTop.Margin = new System.Windows.Forms.Padding(4);
            this.PanelSeperatorTop.Name = "PanelSeperatorTop";
            this.PanelSeperatorTop.Size = new System.Drawing.Size(267, 1);
            this.PanelSeperatorTop.TabIndex = 18;
            // 
            // LabelDetailsPriceText
            // 
            this.LabelDetailsPriceText.AutoSize = true;
            this.LabelDetailsPriceText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPriceText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsPriceText.Location = new System.Drawing.Point(223, 46);
            this.LabelDetailsPriceText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsPriceText.Name = "LabelDetailsPriceText";
            this.LabelDetailsPriceText.Size = new System.Drawing.Size(42, 17);
            this.LabelDetailsPriceText.TabIndex = 17;
            this.LabelDetailsPriceText.Text = "Price";
            // 
            // LabelDetailsTypeText
            // 
            this.LabelDetailsTypeText.AutoSize = true;
            this.LabelDetailsTypeText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsTypeText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsTypeText.Location = new System.Drawing.Point(305, 225);
            this.LabelDetailsTypeText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsTypeText.Name = "LabelDetailsTypeText";
            this.LabelDetailsTypeText.Size = new System.Drawing.Size(39, 17);
            this.LabelDetailsTypeText.TabIndex = 15;
            this.LabelDetailsTypeText.Text = "Type";
            // 
            // PanelMagazinePageTop
            // 
            this.PanelMagazinePageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelMagazinePageTop.Controls.Add(this.LabelMyCartCount);
            this.PanelMagazinePageTop.Controls.Add(this.ButtonMagazinePageMyCart);
            this.PanelMagazinePageTop.Controls.Add(this.ButtonMagazinePageTopUsername);
            this.PanelMagazinePageTop.Controls.Add(this.ButtonMagazinePageExit);
            this.PanelMagazinePageTop.Controls.Add(this.LabelMagazinePageTop);
            this.PanelMagazinePageTop.Controls.Add(this.PictureBoxMagazinePageTop);
            this.PanelMagazinePageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMagazinePageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelMagazinePageTop.Margin = new System.Windows.Forms.Padding(4);
            this.PanelMagazinePageTop.Name = "PanelMagazinePageTop";
            this.PanelMagazinePageTop.Size = new System.Drawing.Size(1182, 69);
            this.PanelMagazinePageTop.TabIndex = 23;
            // 
            // LabelMyCartCount
            // 
            this.LabelMyCartCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelMyCartCount.AutoSize = true;
            this.LabelMyCartCount.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMyCartCount.ForeColor = System.Drawing.Color.White;
            this.LabelMyCartCount.Location = new System.Drawing.Point(886, 7);
            this.LabelMyCartCount.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelMyCartCount.Name = "LabelMyCartCount";
            this.LabelMyCartCount.Size = new System.Drawing.Size(19, 19);
            this.LabelMyCartCount.TabIndex = 5;
            this.LabelMyCartCount.Text = "0";
            // 
            // ButtonMagazinePageMyCart
            // 
            this.ButtonMagazinePageMyCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMagazinePageMyCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMagazinePageMyCart.FlatAppearance.BorderSize = 0;
            this.ButtonMagazinePageMyCart.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMagazinePageMyCart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMagazinePageMyCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMagazinePageMyCart.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMagazinePageMyCart.ForeColor = System.Drawing.Color.White;
            this.ButtonMagazinePageMyCart.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMagazinePageMyCart.Image")));
            this.ButtonMagazinePageMyCart.Location = new System.Drawing.Point(842, 4);
            this.ButtonMagazinePageMyCart.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonMagazinePageMyCart.Name = "ButtonMagazinePageMyCart";
            this.ButtonMagazinePageMyCart.Size = new System.Drawing.Size(78, 62);
            this.ButtonMagazinePageMyCart.TabIndex = 4;
            this.ButtonMagazinePageMyCart.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ButtonMagazinePageMyCart.UseVisualStyleBackColor = true;
            this.ButtonMagazinePageMyCart.Click += new System.EventHandler(this.ButtonMagazinePageMyCart_Click);
            // 
            // ButtonMagazinePageTopUsername
            // 
            this.ButtonMagazinePageTopUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMagazinePageTopUsername.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMagazinePageTopUsername.FlatAppearance.BorderSize = 0;
            this.ButtonMagazinePageTopUsername.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMagazinePageTopUsername.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMagazinePageTopUsername.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMagazinePageTopUsername.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonMagazinePageTopUsername.ForeColor = System.Drawing.Color.White;
            this.ButtonMagazinePageTopUsername.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMagazinePageTopUsername.Image")));
            this.ButtonMagazinePageTopUsername.Location = new System.Drawing.Point(934, 4);
            this.ButtonMagazinePageTopUsername.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonMagazinePageTopUsername.Name = "ButtonMagazinePageTopUsername";
            this.ButtonMagazinePageTopUsername.Size = new System.Drawing.Size(165, 62);
            this.ButtonMagazinePageTopUsername.TabIndex = 3;
            this.ButtonMagazinePageTopUsername.Text = "Beta User";
            this.ButtonMagazinePageTopUsername.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ButtonMagazinePageTopUsername.UseVisualStyleBackColor = true;
            this.ButtonMagazinePageTopUsername.Click += new System.EventHandler(this.ButtonMagazinePageTopUsername_Click);
            // 
            // ButtonMagazinePageExit
            // 
            this.ButtonMagazinePageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonMagazinePageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonMagazinePageExit.FlatAppearance.BorderSize = 0;
            this.ButtonMagazinePageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMagazinePageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonMagazinePageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonMagazinePageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonMagazinePageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonMagazinePageExit.Image")));
            this.ButtonMagazinePageExit.Location = new System.Drawing.Point(1107, 5);
            this.ButtonMagazinePageExit.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonMagazinePageExit.Name = "ButtonMagazinePageExit";
            this.ButtonMagazinePageExit.Size = new System.Drawing.Size(71, 60);
            this.ButtonMagazinePageExit.TabIndex = 1;
            this.ButtonMagazinePageExit.UseVisualStyleBackColor = true;
            this.ButtonMagazinePageExit.Click += new System.EventHandler(this.ButtonMagazinePageExit_Click);
            // 
            // LabelMagazinePageTop
            // 
            this.LabelMagazinePageTop.AutoSize = true;
            this.LabelMagazinePageTop.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMagazinePageTop.ForeColor = System.Drawing.Color.White;
            this.LabelMagazinePageTop.Location = new System.Drawing.Point(99, 20);
            this.LabelMagazinePageTop.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelMagazinePageTop.Name = "LabelMagazinePageTop";
            this.LabelMagazinePageTop.Size = new System.Drawing.Size(246, 32);
            this.LabelMagazinePageTop.TabIndex = 2;
            this.LabelMagazinePageTop.Text = "Online Book Store";
            // 
            // PictureBoxMagazinePageTop
            // 
            this.PictureBoxMagazinePageTop.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxMagazinePageTop.Image")));
            this.PictureBoxMagazinePageTop.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxMagazinePageTop.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBoxMagazinePageTop.Name = "PictureBoxMagazinePageTop";
            this.PictureBoxMagazinePageTop.Size = new System.Drawing.Size(91, 69);
            this.PictureBoxMagazinePageTop.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxMagazinePageTop.TabIndex = 1;
            this.PictureBoxMagazinePageTop.TabStop = false;
            // 
            // LabelDetailsIssueText
            // 
            this.LabelDetailsIssueText.AutoSize = true;
            this.LabelDetailsIssueText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsIssueText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsIssueText.Location = new System.Drawing.Point(1, 281);
            this.LabelDetailsIssueText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsIssueText.Name = "LabelDetailsIssueText";
            this.LabelDetailsIssueText.Size = new System.Drawing.Size(41, 17);
            this.LabelDetailsIssueText.TabIndex = 14;
            this.LabelDetailsIssueText.Text = "Issue";
            // 
            // LabelDetailsNameText
            // 
            this.LabelDetailsNameText.AutoSize = true;
            this.LabelDetailsNameText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsNameText.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsNameText.Location = new System.Drawing.Point(1, 225);
            this.LabelDetailsNameText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsNameText.Name = "LabelDetailsNameText";
            this.LabelDetailsNameText.Size = new System.Drawing.Size(50, 17);
            this.LabelDetailsNameText.TabIndex = 13;
            this.LabelDetailsNameText.Text = "Name";
            // 
            // LabelDetailsPrice
            // 
            this.LabelDetailsPrice.AutoSize = true;
            this.LabelDetailsPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelDetailsPrice.Font = new System.Drawing.Font("Century Gothic", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsPrice.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.LabelDetailsPrice.Location = new System.Drawing.Point(227, 64);
            this.LabelDetailsPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsPrice.Name = "LabelDetailsPrice";
            this.LabelDetailsPrice.Size = new System.Drawing.Size(185, 72);
            this.LabelDetailsPrice.TabIndex = 11;
            this.LabelDetailsPrice.Text = "5,63₺";
            // 
            // LabelDetailsIssue
            // 
            this.LabelDetailsIssue.AutoSize = true;
            this.LabelDetailsIssue.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsIssue.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsIssue.Location = new System.Drawing.Point(0, 300);
            this.LabelDetailsIssue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsIssue.Name = "LabelDetailsIssue";
            this.LabelDetailsIssue.Size = new System.Drawing.Size(43, 23);
            this.LabelDetailsIssue.TabIndex = 9;
            this.LabelDetailsIssue.Text = "164";
            // 
            // LabelDetailsName
            // 
            this.LabelDetailsName.AutoEllipsis = true;
            this.LabelDetailsName.AutoSize = true;
            this.LabelDetailsName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsName.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsName.Location = new System.Drawing.Point(0, 243);
            this.LabelDetailsName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsName.Name = "LabelDetailsName";
            this.LabelDetailsName.Size = new System.Drawing.Size(238, 23);
            this.LabelDetailsName.TabIndex = 7;
            this.LabelDetailsName.Text = "Kürk Mantolu Madonna";
            // 
            // LabelDetailsType
            // 
            this.LabelDetailsType.AutoEllipsis = true;
            this.LabelDetailsType.AutoSize = true;
            this.LabelDetailsType.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDetailsType.ForeColor = System.Drawing.Color.Black;
            this.LabelDetailsType.Location = new System.Drawing.Point(304, 243);
            this.LabelDetailsType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelDetailsType.Name = "LabelDetailsType";
            this.LabelDetailsType.Size = new System.Drawing.Size(146, 23);
            this.LabelDetailsType.TabIndex = 6;
            this.LabelDetailsType.Text = "Sabahattin Ali";
            // 
            // PanelDetails
            // 
            this.PanelDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelDetails.BackColor = System.Drawing.Color.White;
            this.PanelDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelDetails.Controls.Add(this.PanelSeperatorIssue);
            this.PanelDetails.Controls.Add(this.PanelSeperatorTopRight);
            this.PanelDetails.Controls.Add(this.PanelSeperatorType);
            this.PanelDetails.Controls.Add(this.PanelSeperatorName);
            this.PanelDetails.Controls.Add(this.PanelSeperatorTop);
            this.PanelDetails.Controls.Add(this.LabelDetailsPriceText);
            this.PanelDetails.Controls.Add(this.LabelDetailsTypeText);
            this.PanelDetails.Controls.Add(this.LabelDetailsIssueText);
            this.PanelDetails.Controls.Add(this.LabelDetailsNameText);
            this.PanelDetails.Controls.Add(this.LabelDetailsPrice);
            this.PanelDetails.Controls.Add(this.LabelDetailsIssue);
            this.PanelDetails.Controls.Add(this.LabelDetailsName);
            this.PanelDetails.Controls.Add(this.LabelDetailsType);
            this.PanelDetails.Controls.Add(this.PictureBoxDetails);
            this.PanelDetails.Location = new System.Drawing.Point(580, 102);
            this.PanelDetails.Margin = new System.Windows.Forms.Padding(4);
            this.PanelDetails.Name = "PanelDetails";
            this.PanelDetails.Size = new System.Drawing.Size(574, 409);
            this.PanelDetails.TabIndex = 24;
            this.PanelDetails.Visible = false;
            // 
            // PictureBoxDetails
            // 
            this.PictureBoxDetails.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxDetails.Image")));
            this.PictureBoxDetails.Location = new System.Drawing.Point(5, 5);
            this.PictureBoxDetails.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBoxDetails.Name = "PictureBoxDetails";
            this.PictureBoxDetails.Size = new System.Drawing.Size(213, 197);
            this.PictureBoxDetails.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxDetails.TabIndex = 0;
            this.PictureBoxDetails.TabStop = false;
            // 
            // MagazinePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 653);
            this.Controls.Add(this.PanelMagazinePageBottom);
            this.Controls.Add(this.GroupBoxMagazine);
            this.Controls.Add(this.LabelTableLayoutText);
            this.Controls.Add(this.LabelPanelDetailsText);
            this.Controls.Add(this.PanelMagazinePageTop);
            this.Controls.Add(this.PanelDetails);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MagazinePage";
            this.Text = "MagazinePage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MagazinePage_FormClosing);
            this.Load += new System.EventHandler(this.MagazinePage_Load);
            this.DoubleClick += new System.EventHandler(this.MagazinePage_DoubleClick);
            this.GroupBoxMagazine.ResumeLayout(false);
            this.GroupBoxMagazine.PerformLayout();
            this.PanelMagazinePageTop.ResumeLayout(false);
            this.PanelMagazinePageTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMagazinePageTop)).EndInit();
            this.PanelDetails.ResumeLayout(false);
            this.PanelDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxDetails)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer TimerRefreshEventsMagazinePage;
        private System.Windows.Forms.TableLayoutPanel TableLayoutPanelMagazine;
        private System.Windows.Forms.Panel PanelMagazinePageBottom;
        private System.Windows.Forms.GroupBox GroupBoxMagazine;
        private System.Windows.Forms.Label LabelTableLayoutText;
        private System.Windows.Forms.Label LabelPanelDetailsText;
        private System.Windows.Forms.Panel PanelSeperatorIssue;
        private System.Windows.Forms.Panel PanelSeperatorTopRight;
        private System.Windows.Forms.Panel PanelSeperatorType;
        private System.Windows.Forms.Panel PanelSeperatorName;
        private System.Windows.Forms.Panel PanelSeperatorTop;
        private System.Windows.Forms.Label LabelDetailsPriceText;
        private System.Windows.Forms.Label LabelDetailsTypeText;
        private System.Windows.Forms.Panel PanelMagazinePageTop;
        private System.Windows.Forms.Label LabelMyCartCount;
        private System.Windows.Forms.Button ButtonMagazinePageMyCart;
        private System.Windows.Forms.Button ButtonMagazinePageTopUsername;
        private System.Windows.Forms.Button ButtonMagazinePageExit;
        private System.Windows.Forms.Label LabelMagazinePageTop;
        private System.Windows.Forms.PictureBox PictureBoxMagazinePageTop;
        private System.Windows.Forms.Label LabelDetailsIssueText;
        private System.Windows.Forms.Label LabelDetailsNameText;
        private System.Windows.Forms.Label LabelDetailsPrice;
        private System.Windows.Forms.Label LabelDetailsIssue;
        private System.Windows.Forms.Label LabelDetailsName;
        private System.Windows.Forms.Label LabelDetailsType;
        private System.Windows.Forms.Panel PanelDetails;
        private System.Windows.Forms.PictureBox PictureBoxDetails;
    }
}