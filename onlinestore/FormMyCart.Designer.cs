﻿namespace onlinestore {
    partial class FormMyCart {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMyCart));
            this.PanelPurchaseTop = new System.Windows.Forms.Panel();
            this.ButtonExit = new System.Windows.Forms.Button();
            this.LabelMyCartTopText = new System.Windows.Forms.Label();
            this.DataGridViewMyCart = new System.Windows.Forms.DataGridView();
            this.PanelPage = new System.Windows.Forms.Panel();
            this.ButtonProceed = new System.Windows.Forms.Button();
            this.GroupBoxCreditCard = new System.Windows.Forms.GroupBox();
            this.TextBoxCVC = new System.Windows.Forms.TextBox();
            this.LabelCVC = new System.Windows.Forms.Label();
            this.LabelCreditCardDate = new System.Windows.Forms.Label();
            this.LabelCreditCard = new System.Windows.Forms.Label();
            this.MaskedTextBoxExpretionDate = new System.Windows.Forms.MaskedTextBox();
            this.MaskedTextBoxCreditCard = new System.Windows.Forms.MaskedTextBox();
            this.GroupBoxPaymentMethod = new System.Windows.Forms.GroupBox();
            this.RadioButtonCreditCard = new System.Windows.Forms.RadioButton();
            this.RadioButtonCash = new System.Windows.Forms.RadioButton();
            this.LabelTotalPrice = new System.Windows.Forms.Label();
            this.LabelTotalPriceText = new System.Windows.Forms.Label();
            this.TimerRefreshMyCartEvents = new System.Windows.Forms.Timer(this.components);
            this.PanelPurchaseTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewMyCart)).BeginInit();
            this.PanelPage.SuspendLayout();
            this.GroupBoxCreditCard.SuspendLayout();
            this.GroupBoxPaymentMethod.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelPurchaseTop
            // 
            this.PanelPurchaseTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.PanelPurchaseTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPurchaseTop.Controls.Add(this.ButtonExit);
            this.PanelPurchaseTop.Controls.Add(this.LabelMyCartTopText);
            this.PanelPurchaseTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelPurchaseTop.Location = new System.Drawing.Point(0, 0);
            this.PanelPurchaseTop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PanelPurchaseTop.Name = "PanelPurchaseTop";
            this.PanelPurchaseTop.Size = new System.Drawing.Size(1067, 46);
            this.PanelPurchaseTop.TabIndex = 1;
            // 
            // ButtonExit
            // 
            this.ButtonExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonExit.FlatAppearance.BorderSize = 0;
            this.ButtonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonExit.Image")));
            this.ButtonExit.Location = new System.Drawing.Point(965, 4);
            this.ButtonExit.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonExit.Name = "ButtonExit";
            this.ButtonExit.Size = new System.Drawing.Size(85, 38);
            this.ButtonExit.TabIndex = 1;
            this.ButtonExit.UseVisualStyleBackColor = true;
            this.ButtonExit.Click += new System.EventHandler(this.ButtonExit_Click);
            // 
            // LabelMyCartTopText
            // 
            this.LabelMyCartTopText.AutoSize = true;
            this.LabelMyCartTopText.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelMyCartTopText.ForeColor = System.Drawing.Color.White;
            this.LabelMyCartTopText.Location = new System.Drawing.Point(44, 9);
            this.LabelMyCartTopText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelMyCartTopText.Name = "LabelMyCartTopText";
            this.LabelMyCartTopText.Size = new System.Drawing.Size(105, 28);
            this.LabelMyCartTopText.TabIndex = 0;
            this.LabelMyCartTopText.Text = "My Cart";
            // 
            // DataGridViewMyCart
            // 
            this.DataGridViewMyCart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridViewMyCart.ColumnHeadersHeight = 30;
            this.DataGridViewMyCart.Location = new System.Drawing.Point(49, 27);
            this.DataGridViewMyCart.Margin = new System.Windows.Forms.Padding(4);
            this.DataGridViewMyCart.MultiSelect = false;
            this.DataGridViewMyCart.Name = "DataGridViewMyCart";
            this.DataGridViewMyCart.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToFirstHeader;
            this.DataGridViewMyCart.RowTemplate.Height = 130;
            this.DataGridViewMyCart.Size = new System.Drawing.Size(973, 385);
            this.DataGridViewMyCart.TabIndex = 2;
            this.DataGridViewMyCart.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewMyCart_CellContentClick);
            this.DataGridViewMyCart.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewMyCart_CellValueChanged);
            // 
            // PanelPage
            // 
            this.PanelPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelPage.Controls.Add(this.ButtonProceed);
            this.PanelPage.Controls.Add(this.GroupBoxCreditCard);
            this.PanelPage.Controls.Add(this.GroupBoxPaymentMethod);
            this.PanelPage.Controls.Add(this.LabelTotalPrice);
            this.PanelPage.Controls.Add(this.LabelTotalPriceText);
            this.PanelPage.Controls.Add(this.DataGridViewMyCart);
            this.PanelPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelPage.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.PanelPage.Location = new System.Drawing.Point(0, 46);
            this.PanelPage.Margin = new System.Windows.Forms.Padding(4);
            this.PanelPage.Name = "PanelPage";
            this.PanelPage.Size = new System.Drawing.Size(1067, 692);
            this.PanelPage.TabIndex = 3;
            // 
            // ButtonProceed
            // 
            this.ButtonProceed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.ButtonProceed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonProceed.FlatAppearance.BorderSize = 0;
            this.ButtonProceed.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonProceed.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonProceed.ForeColor = System.Drawing.Color.White;
            this.ButtonProceed.Location = new System.Drawing.Point(853, 593);
            this.ButtonProceed.Margin = new System.Windows.Forms.Padding(4);
            this.ButtonProceed.Name = "ButtonProceed";
            this.ButtonProceed.Size = new System.Drawing.Size(169, 43);
            this.ButtonProceed.TabIndex = 8;
            this.ButtonProceed.Text = "Proceed";
            this.ButtonProceed.UseVisualStyleBackColor = false;
            this.ButtonProceed.Click += new System.EventHandler(this.ButtonProceed_Click);
            // 
            // GroupBoxCreditCard
            // 
            this.GroupBoxCreditCard.Controls.Add(this.TextBoxCVC);
            this.GroupBoxCreditCard.Controls.Add(this.LabelCVC);
            this.GroupBoxCreditCard.Controls.Add(this.LabelCreditCardDate);
            this.GroupBoxCreditCard.Controls.Add(this.LabelCreditCard);
            this.GroupBoxCreditCard.Controls.Add(this.MaskedTextBoxExpretionDate);
            this.GroupBoxCreditCard.Controls.Add(this.MaskedTextBoxCreditCard);
            this.GroupBoxCreditCard.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.GroupBoxCreditCard.Location = new System.Drawing.Point(340, 441);
            this.GroupBoxCreditCard.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBoxCreditCard.Name = "GroupBoxCreditCard";
            this.GroupBoxCreditCard.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBoxCreditCard.Size = new System.Drawing.Size(327, 204);
            this.GroupBoxCreditCard.TabIndex = 7;
            this.GroupBoxCreditCard.TabStop = false;
            this.GroupBoxCreditCard.Text = "Credit Card";
            // 
            // TextBoxCVC
            // 
            this.TextBoxCVC.Location = new System.Drawing.Point(51, 171);
            this.TextBoxCVC.Margin = new System.Windows.Forms.Padding(4);
            this.TextBoxCVC.MaxLength = 3;
            this.TextBoxCVC.Name = "TextBoxCVC";
            this.TextBoxCVC.Size = new System.Drawing.Size(223, 24);
            this.TextBoxCVC.TabIndex = 5;
            this.TextBoxCVC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LabelCVC
            // 
            this.LabelCVC.AutoSize = true;
            this.LabelCVC.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelCVC.Location = new System.Drawing.Point(48, 153);
            this.LabelCVC.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelCVC.Name = "LabelCVC";
            this.LabelCVC.Size = new System.Drawing.Size(30, 14);
            this.LabelCVC.TabIndex = 4;
            this.LabelCVC.Text = "CVC";
            // 
            // LabelCreditCardDate
            // 
            this.LabelCreditCardDate.AutoSize = true;
            this.LabelCreditCardDate.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelCreditCardDate.Location = new System.Drawing.Point(49, 90);
            this.LabelCreditCardDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelCreditCardDate.Name = "LabelCreditCardDate";
            this.LabelCreditCardDate.Size = new System.Drawing.Size(77, 14);
            this.LabelCreditCardDate.TabIndex = 3;
            this.LabelCreditCardDate.Text = "Expretion Date";
            // 
            // LabelCreditCard
            // 
            this.LabelCreditCard.AutoSize = true;
            this.LabelCreditCard.Font = new System.Drawing.Font("Century Gothic", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelCreditCard.Location = new System.Drawing.Point(48, 34);
            this.LabelCreditCard.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelCreditCard.Name = "LabelCreditCard";
            this.LabelCreditCard.Size = new System.Drawing.Size(106, 14);
            this.LabelCreditCard.TabIndex = 2;
            this.LabelCreditCard.Text = "Credit Card Number";
            // 
            // MaskedTextBoxExpretionDate
            // 
            this.MaskedTextBoxExpretionDate.Location = new System.Drawing.Point(52, 108);
            this.MaskedTextBoxExpretionDate.Margin = new System.Windows.Forms.Padding(4);
            this.MaskedTextBoxExpretionDate.Mask = "(00) (00)";
            this.MaskedTextBoxExpretionDate.Name = "MaskedTextBoxExpretionDate";
            this.MaskedTextBoxExpretionDate.Size = new System.Drawing.Size(221, 24);
            this.MaskedTextBoxExpretionDate.TabIndex = 1;
            this.MaskedTextBoxExpretionDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MaskedTextBoxCreditCard
            // 
            this.MaskedTextBoxCreditCard.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.MaskedTextBoxCreditCard.Location = new System.Drawing.Point(52, 53);
            this.MaskedTextBoxCreditCard.Margin = new System.Windows.Forms.Padding(4);
            this.MaskedTextBoxCreditCard.Mask = "0000  0000  0000  0000";
            this.MaskedTextBoxCreditCard.Name = "MaskedTextBoxCreditCard";
            this.MaskedTextBoxCreditCard.Size = new System.Drawing.Size(223, 24);
            this.MaskedTextBoxCreditCard.TabIndex = 0;
            this.MaskedTextBoxCreditCard.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // GroupBoxPaymentMethod
            // 
            this.GroupBoxPaymentMethod.Controls.Add(this.RadioButtonCreditCard);
            this.GroupBoxPaymentMethod.Controls.Add(this.RadioButtonCash);
            this.GroupBoxPaymentMethod.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.GroupBoxPaymentMethod.Location = new System.Drawing.Point(49, 441);
            this.GroupBoxPaymentMethod.Margin = new System.Windows.Forms.Padding(4);
            this.GroupBoxPaymentMethod.Name = "GroupBoxPaymentMethod";
            this.GroupBoxPaymentMethod.Padding = new System.Windows.Forms.Padding(4);
            this.GroupBoxPaymentMethod.Size = new System.Drawing.Size(283, 112);
            this.GroupBoxPaymentMethod.TabIndex = 6;
            this.GroupBoxPaymentMethod.TabStop = false;
            this.GroupBoxPaymentMethod.Text = "Payment Method";
            // 
            // RadioButtonCreditCard
            // 
            this.RadioButtonCreditCard.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RadioButtonCreditCard.AutoSize = true;
            this.RadioButtonCreditCard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RadioButtonCreditCard.Location = new System.Drawing.Point(16, 68);
            this.RadioButtonCreditCard.Margin = new System.Windows.Forms.Padding(4);
            this.RadioButtonCreditCard.Name = "RadioButtonCreditCard";
            this.RadioButtonCreditCard.Size = new System.Drawing.Size(107, 21);
            this.RadioButtonCreditCard.TabIndex = 6;
            this.RadioButtonCreditCard.Text = "Credit Card";
            this.RadioButtonCreditCard.UseVisualStyleBackColor = true;
            this.RadioButtonCreditCard.CheckedChanged += new System.EventHandler(this.RadioButtonCreditCard_CheckedChanged);
            // 
            // RadioButtonCash
            // 
            this.RadioButtonCash.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RadioButtonCash.AutoSize = true;
            this.RadioButtonCash.Checked = true;
            this.RadioButtonCash.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RadioButtonCash.Location = new System.Drawing.Point(16, 36);
            this.RadioButtonCash.Margin = new System.Windows.Forms.Padding(4);
            this.RadioButtonCash.Name = "RadioButtonCash";
            this.RadioButtonCash.Size = new System.Drawing.Size(63, 21);
            this.RadioButtonCash.TabIndex = 5;
            this.RadioButtonCash.TabStop = true;
            this.RadioButtonCash.Text = "Cash";
            this.RadioButtonCash.UseVisualStyleBackColor = true;
            // 
            // LabelTotalPrice
            // 
            this.LabelTotalPrice.AutoSize = true;
            this.LabelTotalPrice.Font = new System.Drawing.Font("Century Gothic", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelTotalPrice.Location = new System.Drawing.Point(869, 459);
            this.LabelTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelTotalPrice.Name = "LabelTotalPrice";
            this.LabelTotalPrice.Size = new System.Drawing.Size(144, 32);
            this.LabelTotalPrice.TabIndex = 4;
            this.LabelTotalPrice.Text = "11111,11₺";
            // 
            // LabelTotalPriceText
            // 
            this.LabelTotalPriceText.AutoSize = true;
            this.LabelTotalPriceText.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelTotalPriceText.Location = new System.Drawing.Point(872, 441);
            this.LabelTotalPriceText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LabelTotalPriceText.Name = "LabelTotalPriceText";
            this.LabelTotalPriceText.Size = new System.Drawing.Size(42, 17);
            this.LabelTotalPriceText.TabIndex = 3;
            this.LabelTotalPriceText.Text = "Price";
            // 
            // TimerRefreshMyCartEvents
            // 
            this.TimerRefreshMyCartEvents.Enabled = true;
            this.TimerRefreshMyCartEvents.Interval = 1;
            this.TimerRefreshMyCartEvents.Tick += new System.EventHandler(this.TimerRefreshMyCartEvents_Tick);
            // 
            // MyCartPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 738);
            this.Controls.Add(this.PanelPage);
            this.Controls.Add(this.PanelPurchaseTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MyCartPage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyCartPage";
            this.Load += new System.EventHandler(this.MyCartPage_Load);
            this.PanelPurchaseTop.ResumeLayout(false);
            this.PanelPurchaseTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewMyCart)).EndInit();
            this.PanelPage.ResumeLayout(false);
            this.PanelPage.PerformLayout();
            this.GroupBoxCreditCard.ResumeLayout(false);
            this.GroupBoxCreditCard.PerformLayout();
            this.GroupBoxPaymentMethod.ResumeLayout(false);
            this.GroupBoxPaymentMethod.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelPurchaseTop;
        private System.Windows.Forms.Button ButtonExit;
        private System.Windows.Forms.Label LabelMyCartTopText;
        private System.Windows.Forms.DataGridView DataGridViewMyCart;
        private System.Windows.Forms.Panel PanelPage;
        private System.Windows.Forms.GroupBox GroupBoxCreditCard;
        private System.Windows.Forms.TextBox TextBoxCVC;
        private System.Windows.Forms.Label LabelCVC;
        private System.Windows.Forms.Label LabelCreditCardDate;
        private System.Windows.Forms.Label LabelCreditCard;
        private System.Windows.Forms.MaskedTextBox MaskedTextBoxExpretionDate;
        private System.Windows.Forms.MaskedTextBox MaskedTextBoxCreditCard;
        private System.Windows.Forms.GroupBox GroupBoxPaymentMethod;
        private System.Windows.Forms.RadioButton RadioButtonCreditCard;
        private System.Windows.Forms.RadioButton RadioButtonCash;
        private System.Windows.Forms.Label LabelTotalPrice;
        private System.Windows.Forms.Label LabelTotalPriceText;
        private System.Windows.Forms.Button ButtonProceed;
        private System.Windows.Forms.Timer TimerRefreshMyCartEvents;
    }
}