﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Giris ekrani formu.
    /// </summary>
    public partial class FormLogin : Form {

        public static ClassCustomer customer = new ClassCustomer();
        ISubjectUser subjectUser;
       
        static int IDCustomer;
        
        public FormLogin() {
            InitializeComponent();            
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is connected to the application.", "system", nameof(FormLogin));
            ClassCustomer.InstanceCustomer = customer;
            this.AcceptButton = ButtonLoginPageLogin;
        }

        ClassDatabase database = ClassDatabase.connect();

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonLoginPageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is disconnected from the application.", "system", nameof(ButtonLoginPageExit_Click));
            this.Close();
        }

        /// <summary>
        /// Kullanicinin giris yapmasini saglar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonLoginPageLogin_Click(object sender, EventArgs e)
        {
            if (database.GetUserDataToLogin(TextBoxLoginPageUsername.Text, TextBoxLoginPagePassword.Text, ref IDCustomer)) {
                database.GetUserData(ref customer, IDCustomer);
                if(customer.Username.Equals("admin") && customer.ID == 1) {
                    subjectUser.Notify("Observer - User is connected to the account.", customer.Username, nameof(ButtonLoginPageLogin_Click));
                    FormAdmin formAdmin = new FormAdmin();
                    formAdmin.ShowDialog();
                } else {
                    FormMain mainpage = new FormMain(ClassCustomer.InstanceCustomer);
                    mainpage = FormMain.InstanceMainPage;
                    subjectUser.Notify("Observer - User could not connected to the account.", customer.Username, nameof(ButtonLoginPageLogin_Click));
                    mainpage.ShowDialog();
                }
            } else {
                FormMessageBox.Show("Error", "You entered wrong username or password. Please try again!");
            }
            TextBoxLoginPageUsername.Clear();
            TextBoxLoginPagePassword.Clear();
        }

        /// <summary>
        /// Yeni kullanici ise kayıt olma formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void LabelLoginPageRegister_Click(object sender, EventArgs e)
        {
            FormRegister registerPage = new FormRegister();
            subjectUser.Notify("Observer - User is clicked to the register page.", "system", nameof(LabelLoginPageRegister_Click));
            registerPage.ShowDialog();
        }

        /// <summary>
        /// Kullanici sifresini unuttuysa, sifremi unuttum formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void LabelLoginPageForgotPassword_Click(object sender, EventArgs e)
        {
            FormForgotPassword forgotPasswordPage = new FormForgotPassword();
            subjectUser.Notify("Observer - User is clicked to the forgot password page.", "system", nameof(LabelLoginPageForgotPassword_Click));
            forgotPasswordPage.ShowDialog();
        }
    }
}