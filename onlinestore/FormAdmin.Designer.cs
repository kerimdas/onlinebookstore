﻿namespace onlinestore {
    partial class FormAdmin {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdmin));
            this.PanelAdminPageTop = new System.Windows.Forms.Panel();
            this.ButtonAdminPageExit = new System.Windows.Forms.Button();
            this.LabelAdminPageTop = new System.Windows.Forms.Label();
            this.ButtonAdminPageAddNewBook = new System.Windows.Forms.Button();
            this.ButtonAdminPageAddNewMusicCD = new System.Windows.Forms.Button();
            this.ButtonAdminPageAddNewMagazine = new System.Windows.Forms.Button();
            this.ButtonAdminPageAddNewCustomer = new System.Windows.Forms.Button();
            this.PanelAdminPageSidebar = new System.Windows.Forms.Panel();
            this.PanelAdminPageSidebarSeperatorDesignedBy = new System.Windows.Forms.Panel();
            this.LabelDesignedBy = new System.Windows.Forms.Label();
            this.PanelAdminPageSidebarSeperatorMusicCD = new System.Windows.Forms.Panel();
            this.PanelAdminPageSidebarSeperatorBook = new System.Windows.Forms.Panel();
            this.PanelAdminPageSidebarSeperatorCustomer = new System.Windows.Forms.Panel();
            this.PanelAdminPageSidebarSeperatorMagazine = new System.Windows.Forms.Panel();
            this.PanelAdminPageSidebarSeperatorTop = new System.Windows.Forms.Panel();
            this.PanelAdminPageTop.SuspendLayout();
            this.PanelAdminPageSidebar.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelAdminPageTop
            // 
            this.PanelAdminPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAdminPageTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAdminPageTop.Controls.Add(this.ButtonAdminPageExit);
            this.PanelAdminPageTop.Controls.Add(this.LabelAdminPageTop);
            this.PanelAdminPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelAdminPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelAdminPageTop.Name = "PanelAdminPageTop";
            this.PanelAdminPageTop.Size = new System.Drawing.Size(1200, 50);
            this.PanelAdminPageTop.TabIndex = 0;
            // 
            // ButtonAdminPageExit
            // 
            this.ButtonAdminPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonAdminPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAdminPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonAdminPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAdminPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAdminPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdminPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonAdminPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAdminPageExit.Image")));
            this.ButtonAdminPageExit.Location = new System.Drawing.Point(1139, 3);
            this.ButtonAdminPageExit.Name = "ButtonAdminPageExit";
            this.ButtonAdminPageExit.Size = new System.Drawing.Size(53, 46);
            this.ButtonAdminPageExit.TabIndex = 4;
            this.ButtonAdminPageExit.UseVisualStyleBackColor = true;
            this.ButtonAdminPageExit.Click += new System.EventHandler(this.ButtonAdminPageExit_Click);
            // 
            // LabelAdminPageTop
            // 
            this.LabelAdminPageTop.AutoSize = true;
            this.LabelAdminPageTop.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAdminPageTop.ForeColor = System.Drawing.Color.White;
            this.LabelAdminPageTop.Location = new System.Drawing.Point(74, 10);
            this.LabelAdminPageTop.Name = "LabelAdminPageTop";
            this.LabelAdminPageTop.Size = new System.Drawing.Size(141, 28);
            this.LabelAdminPageTop.TabIndex = 0;
            this.LabelAdminPageTop.Text = "Dashboard";
            // 
            // ButtonAdminPageAddNewBook
            // 
            this.ButtonAdminPageAddNewBook.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ButtonAdminPageAddNewBook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAdminPageAddNewBook.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAdminPageAddNewBook.FlatAppearance.BorderSize = 0;
            this.ButtonAdminPageAddNewBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdminPageAddNewBook.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAdminPageAddNewBook.ForeColor = System.Drawing.Color.White;
            this.ButtonAdminPageAddNewBook.Location = new System.Drawing.Point(12, 218);
            this.ButtonAdminPageAddNewBook.Name = "ButtonAdminPageAddNewBook";
            this.ButtonAdminPageAddNewBook.Size = new System.Drawing.Size(275, 50);
            this.ButtonAdminPageAddNewBook.TabIndex = 0;
            this.ButtonAdminPageAddNewBook.Text = "Add New Book";
            this.ButtonAdminPageAddNewBook.UseVisualStyleBackColor = false;
            this.ButtonAdminPageAddNewBook.Click += new System.EventHandler(this.ButtonAdminPageAddNewBook_Click);
            // 
            // ButtonAdminPageAddNewMusicCD
            // 
            this.ButtonAdminPageAddNewMusicCD.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ButtonAdminPageAddNewMusicCD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAdminPageAddNewMusicCD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAdminPageAddNewMusicCD.FlatAppearance.BorderSize = 0;
            this.ButtonAdminPageAddNewMusicCD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdminPageAddNewMusicCD.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAdminPageAddNewMusicCD.ForeColor = System.Drawing.Color.White;
            this.ButtonAdminPageAddNewMusicCD.Location = new System.Drawing.Point(11, 303);
            this.ButtonAdminPageAddNewMusicCD.Name = "ButtonAdminPageAddNewMusicCD";
            this.ButtonAdminPageAddNewMusicCD.Size = new System.Drawing.Size(275, 50);
            this.ButtonAdminPageAddNewMusicCD.TabIndex = 1;
            this.ButtonAdminPageAddNewMusicCD.Text = "Add New MusicCD";
            this.ButtonAdminPageAddNewMusicCD.UseVisualStyleBackColor = false;
            this.ButtonAdminPageAddNewMusicCD.Click += new System.EventHandler(this.ButtonAdminPageAddNewMusicCD_Click);
            // 
            // ButtonAdminPageAddNewMagazine
            // 
            this.ButtonAdminPageAddNewMagazine.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ButtonAdminPageAddNewMagazine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAdminPageAddNewMagazine.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAdminPageAddNewMagazine.FlatAppearance.BorderSize = 0;
            this.ButtonAdminPageAddNewMagazine.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdminPageAddNewMagazine.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAdminPageAddNewMagazine.ForeColor = System.Drawing.Color.White;
            this.ButtonAdminPageAddNewMagazine.Location = new System.Drawing.Point(11, 392);
            this.ButtonAdminPageAddNewMagazine.Name = "ButtonAdminPageAddNewMagazine";
            this.ButtonAdminPageAddNewMagazine.Size = new System.Drawing.Size(275, 50);
            this.ButtonAdminPageAddNewMagazine.TabIndex = 2;
            this.ButtonAdminPageAddNewMagazine.Text = "Add New Magazine";
            this.ButtonAdminPageAddNewMagazine.UseVisualStyleBackColor = false;
            this.ButtonAdminPageAddNewMagazine.Click += new System.EventHandler(this.ButtonAdminPageAddNewMagazine_Click);
            // 
            // ButtonAdminPageAddNewCustomer
            // 
            this.ButtonAdminPageAddNewCustomer.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ButtonAdminPageAddNewCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAdminPageAddNewCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAdminPageAddNewCustomer.FlatAppearance.BorderSize = 0;
            this.ButtonAdminPageAddNewCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAdminPageAddNewCustomer.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAdminPageAddNewCustomer.ForeColor = System.Drawing.Color.White;
            this.ButtonAdminPageAddNewCustomer.Location = new System.Drawing.Point(11, 132);
            this.ButtonAdminPageAddNewCustomer.Name = "ButtonAdminPageAddNewCustomer";
            this.ButtonAdminPageAddNewCustomer.Size = new System.Drawing.Size(275, 50);
            this.ButtonAdminPageAddNewCustomer.TabIndex = 3;
            this.ButtonAdminPageAddNewCustomer.Text = "Add New Customer";
            this.ButtonAdminPageAddNewCustomer.UseVisualStyleBackColor = false;
            this.ButtonAdminPageAddNewCustomer.Click += new System.EventHandler(this.ButtonAdminPageAddNewCustomer_Click);
            // 
            // PanelAdminPageSidebar
            // 
            this.PanelAdminPageSidebar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAdminPageSidebar.Controls.Add(this.PanelAdminPageSidebarSeperatorDesignedBy);
            this.PanelAdminPageSidebar.Controls.Add(this.LabelDesignedBy);
            this.PanelAdminPageSidebar.Controls.Add(this.PanelAdminPageSidebarSeperatorMusicCD);
            this.PanelAdminPageSidebar.Controls.Add(this.PanelAdminPageSidebarSeperatorBook);
            this.PanelAdminPageSidebar.Controls.Add(this.PanelAdminPageSidebarSeperatorCustomer);
            this.PanelAdminPageSidebar.Controls.Add(this.PanelAdminPageSidebarSeperatorMagazine);
            this.PanelAdminPageSidebar.Controls.Add(this.PanelAdminPageSidebarSeperatorTop);
            this.PanelAdminPageSidebar.Controls.Add(this.ButtonAdminPageAddNewCustomer);
            this.PanelAdminPageSidebar.Controls.Add(this.ButtonAdminPageAddNewMusicCD);
            this.PanelAdminPageSidebar.Controls.Add(this.ButtonAdminPageAddNewBook);
            this.PanelAdminPageSidebar.Controls.Add(this.ButtonAdminPageAddNewMagazine);
            this.PanelAdminPageSidebar.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelAdminPageSidebar.Location = new System.Drawing.Point(0, 50);
            this.PanelAdminPageSidebar.Name = "PanelAdminPageSidebar";
            this.PanelAdminPageSidebar.Size = new System.Drawing.Size(300, 650);
            this.PanelAdminPageSidebar.TabIndex = 4;
            // 
            // PanelAdminPageSidebarSeperatorDesignedBy
            // 
            this.PanelAdminPageSidebarSeperatorDesignedBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.PanelAdminPageSidebarSeperatorDesignedBy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAdminPageSidebarSeperatorDesignedBy.Location = new System.Drawing.Point(0, 616);
            this.PanelAdminPageSidebarSeperatorDesignedBy.Name = "PanelAdminPageSidebarSeperatorDesignedBy";
            this.PanelAdminPageSidebarSeperatorDesignedBy.Size = new System.Drawing.Size(300, 1);
            this.PanelAdminPageSidebarSeperatorDesignedBy.TabIndex = 6;
            // 
            // LabelDesignedBy
            // 
            this.LabelDesignedBy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelDesignedBy.AutoSize = true;
            this.LabelDesignedBy.Font = new System.Drawing.Font("Century Gothic", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelDesignedBy.ForeColor = System.Drawing.Color.White;
            this.LabelDesignedBy.Location = new System.Drawing.Point(7, 623);
            this.LabelDesignedBy.Name = "LabelDesignedBy";
            this.LabelDesignedBy.Size = new System.Drawing.Size(288, 17);
            this.LabelDesignedBy.TabIndex = 5;
            this.LabelDesignedBy.Text = "Designed By Kerim DAS - Hasan OZDEMIR";
            // 
            // PanelAdminPageSidebarSeperatorMusicCD
            // 
            this.PanelAdminPageSidebarSeperatorMusicCD.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PanelAdminPageSidebarSeperatorMusicCD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAdminPageSidebarSeperatorMusicCD.Location = new System.Drawing.Point(0, 372);
            this.PanelAdminPageSidebarSeperatorMusicCD.Name = "PanelAdminPageSidebarSeperatorMusicCD";
            this.PanelAdminPageSidebarSeperatorMusicCD.Size = new System.Drawing.Size(300, 1);
            this.PanelAdminPageSidebarSeperatorMusicCD.TabIndex = 7;
            // 
            // PanelAdminPageSidebarSeperatorBook
            // 
            this.PanelAdminPageSidebarSeperatorBook.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PanelAdminPageSidebarSeperatorBook.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAdminPageSidebarSeperatorBook.Location = new System.Drawing.Point(-1, 287);
            this.PanelAdminPageSidebarSeperatorBook.Name = "PanelAdminPageSidebarSeperatorBook";
            this.PanelAdminPageSidebarSeperatorBook.Size = new System.Drawing.Size(300, 1);
            this.PanelAdminPageSidebarSeperatorBook.TabIndex = 6;
            // 
            // PanelAdminPageSidebarSeperatorCustomer
            // 
            this.PanelAdminPageSidebarSeperatorCustomer.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PanelAdminPageSidebarSeperatorCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAdminPageSidebarSeperatorCustomer.Location = new System.Drawing.Point(0, 198);
            this.PanelAdminPageSidebarSeperatorCustomer.Name = "PanelAdminPageSidebarSeperatorCustomer";
            this.PanelAdminPageSidebarSeperatorCustomer.Size = new System.Drawing.Size(300, 1);
            this.PanelAdminPageSidebarSeperatorCustomer.TabIndex = 5;
            // 
            // PanelAdminPageSidebarSeperatorMagazine
            // 
            this.PanelAdminPageSidebarSeperatorMagazine.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PanelAdminPageSidebarSeperatorMagazine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAdminPageSidebarSeperatorMagazine.Location = new System.Drawing.Point(-2, 461);
            this.PanelAdminPageSidebarSeperatorMagazine.Name = "PanelAdminPageSidebarSeperatorMagazine";
            this.PanelAdminPageSidebarSeperatorMagazine.Size = new System.Drawing.Size(300, 1);
            this.PanelAdminPageSidebarSeperatorMagazine.TabIndex = 5;
            // 
            // PanelAdminPageSidebarSeperatorTop
            // 
            this.PanelAdminPageSidebarSeperatorTop.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.PanelAdminPageSidebarSeperatorTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAdminPageSidebarSeperatorTop.Location = new System.Drawing.Point(-1, 116);
            this.PanelAdminPageSidebarSeperatorTop.Name = "PanelAdminPageSidebarSeperatorTop";
            this.PanelAdminPageSidebarSeperatorTop.Size = new System.Drawing.Size(300, 1);
            this.PanelAdminPageSidebarSeperatorTop.TabIndex = 4;
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(1200, 700);
            this.Controls.Add(this.PanelAdminPageSidebar);
            this.Controls.Add(this.PanelAdminPageTop);
            this.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAdmin";
            this.Text = "AdminPage";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.PanelAdminPageTop.ResumeLayout(false);
            this.PanelAdminPageTop.PerformLayout();
            this.PanelAdminPageSidebar.ResumeLayout(false);
            this.PanelAdminPageSidebar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelAdminPageTop;
        private System.Windows.Forms.Label LabelAdminPageTop;
        private System.Windows.Forms.Button ButtonAdminPageExit;
        private System.Windows.Forms.Button ButtonAdminPageAddNewBook;
        private System.Windows.Forms.Button ButtonAdminPageAddNewMusicCD;
        private System.Windows.Forms.Button ButtonAdminPageAddNewMagazine;
        private System.Windows.Forms.Button ButtonAdminPageAddNewCustomer;
        private System.Windows.Forms.Panel PanelAdminPageSidebar;
        private System.Windows.Forms.Label LabelDesignedBy;
        private System.Windows.Forms.Panel PanelAdminPageSidebarSeperatorMusicCD;
        private System.Windows.Forms.Panel PanelAdminPageSidebarSeperatorBook;
        private System.Windows.Forms.Panel PanelAdminPageSidebarSeperatorCustomer;
        private System.Windows.Forms.Panel PanelAdminPageSidebarSeperatorMagazine;
        private System.Windows.Forms.Panel PanelAdminPageSidebarSeperatorTop;
        private System.Windows.Forms.Panel PanelAdminPageSidebarSeperatorDesignedBy;
    }
}