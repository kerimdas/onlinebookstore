﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace onlinestore {
    /// <summary>
    /// Observer tasarimi icin sinif.
    /// </summary>
    public class ClassObserverUser : IObserverUser {

        const string file = "log.txt";

        /// <summary>
        /// Konsola cikti yazdirir ve log fonksiyonunu cagirir.
        /// </summary>
        /// <param name="data">Konsola yazdirir.</param>
        /// <param name="username">Log fonksiyonuna yollar.</param>
        /// <param name="_object">Log fonksiyonuna yollar.</param>
        public void Update(string data, string username, object _object) {
            Console.WriteLine(data);
            Log(username, _object);
        }

        /// <summary>
        /// Parametreleri log dosyasina kaydeder.
        /// </summary>
        /// <param name="username">Kullanici ismini parametre olarak alir.</param>
        /// <param name="_object">Kullanilan objeyi parametre olarak alir.</param>
        public void Log(string username, object _object) {
            using (StreamWriter writer = new StreamWriter(file, true)) {
                if (File.Exists(file)) {
                    string line = String.Format("{0} \t-\t {1} \t:\t {2} \t-->\t {3}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), username, _object);
                    writer.WriteLine(line);
                    writer.Close();
                }
            }
        }
    }
}
