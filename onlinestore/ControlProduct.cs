﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Urun tasarimi icin UserControl. 
    /// </summary>
    public partial class ControlProduct : UserControl {

        private ClassProduct product;
        private int type;
        //private Image itemImage;
        //private double price;
        //private string name;
        private bool buttonViewDetailsClicked = false;
        private bool buttonAddCartClicked = false;

        public bool ButtonViewDetailsClicked
        {

            set { buttonViewDetailsClicked = value; }
            get { return buttonViewDetailsClicked; }

        }
        public bool ButtonAddCartClicked
        {

            set { buttonAddCartClicked = value; }
            get { return buttonAddCartClicked; }

        }
        public ClassProduct Product
        {
            get { return product; }
            set { product = value; }
        }

        public string NameItem
        {
            get { return product.Name; }
            set { product.Name = value; LabelProductName.Text = value; }
        }


        public Image ItemImage
        {
            get { return product.CoverPicture; }
            set { product.CoverPicture = value; PictureBoxProduct.Image = value; }
        }

        public double Price
        {
            get { return product.Price; }
            set { product.Price = value; LabelProductPrice.Text = value.ToString("0.00")+"₺"; }//0.00 taşmasın diye
        }


        public ControlProduct() {
            InitializeComponent();
        }

        /// <summary>
        /// Sepete ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddCart_Click(object sender, EventArgs e)
        {
            FormPurchase purchase = new FormPurchase(productControl: this);

            purchase.ShowDialog();
            buttonAddCartClicked = true;
        }

        /// <summary>
        /// Detaylari gosterir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonDetails_Click(object sender, EventArgs e)
        {
            ButtonViewDetailsClicked = true;
        }
    }
}
