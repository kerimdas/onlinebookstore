﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Kullanici eklemek icin form.
    /// </summary>
    public partial class FormAddCustomer : Form {

        ClassDatabase database = ClassDatabase.connect();
        ISubjectUser subjectUser;

        public FormAddCustomer() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormAddCustomer.", "admin", nameof(FormAddCustomer));
        }

        private void AddCustomerPage_Load(object sender, EventArgs e) {
            DataGridViewAddCustomerPageCustomer.DataSource = database.GetNotConfirmedUserData();
            DataGridViewAddCustomerPageCustomer.Columns[0].Visible = false;
            DataGridViewAddCustomerPageCustomer.AllowUserToAddRows = false;

        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAddCustomerPageExit_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is exited from the FormAddCustomer.", "admin", nameof(ButtonAddCustomerPageExit_Click));
            this.Close();
        }

        /// <summary>
        /// Kullanici ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonConfirmAddCustomer_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is added a new customer to the application.", "admin", nameof(ButtonConfirmAddCustomer_Click));
            try
            {
                int i = 0;
                foreach (DataGridViewRow item in DataGridViewAddCustomerPageCustomer.SelectedRows)
                {
                    string Username = DataGridViewAddCustomerPageCustomer.SelectedRows[i].Cells[1].Value.ToString();
                    string Password = DataGridViewAddCustomerPageCustomer.SelectedRows[i].Cells[2].Value.ToString();
                    string Name = DataGridViewAddCustomerPageCustomer.SelectedRows[i].Cells[3].Value.ToString();
                    string Email = DataGridViewAddCustomerPageCustomer.SelectedRows[i].Cells[4].Value.ToString();
                    string Address = DataGridViewAddCustomerPageCustomer.SelectedRows[i].Cells[5].Value.ToString();

                    int ID = database.ReturnID("[NotConfirmedUserTable]",item.Index+1);
                    database.SetUserData(Username, Password, Name, Email, Address);
                    
                    DataGridViewAddCustomerPageCustomer.Rows.Remove(item);

                    DataGridViewAddCustomerPageCustomer.Update();
                    i++;

                    database.RemoveNotConfirmedUserData(ID);
                }
            }
            catch (Exception)
            {

            }           
        }
    }
}
