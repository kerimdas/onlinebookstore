﻿namespace onlinestore {
    partial class FormAddCustomer {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddCustomer));
            this.PanelAddCustomerPageTop = new System.Windows.Forms.Panel();
            this.ButtonAddCustomerPageExit = new System.Windows.Forms.Button();
            this.LabelAddCustomerPageTop = new System.Windows.Forms.Label();
            this.PanelAddCustomerPageMiddle = new System.Windows.Forms.Panel();
            this.ButtonConfirmAddCustomer = new System.Windows.Forms.Button();
            this.LabelAddCustomerPageConfirm = new System.Windows.Forms.Label();
            this.LabelAddCustomerPageCustomer = new System.Windows.Forms.Label();
            this.DataGridViewAddCustomerPageCustomer = new System.Windows.Forms.DataGridView();
            this.PanelAddCustomerPageSidebarBottom = new System.Windows.Forms.Panel();
            this.PanelAddCustomerPageSidebarRight = new System.Windows.Forms.Panel();
            this.PanelAddCustomerPageSidebarLeft = new System.Windows.Forms.Panel();
            this.PanelAddCustomerPageTop.SuspendLayout();
            this.PanelAddCustomerPageMiddle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewAddCustomerPageCustomer)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelAddCustomerPageTop
            // 
            this.PanelAddCustomerPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddCustomerPageTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAddCustomerPageTop.Controls.Add(this.ButtonAddCustomerPageExit);
            this.PanelAddCustomerPageTop.Controls.Add(this.LabelAddCustomerPageTop);
            this.PanelAddCustomerPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelAddCustomerPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelAddCustomerPageTop.Name = "PanelAddCustomerPageTop";
            this.PanelAddCustomerPageTop.Size = new System.Drawing.Size(800, 50);
            this.PanelAddCustomerPageTop.TabIndex = 1;
            // 
            // ButtonAddCustomerPageExit
            // 
            this.ButtonAddCustomerPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonAddCustomerPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddCustomerPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonAddCustomerPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddCustomerPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddCustomerPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddCustomerPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonAddCustomerPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAddCustomerPageExit.Image")));
            this.ButtonAddCustomerPageExit.Location = new System.Drawing.Point(739, 3);
            this.ButtonAddCustomerPageExit.Name = "ButtonAddCustomerPageExit";
            this.ButtonAddCustomerPageExit.Size = new System.Drawing.Size(53, 46);
            this.ButtonAddCustomerPageExit.TabIndex = 4;
            this.ButtonAddCustomerPageExit.UseVisualStyleBackColor = true;
            this.ButtonAddCustomerPageExit.Click += new System.EventHandler(this.ButtonAddCustomerPageExit_Click);
            // 
            // LabelAddCustomerPageTop
            // 
            this.LabelAddCustomerPageTop.AutoSize = true;
            this.LabelAddCustomerPageTop.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddCustomerPageTop.ForeColor = System.Drawing.Color.White;
            this.LabelAddCustomerPageTop.Location = new System.Drawing.Point(23, 10);
            this.LabelAddCustomerPageTop.Name = "LabelAddCustomerPageTop";
            this.LabelAddCustomerPageTop.Size = new System.Drawing.Size(181, 28);
            this.LabelAddCustomerPageTop.TabIndex = 0;
            this.LabelAddCustomerPageTop.Text = "Add Customer";
            // 
            // PanelAddCustomerPageMiddle
            // 
            this.PanelAddCustomerPageMiddle.BackColor = System.Drawing.Color.Silver;
            this.PanelAddCustomerPageMiddle.Controls.Add(this.ButtonConfirmAddCustomer);
            this.PanelAddCustomerPageMiddle.Controls.Add(this.LabelAddCustomerPageConfirm);
            this.PanelAddCustomerPageMiddle.Controls.Add(this.LabelAddCustomerPageCustomer);
            this.PanelAddCustomerPageMiddle.Controls.Add(this.DataGridViewAddCustomerPageCustomer);
            this.PanelAddCustomerPageMiddle.Controls.Add(this.PanelAddCustomerPageSidebarBottom);
            this.PanelAddCustomerPageMiddle.Controls.Add(this.PanelAddCustomerPageSidebarRight);
            this.PanelAddCustomerPageMiddle.Controls.Add(this.PanelAddCustomerPageSidebarLeft);
            this.PanelAddCustomerPageMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelAddCustomerPageMiddle.Location = new System.Drawing.Point(0, 50);
            this.PanelAddCustomerPageMiddle.Name = "PanelAddCustomerPageMiddle";
            this.PanelAddCustomerPageMiddle.Size = new System.Drawing.Size(800, 500);
            this.PanelAddCustomerPageMiddle.TabIndex = 2;
            // 
            // ButtonConfirmAddCustomer
            // 
            this.ButtonConfirmAddCustomer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonConfirmAddCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonConfirmAddCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonConfirmAddCustomer.FlatAppearance.BorderSize = 0;
            this.ButtonConfirmAddCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonConfirmAddCustomer.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonConfirmAddCustomer.ForeColor = System.Drawing.Color.White;
            this.ButtonConfirmAddCustomer.Location = new System.Drawing.Point(477, 418);
            this.ButtonConfirmAddCustomer.Name = "ButtonConfirmAddCustomer";
            this.ButtonConfirmAddCustomer.Size = new System.Drawing.Size(275, 50);
            this.ButtonConfirmAddCustomer.TabIndex = 6;
            this.ButtonConfirmAddCustomer.Text = "Confirm Customer";
            this.ButtonConfirmAddCustomer.UseVisualStyleBackColor = false;
            this.ButtonConfirmAddCustomer.Click += new System.EventHandler(this.ButtonConfirmAddCustomer_Click);
            // 
            // LabelAddCustomerPageConfirm
            // 
            this.LabelAddCustomerPageConfirm.AutoSize = true;
            this.LabelAddCustomerPageConfirm.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddCustomerPageConfirm.ForeColor = System.Drawing.Color.White;
            this.LabelAddCustomerPageConfirm.Location = new System.Drawing.Point(54, 451);
            this.LabelAddCustomerPageConfirm.Name = "LabelAddCustomerPageConfirm";
            this.LabelAddCustomerPageConfirm.Size = new System.Drawing.Size(199, 17);
            this.LabelAddCustomerPageConfirm.TabIndex = 5;
            this.LabelAddCustomerPageConfirm.Text = "**Click customer to confirm.";
            // 
            // LabelAddCustomerPageCustomer
            // 
            this.LabelAddCustomerPageCustomer.AutoSize = true;
            this.LabelAddCustomerPageCustomer.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddCustomerPageCustomer.ForeColor = System.Drawing.Color.White;
            this.LabelAddCustomerPageCustomer.Location = new System.Drawing.Point(54, 22);
            this.LabelAddCustomerPageCustomer.Name = "LabelAddCustomerPageCustomer";
            this.LabelAddCustomerPageCustomer.Size = new System.Drawing.Size(113, 23);
            this.LabelAddCustomerPageCustomer.TabIndex = 4;
            this.LabelAddCustomerPageCustomer.Text = "Customers";
            // 
            // DataGridViewAddCustomerPageCustomer
            // 
            this.DataGridViewAddCustomerPageCustomer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridViewAddCustomerPageCustomer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.DataGridViewAddCustomerPageCustomer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewAddCustomerPageCustomer.Location = new System.Drawing.Point(52, 48);
            this.DataGridViewAddCustomerPageCustomer.Name = "DataGridViewAddCustomerPageCustomer";
            this.DataGridViewAddCustomerPageCustomer.ReadOnly = true;
            this.DataGridViewAddCustomerPageCustomer.RowTemplate.Height = 24;
            this.DataGridViewAddCustomerPageCustomer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridViewAddCustomerPageCustomer.Size = new System.Drawing.Size(700, 361);
            this.DataGridViewAddCustomerPageCustomer.TabIndex = 3;
            // 
            // PanelAddCustomerPageSidebarBottom
            // 
            this.PanelAddCustomerPageSidebarBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddCustomerPageSidebarBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelAddCustomerPageSidebarBottom.Location = new System.Drawing.Point(1, 499);
            this.PanelAddCustomerPageSidebarBottom.Name = "PanelAddCustomerPageSidebarBottom";
            this.PanelAddCustomerPageSidebarBottom.Size = new System.Drawing.Size(798, 1);
            this.PanelAddCustomerPageSidebarBottom.TabIndex = 2;
            // 
            // PanelAddCustomerPageSidebarRight
            // 
            this.PanelAddCustomerPageSidebarRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddCustomerPageSidebarRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelAddCustomerPageSidebarRight.Location = new System.Drawing.Point(799, 0);
            this.PanelAddCustomerPageSidebarRight.Name = "PanelAddCustomerPageSidebarRight";
            this.PanelAddCustomerPageSidebarRight.Size = new System.Drawing.Size(1, 500);
            this.PanelAddCustomerPageSidebarRight.TabIndex = 1;
            // 
            // PanelAddCustomerPageSidebarLeft
            // 
            this.PanelAddCustomerPageSidebarLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddCustomerPageSidebarLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelAddCustomerPageSidebarLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelAddCustomerPageSidebarLeft.Name = "PanelAddCustomerPageSidebarLeft";
            this.PanelAddCustomerPageSidebarLeft.Size = new System.Drawing.Size(1, 500);
            this.PanelAddCustomerPageSidebarLeft.TabIndex = 0;
            // 
            // FormAddCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 550);
            this.Controls.Add(this.PanelAddCustomerPageMiddle);
            this.Controls.Add(this.PanelAddCustomerPageTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Name = "FormAddCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddCustomerPage";
            this.Load += new System.EventHandler(this.AddCustomerPage_Load);
            this.PanelAddCustomerPageTop.ResumeLayout(false);
            this.PanelAddCustomerPageTop.PerformLayout();
            this.PanelAddCustomerPageMiddle.ResumeLayout(false);
            this.PanelAddCustomerPageMiddle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewAddCustomerPageCustomer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelAddCustomerPageTop;
        private System.Windows.Forms.Button ButtonAddCustomerPageExit;
        private System.Windows.Forms.Label LabelAddCustomerPageTop;
        private System.Windows.Forms.Panel PanelAddCustomerPageMiddle;
        private System.Windows.Forms.Panel PanelAddCustomerPageSidebarBottom;
        private System.Windows.Forms.Panel PanelAddCustomerPageSidebarRight;
        private System.Windows.Forms.Panel PanelAddCustomerPageSidebarLeft;
        private System.Windows.Forms.Label LabelAddCustomerPageConfirm;
        private System.Windows.Forms.Label LabelAddCustomerPageCustomer;
        private System.Windows.Forms.DataGridView DataGridViewAddCustomerPageCustomer;
        private System.Windows.Forms.Button ButtonConfirmAddCustomer;
    }
}