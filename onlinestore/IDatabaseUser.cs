﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace onlinestore {
    /// <summary>
    /// Kullanici veritabani sinifi icin arayuz.
    /// </summary>
    public interface IDatabaseUser {
        bool CheckUserExist(string username);
        void SetNotConfirmedUserData(string username, string password, string name, string email, string address);
        bool GetUserDataToLogin(string username, string password, ref int id);
        bool CheckUserEmailExist(string email);
        void RemoveNotConfirmedUserData(int ID);
        void SetUserData(string username, string password, string name, string email, string address);
        void UpdateUserData(int id, string password, string email, string address);
        void UpdateUserPassword(int id, string password);
        DataTable GetNotConfirmedUserData();
        void GetUserData(ref ClassCustomer customer, int id);
        int GetUserID(string email);
    }
}
