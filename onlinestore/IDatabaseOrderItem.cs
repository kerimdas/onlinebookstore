﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace onlinestore {
    /// <summary>
    /// Kullanicinin alisveris gecmisi veritabani icin olusturulan sinifin arayuzu.
    /// </summary>
    public interface IDatabaseOrderItem {
        DataTable GetOrderData(int customerid);
        void SetOrderData(int customerid, string type, string name, float unitprice, int quantity, float totalprice, int itemid);
    }
}
