﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace onlinestore
{
    /// <summary>
    /// Alisveris sepeti icin sinif.
    /// </summary>
    public class ClassShoppingCart
    {
        private int customerId;
        private ArrayList itemsToPurchase = new ArrayList();
        private double paymentAmount = 0;
        private string paymentType;//cash, credit card//enum olabilir?

        public int CustomerID { set { customerId = value; } get { return customerId; } }
        public double PaymentAmount { set { paymentAmount = value; } get { return paymentAmount; } }
        public string PaymentType { set { paymentType = value; } get { return paymentType; } }

        public ArrayList ItemsToPurchase { set { itemsToPurchase = value; } get { return itemsToPurchase; } }

        public void printProducts()
        {
            foreach (var item in itemsToPurchase)
            {
                Console.WriteLine(((ClassItemToPurchase)item).Product.Name + " " + ((ClassItemToPurchase)item).Quantity);
            }
        }

        /// <summary>
        /// Urun ekler.
        /// </summary>
        /// <param name="item">Eklenen urunu parametre olarak alir.</param>
        void addProduct(ClassItemToPurchase item)
        {
            itemsToPurchase.Add(item);
        }

        /// <summary>
        /// Urun siler.
        /// </summary>
        /// <param name="item">Silinecek urunu parametre olarak alir.</param>
        void removeProduct(ClassItemToPurchase item)
        {
            itemsToPurchase.Remove(item);
        }

        public void placeOrder()
        { }

        public void cancelOrder()
        { }
        public void sendInvoicebyEmail()
        { }
    }
}
