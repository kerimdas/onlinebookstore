﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace onlinestore {
    /// <summary>
    /// Kitap veritabani icin bir sinif.
    /// </summary>
    public class ClassDatabaseBook : IDatabaseBook {
        public static SqlConnection connection = ClassDatabase.connection;

        /// <summary>
        /// Veritabanina gonderilen indeksteki kitabi bulup parametreye esitler.
        /// </summary>
        /// <param name="book">Kitabi esitlemek icin parametre.</param>
        /// <param name="index">Istenilen kitabi bulmak icin parametre.</param>
        public void GetBookData(ClassBook book, int index) {
            connection.Open();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [BookTable] WHERE ID = " + index + "", connection)) {
                command.Parameters.AddWithValue("ID", index);

                using (SqlDataReader dr = command.ExecuteReader()) {
                    dr.Read();
                    book.ID = Convert.ToInt32(dr["ID"]);
                    book.ISBN = dr["ISBN"].ToString();
                    book.Name = dr["NAME"].ToString();
                    book.Author = dr["AUTHOR"].ToString();
                    book.Publisher = dr["PUBLISHER"].ToString();
                    book.Page = Convert.ToInt32(dr["PAGE"]);
                    book.Price = Convert.ToDouble(dr["PRICE"]);
                }
            }
            connection.Close();
        }

        /// <summary>
        /// Kitabi veritabanina ekler.
        /// </summary>
        /// <param name="isbn">ISBN numarasini parametre olarak alir.</param>
        /// <param name="name">Ismini parametre olarak alir.</param>
        /// <param name="author">Yazarini parametre olarak alir.</param>
        /// <param name="publisher">Yayincisini parametre olarak alir.</param>
        /// <param name="page">Sayfa sayisini parametre olarak alir.</param>
        /// <param name="price">Fiyatini parametre olarak alir.</param>
        public void SetBookData(string isbn, string name, string author, string publisher, int page, float price) {
            connection.Open();
            using (SqlCommand command = new SqlCommand("INSERT INTO [BookTable] (ISBN, NAME, AUTHOR, PUBLISHER, PAGE, PRICE) values(@isbn,@name,@author,@publisher,@page,@price)", connection)) {
                command.Parameters.AddWithValue("isbn", isbn);
                command.Parameters.AddWithValue("name", name);
                command.Parameters.AddWithValue("author", author);
                command.Parameters.AddWithValue("publisher", publisher);
                command.Parameters.AddWithValue("page", page);
                command.Parameters.AddWithValue("price", price);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }
    }
}
