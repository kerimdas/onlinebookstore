﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore
{
    /// <summary>
    /// Satin alinacak urun icin sinif.
    /// </summary>
    class ClassItemToPurchase
    {
        private ClassProduct product;
        private int quantity;

        public ClassProduct Product { set { product = value; } get { return product; } }
        public int Quantity { set { quantity = value; } get { return quantity; } }
    }
}
