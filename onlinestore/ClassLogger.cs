﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace onlinestore {
    class ClassLogger {
        public void UserLogToTextFile(string username, object _object, string name) {
            using (StreamWriter writer = new StreamWriter(name, true)) {
                if (File.Exists(name)) {
                    string line = String.Format("{0} - {1} : {2} --> {3}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString(), username, _object);
                    writer.WriteLine(line);
                    writer.Close();
                }
            }
        }
    }
}
