﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Urunu sepete eklemek icin form.
    /// </summary>
    public partial class FormPurchase : Form {

        ClassItemToPurchase itemToPurchase = new ClassItemToPurchase();
        ClassShoppingCart shoppingCart;
        ClassCustomer customer = ClassCustomer.InstanceCustomer;
        ISubjectUser subjectUser;

        int result;

        //public FormPurchase()
        //{
        //    InitializeComponent();
        //    subjectUser = new ClassSubjectUser();
        //    subjectUser.Attach(new ClassObserverUser());
        //    subjectUser.Notify("Observer - User is entered to the FormPurchase.", customer.Username, nameof(FormPurchase));
        //}

        //public FormPurchase(ClassProduct Product, ClassCustomer _customer)
        //{
        //    InitializeComponent();
        //    customer = _customer;
        //    subjectUser = new ClassSubjectUser();
        //    subjectUser.Attach(new ClassObserverUser());
        //    subjectUser.Notify("Observer - User is entered to the FormPurchase.", customer.Username, nameof(FormPurchase));
        //}

        public FormPurchase(ControlProduct productControl)
        {
            InitializeComponent();

            itemToPurchase.Product = productControl.Product;
            FillPurchasePage();

            shoppingCart = FormBook.shoppingCart;            
        }

        /// <summary>
        /// Urun adedini arttirir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonIncrease_Click(object sender, EventArgs e)
        {
            //subjectUser.Notify("Observer - User is increased the quantity 1.", "Current User", nameof(ButtonIncrease_Click));
            int result;
            try
            {
                if (Int32.TryParse(TextBoxQuantity.Text, out result))
                {
                    result = Convert.ToInt32(TextBoxQuantity.Text);
                    result++;
                    TextBoxQuantity.Text = result.ToString();
                    LabelProductPrice.Text = (itemToPurchase.Product.Price * Convert.ToInt32(TextBoxQuantity.Text)).ToString("0.00") + "₺";
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Urun adedini azaltir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonDecrease_Click(object sender, EventArgs e)
        {
            //subjectUser.Notify("Observer - User is decreased the quantity 1.", customer.Username, nameof(ButtonDecrease_Click));
            try
            {
                if (Int32.TryParse(TextBoxQuantity.Text, out result))
                {
                    result = Convert.ToInt32(TextBoxQuantity.Text);
                    if (result - 1 > 0)
                    {
                        result--;
                    }
                    TextBoxQuantity.Text = result.ToString();
                    LabelProductPrice.Text = (itemToPurchase.Product.Price * Convert.ToInt32(TextBoxQuantity.Text)).ToString("0.00") + "₺";
                }
            }
            catch (Exception)
            {

            }
        }

        /// <summary>
        /// Formu, urunun bilgisiyle doldurur.
        /// </summary>
        private void FillPurchasePage()
        {
            PictureBoxPurchase.Image = itemToPurchase.Product.CoverPicture;
            LabelProductName.Text = itemToPurchase.Product.Name;
            LabelProductPrice.Text = itemToPurchase.Product.Price.ToString("0.00") + "₺";
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonExit_Click(object sender, EventArgs e)
        {
            //subjectUser.Notify("Observer - User is exited from the FormPurchase.", customer.Username, nameof(ButtonExit_Click));
            this.Close();
        }

        /// <summary>
        /// Urunu sepete ekler.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
            //subjectUser.Notify("Observer - User is confirmed the item.", customer.Username, nameof(ButtonConfirm_Click));
            result = Convert.ToInt32(TextBoxQuantity.Text);
            itemToPurchase.Quantity = result;
            shoppingCart.PaymentAmount += itemToPurchase.Quantity * itemToPurchase.Product.Price;
            shoppingCart.ItemsToPurchase.Add(itemToPurchase);            
            this.Close();
        }

        /// <summary>
        /// Sayi disinda karakter girilmemesini saglar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TextBoxQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Urun birim fiyatini adetiyle carpar, toplam fiyati gosterir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void TextBoxQuantity_TextChanged(object sender, EventArgs e)
        {
            try
            {
                LabelProductPrice.Text = (itemToPurchase.Product.Price * Convert.ToInt32(TextBoxQuantity.Text)).ToString() + "₺";
            }
            catch (Exception)
            {
                
            }
        }
    }
}
