﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Admin icin form.
    /// </summary>
    public partial class FormAdmin : Form {
        ISubjectUser subjectUser;

        public FormAdmin() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormAdmin.", "admin", nameof(FormAdmin));
        }

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAdminPageExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exited to the FormAdmin.", "admin", nameof(ButtonAdminPageExit_Click));
            this.Close();
        }

        /// <summary>
        /// Yeni kitap eklemek icin kitap ekle formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAdminPageAddNewBook_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormAddBook.", "admin", nameof(ButtonAdminPageAddNewBook_Click));
            FormAddBook addBookPage = new FormAddBook();
            addBookPage.ShowDialog();
        }

        /// <summary>
        /// Yeni kullanici eklemek icin kullanici ekle formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAdminPageAddNewCustomer_Click(object sender, EventArgs e) {
            subjectUser.Notify("Observer - User is entering to the FormAddCustomer.", "admin", nameof(ButtonAdminPageAddNewCustomer_Click));
            FormAddCustomer addCustomerPage = new FormAddCustomer();
            addCustomerPage.ShowDialog();
        }

        /// <summary>
        /// Yeni magazin eklemek icin magazin ekle formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAdminPageAddNewMagazine_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormAddMagazine.", "admin", nameof(ButtonAdminPageAddNewMagazine_Click));
            FormAddMagazine addMagazinePage = new FormAddMagazine();
            addMagazinePage.ShowDialog();
        }

        /// <summary>
        /// Yeni muzik eklemek icin muzik ekle formunu acar.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonAdminPageAddNewMusicCD_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is entering to the FormAddMusic.", "admin", nameof(ButtonAdminPageAddNewMusicCD_Click));
            FormAddMusic addMusicCDPage = new FormAddMusic();
            addMusicCDPage.ShowDialog();
        }
    }
}
