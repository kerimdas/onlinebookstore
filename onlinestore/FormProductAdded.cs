﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace onlinestore {
    /// <summary>
    /// Urun satin alindiginda cikan onay ekrani.
    /// </summary>
    public partial class FormProductAdded : Form {
        ClassCustomer customer = ClassCustomer.InstanceCustomer;
        ISubjectUser subjectUser;

        public FormProductAdded() {
            InitializeComponent();
            subjectUser = new ClassSubjectUser();
            subjectUser.Attach(new ClassObserverUser());
            subjectUser.Notify("Observer - User is entered to the FormProductAdded.", customer.Username, nameof(FormProductAdded));
        }

        //public FormProductAdded(ClassCustomer _customer) {
        //    InitializeComponent();
        //    customer = _customer;
        //    subjectUser = new ClassSubjectUser();
        //    subjectUser.Attach(new ClassObserverUser());
        //    subjectUser.Notify("Observer - User is entered to the FormProductAdded.", customer.Username, nameof(FormProductAdded));
        //}

        /// <summary>
        /// Formu kapatir.
        /// </summary>
        /// <param name="sender">Parametre.</param>
        /// <param name="e">Parametre.</param>
        private void ButtonExit_Click(object sender, EventArgs e)
        {
            subjectUser.Notify("Observer - User is exited from the FormProductAdded.", customer.Username, nameof(ButtonExit_Click));
            this.Close();
        }
    }
}
