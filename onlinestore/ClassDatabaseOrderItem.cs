﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace onlinestore {
    /// <summary>
    /// Siparis ozetinin veritabani icin sinif.
    /// </summary>
    public class ClassDatabaseOrderItem : IDatabaseOrderItem {
        public static SqlConnection connection = ClassDatabase.connection;

        /// <summary>
        /// Istenilen kullanicinin ID'si ile veritabaninda sorgu yapilarak siparis gecmisi DataTable olarak dondurulur.
        /// </summary>
        /// <param name="customerid">Kullanici bulmak icin ID parametre olarak alinir.</param>
        /// <returns>DataTable tipinde veritabani tablosu dondurur.</returns>
        public DataTable GetOrderData(int customerid)
        {
            connection.Open();
            DataTable dataTable = new DataTable();
            using (SqlCommand command = new SqlCommand(@"SELECT * FROM [OrderTable] WHERE CUSTOMERID=@customerid", connection))
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command))
            {
                command.Parameters.AddWithValue("customerid", customerid);
                sqlDataAdapter.Fill(dataTable);
            }
            connection.Close();
            return dataTable;
        }
        
        /// <summary>
        /// Kullanici tarafindan alinan urunleri veritabanina ekler.
        /// </summary>
        /// <param name="customerid">Kullanici ID'sini parametre olarak alir.</param>
        /// <param name="type">Urunun tipini parametre olarak alir.</param>
        /// <param name="name">Urunun ismini parametre olarak alir.</param>
        /// <param name="unitprice">Urunun birim fiyatini parametre olarak alir.</param>
        /// <param name="quantity">Urunden kac tane alindigini parametre olarak alir.</param>
        /// <param name="totalprice">Urunun toplam tutarini parametre olarak alir.</param>
        /// <param name="itemid">Urunun ID'sini parametre olarak alir.</param>
        public void SetOrderData(int customerid, string type, string name, float unitprice, int quantity, float totalprice, int itemid)
        {
            connection.Open();
            using (SqlCommand command = new SqlCommand("INSERT INTO [OrderTable] (CUSTOMERID, TYPE, NAME , UNITPRICE , QUANTITY ,TOTALPRICE, ITEMID) values(@customerid, @type, @name, @unitprice, @quantity, @totalprice, @itemid)", connection))
            {
                command.Parameters.AddWithValue("customerid", customerid);

                command.Parameters.AddWithValue("type", type);
                command.Parameters.AddWithValue("name", name);

                command.Parameters.AddWithValue("unitprice", unitprice);
                command.Parameters.AddWithValue("quantity", quantity);
                command.Parameters.AddWithValue("totalprice", totalprice);
                command.Parameters.AddWithValue("itemid", itemid);

                command.ExecuteNonQuery();
            }
            connection.Close();
        }
    }
}
