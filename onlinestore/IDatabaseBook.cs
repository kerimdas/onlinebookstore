﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace onlinestore {
    /// <summary>
    /// Kitap veritabani sinifi icin arayuz.
    /// </summary>
    public interface IDatabaseBook {
        void GetBookData(ClassBook book, int index);
        void SetBookData(string isbn, string name, string author, string publisher, int page, float price);
    }
}
