﻿namespace onlinestore {
    partial class FormAddBook {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAddBook));
            this.PanelAddBookPageMiddle = new System.Windows.Forms.Panel();
            this.ButtonAddBookPageAddPicture = new System.Windows.Forms.Button();
            this.PanelAddBookPagePictureBox = new System.Windows.Forms.Panel();
            this.PictureBoxAddBookPage = new System.Windows.Forms.PictureBox();
            this.ButtonAddBookPageAdd = new System.Windows.Forms.Button();
            this.TextBoxAddBookPagePrice = new System.Windows.Forms.TextBox();
            this.LabelAddBookPagePrice = new System.Windows.Forms.Label();
            this.TextBoxAddBookPageISBN = new System.Windows.Forms.TextBox();
            this.LabelAddBookPageISBN = new System.Windows.Forms.Label();
            this.TextBoxAddBookPagePage = new System.Windows.Forms.TextBox();
            this.LabelAddBookPagePage = new System.Windows.Forms.Label();
            this.TextBoxAddBookPagePublisher = new System.Windows.Forms.TextBox();
            this.LabelAddBookPagePublisher = new System.Windows.Forms.Label();
            this.TextBoxAddBookPageAuthor = new System.Windows.Forms.TextBox();
            this.LabelAddBookPageAuthor = new System.Windows.Forms.Label();
            this.TextBookAddBookPageName = new System.Windows.Forms.TextBox();
            this.LabelAddBookPageName = new System.Windows.Forms.Label();
            this.PanelAddBookPageSidebarBottom = new System.Windows.Forms.Panel();
            this.PanelAddBookPageSidebarRight = new System.Windows.Forms.Panel();
            this.PanelAddBookPageSidebarLeft = new System.Windows.Forms.Panel();
            this.PanelAddBookPageTop = new System.Windows.Forms.Panel();
            this.ButtonAddBookPageExit = new System.Windows.Forms.Button();
            this.LabelAddBookPageTop = new System.Windows.Forms.Label();
            this.PanelAddBookPageMiddle.SuspendLayout();
            this.PanelAddBookPagePictureBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAddBookPage)).BeginInit();
            this.PanelAddBookPageTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // PanelAddBookPageMiddle
            // 
            this.PanelAddBookPageMiddle.BackColor = System.Drawing.Color.Silver;
            this.PanelAddBookPageMiddle.Controls.Add(this.ButtonAddBookPageAddPicture);
            this.PanelAddBookPageMiddle.Controls.Add(this.PanelAddBookPagePictureBox);
            this.PanelAddBookPageMiddle.Controls.Add(this.ButtonAddBookPageAdd);
            this.PanelAddBookPageMiddle.Controls.Add(this.TextBoxAddBookPagePrice);
            this.PanelAddBookPageMiddle.Controls.Add(this.LabelAddBookPagePrice);
            this.PanelAddBookPageMiddle.Controls.Add(this.TextBoxAddBookPageISBN);
            this.PanelAddBookPageMiddle.Controls.Add(this.LabelAddBookPageISBN);
            this.PanelAddBookPageMiddle.Controls.Add(this.TextBoxAddBookPagePage);
            this.PanelAddBookPageMiddle.Controls.Add(this.LabelAddBookPagePage);
            this.PanelAddBookPageMiddle.Controls.Add(this.TextBoxAddBookPagePublisher);
            this.PanelAddBookPageMiddle.Controls.Add(this.LabelAddBookPagePublisher);
            this.PanelAddBookPageMiddle.Controls.Add(this.TextBoxAddBookPageAuthor);
            this.PanelAddBookPageMiddle.Controls.Add(this.LabelAddBookPageAuthor);
            this.PanelAddBookPageMiddle.Controls.Add(this.TextBookAddBookPageName);
            this.PanelAddBookPageMiddle.Controls.Add(this.LabelAddBookPageName);
            this.PanelAddBookPageMiddle.Controls.Add(this.PanelAddBookPageSidebarBottom);
            this.PanelAddBookPageMiddle.Controls.Add(this.PanelAddBookPageSidebarRight);
            this.PanelAddBookPageMiddle.Controls.Add(this.PanelAddBookPageSidebarLeft);
            this.PanelAddBookPageMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelAddBookPageMiddle.Font = new System.Drawing.Font("Century Gothic", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.PanelAddBookPageMiddle.Location = new System.Drawing.Point(0, 50);
            this.PanelAddBookPageMiddle.Name = "PanelAddBookPageMiddle";
            this.PanelAddBookPageMiddle.Size = new System.Drawing.Size(800, 500);
            this.PanelAddBookPageMiddle.TabIndex = 4;
            // 
            // ButtonAddBookPageAddPicture
            // 
            this.ButtonAddBookPageAddPicture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonAddBookPageAddPicture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddBookPageAddPicture.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddBookPageAddPicture.FlatAppearance.BorderSize = 0;
            this.ButtonAddBookPageAddPicture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddBookPageAddPicture.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAddBookPageAddPicture.ForeColor = System.Drawing.Color.White;
            this.ButtonAddBookPageAddPicture.Location = new System.Drawing.Point(454, 280);
            this.ButtonAddBookPageAddPicture.Name = "ButtonAddBookPageAddPicture";
            this.ButtonAddBookPageAddPicture.Size = new System.Drawing.Size(250, 50);
            this.ButtonAddBookPageAddPicture.TabIndex = 17;
            this.ButtonAddBookPageAddPicture.Text = "Add Picture";
            this.ButtonAddBookPageAddPicture.UseVisualStyleBackColor = false;
            this.ButtonAddBookPageAddPicture.Click += new System.EventHandler(this.ButtonAddBookPageAddPicture_Click);
            // 
            // PanelAddBookPagePictureBox
            // 
            this.PanelAddBookPagePictureBox.Controls.Add(this.PictureBoxAddBookPage);
            this.PanelAddBookPagePictureBox.Location = new System.Drawing.Point(476, 49);
            this.PanelAddBookPagePictureBox.Name = "PanelAddBookPagePictureBox";
            this.PanelAddBookPagePictureBox.Size = new System.Drawing.Size(200, 200);
            this.PanelAddBookPagePictureBox.TabIndex = 16;
            // 
            // PictureBoxAddBookPage
            // 
            this.PictureBoxAddBookPage.Location = new System.Drawing.Point(10, 10);
            this.PictureBoxAddBookPage.Name = "PictureBoxAddBookPage";
            this.PictureBoxAddBookPage.Size = new System.Drawing.Size(180, 180);
            this.PictureBoxAddBookPage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxAddBookPage.TabIndex = 0;
            this.PictureBoxAddBookPage.TabStop = false;
            // 
            // ButtonAddBookPageAdd
            // 
            this.ButtonAddBookPageAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonAddBookPageAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddBookPageAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddBookPageAdd.FlatAppearance.BorderSize = 0;
            this.ButtonAddBookPageAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddBookPageAdd.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ButtonAddBookPageAdd.ForeColor = System.Drawing.Color.White;
            this.ButtonAddBookPageAdd.Location = new System.Drawing.Point(454, 412);
            this.ButtonAddBookPageAdd.Name = "ButtonAddBookPageAdd";
            this.ButtonAddBookPageAdd.Size = new System.Drawing.Size(250, 50);
            this.ButtonAddBookPageAdd.TabIndex = 15;
            this.ButtonAddBookPageAdd.Text = "ADD";
            this.ButtonAddBookPageAdd.UseVisualStyleBackColor = false;
            this.ButtonAddBookPageAdd.Click += new System.EventHandler(this.ButtonAddBookPageAdd_Click);
            // 
            // TextBoxAddBookPagePrice
            // 
            this.TextBoxAddBookPagePrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddBookPagePrice.Location = new System.Drawing.Point(104, 430);
            this.TextBoxAddBookPagePrice.Name = "TextBoxAddBookPagePrice";
            this.TextBoxAddBookPagePrice.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddBookPagePrice.TabIndex = 14;
            this.TextBoxAddBookPagePrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxAddBookPagePrice_KeyPress);
            // 
            // LabelAddBookPagePrice
            // 
            this.LabelAddBookPagePrice.AutoSize = true;
            this.LabelAddBookPagePrice.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddBookPagePrice.ForeColor = System.Drawing.Color.White;
            this.LabelAddBookPagePrice.Location = new System.Drawing.Point(100, 403);
            this.LabelAddBookPagePrice.Name = "LabelAddBookPagePrice";
            this.LabelAddBookPagePrice.Size = new System.Drawing.Size(57, 23);
            this.LabelAddBookPagePrice.TabIndex = 13;
            this.LabelAddBookPagePrice.Text = "Price";
            // 
            // TextBoxAddBookPageISBN
            // 
            this.TextBoxAddBookPageISBN.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddBookPageISBN.Location = new System.Drawing.Point(104, 53);
            this.TextBoxAddBookPageISBN.Name = "TextBoxAddBookPageISBN";
            this.TextBoxAddBookPageISBN.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddBookPageISBN.TabIndex = 12;
            // 
            // LabelAddBookPageISBN
            // 
            this.LabelAddBookPageISBN.AutoSize = true;
            this.LabelAddBookPageISBN.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddBookPageISBN.ForeColor = System.Drawing.Color.White;
            this.LabelAddBookPageISBN.Location = new System.Drawing.Point(100, 26);
            this.LabelAddBookPageISBN.Name = "LabelAddBookPageISBN";
            this.LabelAddBookPageISBN.Size = new System.Drawing.Size(51, 23);
            this.LabelAddBookPageISBN.TabIndex = 11;
            this.LabelAddBookPageISBN.Text = "ISBN";
            // 
            // TextBoxAddBookPagePage
            // 
            this.TextBoxAddBookPagePage.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddBookPagePage.Location = new System.Drawing.Point(104, 357);
            this.TextBoxAddBookPagePage.Name = "TextBoxAddBookPagePage";
            this.TextBoxAddBookPagePage.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddBookPagePage.TabIndex = 10;
            this.TextBoxAddBookPagePage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBoxAddBookPagePage_KeyPress);
            // 
            // LabelAddBookPagePage
            // 
            this.LabelAddBookPagePage.AutoSize = true;
            this.LabelAddBookPagePage.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddBookPagePage.ForeColor = System.Drawing.Color.White;
            this.LabelAddBookPagePage.Location = new System.Drawing.Point(100, 330);
            this.LabelAddBookPagePage.Name = "LabelAddBookPagePage";
            this.LabelAddBookPagePage.Size = new System.Drawing.Size(62, 23);
            this.LabelAddBookPagePage.TabIndex = 9;
            this.LabelAddBookPagePage.Text = "Page";
            // 
            // TextBoxAddBookPagePublisher
            // 
            this.TextBoxAddBookPagePublisher.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddBookPagePublisher.Location = new System.Drawing.Point(104, 280);
            this.TextBoxAddBookPagePublisher.Name = "TextBoxAddBookPagePublisher";
            this.TextBoxAddBookPagePublisher.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddBookPagePublisher.TabIndex = 8;
            // 
            // LabelAddBookPagePublisher
            // 
            this.LabelAddBookPagePublisher.AutoSize = true;
            this.LabelAddBookPagePublisher.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddBookPagePublisher.ForeColor = System.Drawing.Color.White;
            this.LabelAddBookPagePublisher.Location = new System.Drawing.Point(100, 253);
            this.LabelAddBookPagePublisher.Name = "LabelAddBookPagePublisher";
            this.LabelAddBookPagePublisher.Size = new System.Drawing.Size(95, 23);
            this.LabelAddBookPagePublisher.TabIndex = 7;
            this.LabelAddBookPagePublisher.Text = "Publisher";
            // 
            // TextBoxAddBookPageAuthor
            // 
            this.TextBoxAddBookPageAuthor.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBoxAddBookPageAuthor.Location = new System.Drawing.Point(104, 203);
            this.TextBoxAddBookPageAuthor.Name = "TextBoxAddBookPageAuthor";
            this.TextBoxAddBookPageAuthor.Size = new System.Drawing.Size(250, 32);
            this.TextBoxAddBookPageAuthor.TabIndex = 6;
            // 
            // LabelAddBookPageAuthor
            // 
            this.LabelAddBookPageAuthor.AutoSize = true;
            this.LabelAddBookPageAuthor.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddBookPageAuthor.ForeColor = System.Drawing.Color.White;
            this.LabelAddBookPageAuthor.Location = new System.Drawing.Point(100, 176);
            this.LabelAddBookPageAuthor.Name = "LabelAddBookPageAuthor";
            this.LabelAddBookPageAuthor.Size = new System.Drawing.Size(75, 23);
            this.LabelAddBookPageAuthor.TabIndex = 5;
            this.LabelAddBookPageAuthor.Text = "Author";
            // 
            // TextBookAddBookPageName
            // 
            this.TextBookAddBookPageName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TextBookAddBookPageName.Location = new System.Drawing.Point(104, 126);
            this.TextBookAddBookPageName.Name = "TextBookAddBookPageName";
            this.TextBookAddBookPageName.Size = new System.Drawing.Size(250, 32);
            this.TextBookAddBookPageName.TabIndex = 4;
            // 
            // LabelAddBookPageName
            // 
            this.LabelAddBookPageName.AutoSize = true;
            this.LabelAddBookPageName.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddBookPageName.ForeColor = System.Drawing.Color.White;
            this.LabelAddBookPageName.Location = new System.Drawing.Point(100, 99);
            this.LabelAddBookPageName.Name = "LabelAddBookPageName";
            this.LabelAddBookPageName.Size = new System.Drawing.Size(124, 23);
            this.LabelAddBookPageName.TabIndex = 3;
            this.LabelAddBookPageName.Text = "Book Name";
            // 
            // PanelAddBookPageSidebarBottom
            // 
            this.PanelAddBookPageSidebarBottom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddBookPageSidebarBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelAddBookPageSidebarBottom.Location = new System.Drawing.Point(1, 499);
            this.PanelAddBookPageSidebarBottom.Name = "PanelAddBookPageSidebarBottom";
            this.PanelAddBookPageSidebarBottom.Size = new System.Drawing.Size(798, 1);
            this.PanelAddBookPageSidebarBottom.TabIndex = 2;
            // 
            // PanelAddBookPageSidebarRight
            // 
            this.PanelAddBookPageSidebarRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddBookPageSidebarRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelAddBookPageSidebarRight.Location = new System.Drawing.Point(799, 0);
            this.PanelAddBookPageSidebarRight.Name = "PanelAddBookPageSidebarRight";
            this.PanelAddBookPageSidebarRight.Size = new System.Drawing.Size(1, 500);
            this.PanelAddBookPageSidebarRight.TabIndex = 1;
            // 
            // PanelAddBookPageSidebarLeft
            // 
            this.PanelAddBookPageSidebarLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddBookPageSidebarLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelAddBookPageSidebarLeft.Location = new System.Drawing.Point(0, 0);
            this.PanelAddBookPageSidebarLeft.Name = "PanelAddBookPageSidebarLeft";
            this.PanelAddBookPageSidebarLeft.Size = new System.Drawing.Size(1, 500);
            this.PanelAddBookPageSidebarLeft.TabIndex = 0;
            // 
            // PanelAddBookPageTop
            // 
            this.PanelAddBookPageTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PanelAddBookPageTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelAddBookPageTop.Controls.Add(this.ButtonAddBookPageExit);
            this.PanelAddBookPageTop.Controls.Add(this.LabelAddBookPageTop);
            this.PanelAddBookPageTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelAddBookPageTop.Location = new System.Drawing.Point(0, 0);
            this.PanelAddBookPageTop.Name = "PanelAddBookPageTop";
            this.PanelAddBookPageTop.Size = new System.Drawing.Size(800, 50);
            this.PanelAddBookPageTop.TabIndex = 3;
            // 
            // ButtonAddBookPageExit
            // 
            this.ButtonAddBookPageExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonAddBookPageExit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAddBookPageExit.FlatAppearance.BorderSize = 0;
            this.ButtonAddBookPageExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddBookPageExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ButtonAddBookPageExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonAddBookPageExit.ForeColor = System.Drawing.Color.Black;
            this.ButtonAddBookPageExit.Image = ((System.Drawing.Image)(resources.GetObject("ButtonAddBookPageExit.Image")));
            this.ButtonAddBookPageExit.Location = new System.Drawing.Point(739, 3);
            this.ButtonAddBookPageExit.Name = "ButtonAddBookPageExit";
            this.ButtonAddBookPageExit.Size = new System.Drawing.Size(53, 46);
            this.ButtonAddBookPageExit.TabIndex = 4;
            this.ButtonAddBookPageExit.UseVisualStyleBackColor = true;
            this.ButtonAddBookPageExit.Click += new System.EventHandler(this.ButtonAddBookPageExit_Click);
            // 
            // LabelAddBookPageTop
            // 
            this.LabelAddBookPageTop.AutoSize = true;
            this.LabelAddBookPageTop.Font = new System.Drawing.Font("Century Gothic", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LabelAddBookPageTop.ForeColor = System.Drawing.Color.White;
            this.LabelAddBookPageTop.Location = new System.Drawing.Point(23, 10);
            this.LabelAddBookPageTop.Name = "LabelAddBookPageTop";
            this.LabelAddBookPageTop.Size = new System.Drawing.Size(127, 28);
            this.LabelAddBookPageTop.TabIndex = 0;
            this.LabelAddBookPageTop.Text = "Add Book";
            // 
            // FormAddBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 550);
            this.Controls.Add(this.PanelAddBookPageMiddle);
            this.Controls.Add(this.PanelAddBookPageTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormAddBook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddBook";
            this.PanelAddBookPageMiddle.ResumeLayout(false);
            this.PanelAddBookPageMiddle.PerformLayout();
            this.PanelAddBookPagePictureBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAddBookPage)).EndInit();
            this.PanelAddBookPageTop.ResumeLayout(false);
            this.PanelAddBookPageTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PanelAddBookPageMiddle;
        private System.Windows.Forms.Panel PanelAddBookPageSidebarBottom;
        private System.Windows.Forms.Panel PanelAddBookPageSidebarRight;
        private System.Windows.Forms.Panel PanelAddBookPageSidebarLeft;
        private System.Windows.Forms.Panel PanelAddBookPageTop;
        private System.Windows.Forms.Button ButtonAddBookPageExit;
        private System.Windows.Forms.Label LabelAddBookPageTop;
        private System.Windows.Forms.TextBox TextBookAddBookPageName;
        private System.Windows.Forms.Label LabelAddBookPageName;
        private System.Windows.Forms.TextBox TextBoxAddBookPagePage;
        private System.Windows.Forms.Label LabelAddBookPagePage;
        private System.Windows.Forms.TextBox TextBoxAddBookPagePublisher;
        private System.Windows.Forms.Label LabelAddBookPagePublisher;
        private System.Windows.Forms.TextBox TextBoxAddBookPageAuthor;
        private System.Windows.Forms.Label LabelAddBookPageAuthor;
        private System.Windows.Forms.TextBox TextBoxAddBookPagePrice;
        private System.Windows.Forms.Label LabelAddBookPagePrice;
        private System.Windows.Forms.TextBox TextBoxAddBookPageISBN;
        private System.Windows.Forms.Label LabelAddBookPageISBN;
        private System.Windows.Forms.Button ButtonAddBookPageAdd;
        private System.Windows.Forms.Panel PanelAddBookPagePictureBox;
        private System.Windows.Forms.PictureBox PictureBoxAddBookPage;
        private System.Windows.Forms.Button ButtonAddBookPageAddPicture;
    }
}